<?php
include('config.php');

$europe = [
    'Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina',
    'Bulgaria', 'Croatia', 'Czech Republic', 'Denmark', 'Estonia', 'Faroe Islands', 'Finland', 'France', 'Georgia',
    'Gibraltar', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg',
    'Macedonia, the former Yugoslav Republic of', 'Malta', 'Moldova, Republic of', 'Monaco', 'Montenegro',
    'Netherlands', 'Norway', 'Portugal', 'Romania', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain',
    'Swaziland', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', 'United Kingdom'
];

if(!isset($_SESSION))
{
    session_start();
}

if(isset($_GET['delete-product'] )){
    
    $selected_product = array_search($_GET['id'], $_SESSION['cart']['id']);
    $_SESSION['product_num'] = $_SESSION['product_num'] - $_GET['qty'];
    if($_SESSION['product_num'] == 0){
        session_destroy();
    }
    unset($_SESSION['cart']['id'][$selected_product]);
    unset($_SESSION['cart']['quantity'][$selected_product]);
    array_values($_SESSION['cart']['id']);
    array_values($_SESSION['cart']['quantity']);
    sort($_SESSION['cart']['id']);
    sort($_SESSION['cart']['quantity']);

	$country = $_GET['country'];
	
	$new_shipping_cost = calculateshipping ($country, $conn, $europe);

    echo json_encode(['num_of_products' => $_SESSION['product_num'], 'new_shipping_cost' => $new_shipping_cost]);
}

if(isset($_POST['product_id'])){
    $qty = $_POST['quantity'];
    $id = $_POST['product_id'];

	$product = db_find_product_by_id($id);

    if(isset($_SESSION['cart'])){

        $srch = array_search($id, $_SESSION['cart']['id']);
        if(is_int($srch))
        {
            $_SESSION['cart']['quantity'][$srch] += $qty ;
        } else {
            array_push($_SESSION['cart']['id'], $id);
            array_push($_SESSION['cart']['quantity'], $qty);
        }
        $_SESSION['product_num'] += $qty;
    } else {

        $_SESSION['cart'] = ['id' => array(), 'quantity' => array()];
        array_push($_SESSION['cart']['id'], $id);
        array_push($_SESSION['cart']['quantity'], $qty);
        $_SESSION['product_num'] = $qty;
    }

    echo json_encode($_SESSION['cart']);
}

if(isset($_GET['update-quantity'])){

    $selected_product = array_search($_GET['id'], $_SESSION['cart']['id']);

    $_SESSION['product_num'] = $_SESSION['product_num'] - $_SESSION['cart']['quantity'][$selected_product] + $_GET['qty'];
    $_SESSION['cart']['quantity'][$selected_product] = $_GET['qty'];

    echo $_SESSION['product_num'];
	
	$country = $_GET['country'];
	$totalitemsincart = $_GET['totalitemsincart'];
	$productid = $_GET['id'];
	$oldqty = $_GET['oldqty'];
	
	$new_shipping_cost = calculateshipping ($country, $conn, $europe);
	
	echo "|".$new_shipping_cost;

}


if(isset($_POST['checkout-submit'])) {
	global $env;

	if(isset($_POST['inv_required']))
	{
		$_SESSION['invoice_req_flag'] = true ;
	}
	else
	{
		$_SESSION['invoice_req_flag'] = false ;
	}

    $_SESSION['review_lng'] = $_POST['lng'];

    $_SESSION['custom_address_val'] = $_POST['custom_address'];

    $vatnr = !empty($_POST['vatnr']) ? trim($_POST['vatnr']) : null;
    $company_name = !empty($_POST['company_name']) ? trim($_POST['company_name']) : null;

	$lng = $env['lang'];
	$price_currency = get_currency_by_lang($env['lang']);

	$is_create_account = (bool) ((isset($_POST['create_account'])) ? $_POST['create_account'] : false);

	$customer_email = (isset($_POST['email'])) ? $_POST['email'] : '';
	$customer_pass = (isset($_POST['pass'])) ? $_POST['pass'] : '';
	$firstname_inv = !empty($_POST['firstname_inv']) ? trim($_POST['firstname_inv']) : null;
	$lastname_inv = !empty($_POST['lastname_inv']) ? trim($_POST['lastname_inv']) : null;

	$use_custom_shipping_address = (bool) (isset($_POST['save_custom_address'])) ? $_POST['save_custom_address'] : 0;
	$use_custom_billing_address = (bool) (isset($_POST['save_custom_billing_address'])) ? $_POST['save_custom_billing_address'] : 0;

	if (!is_authenticated()) {
		$use_custom_shipping_address = true;
		$use_custom_billing_address = true;
	}

	$billing_address_id = (int) ((isset($_POST['billing_address_id'])) ? $_POST['billing_address_id'] : 0);
	$shipping_address_id = (int) ((isset($_POST['shipping_address_id'])) ? $_POST['shipping_address_id'] : 0);

	try {
		$user = db_find_customer_by_email($customer_email);

		if ($is_create_account && !$user) {
			$customer_id = register_customer_account(array(
				'email' => $customer_email,
				'password' => $customer_pass,
				'first_name' => $firstname_inv,
				'last_name' => $lastname_inv
			));

			login_by_id($customer_id);
		} else {
			$customer_id = get_auth_user_id();
		}

		if ($use_custom_billing_address) {
			$address1_inv = !empty($_POST['address1_inv']) ? trim($_POST['address1_inv']) : null;
			$address2_inv = !empty($_POST['address2_inv']) ? trim($_POST['address2_inv']) : null;
			$country_inv = !empty($_POST['country_inv']) ? trim($_POST['country_inv']) : null;
			$postcode_inv = !empty($_POST['postcode_inv']) ? trim($_POST['postcode_inv']) : null;
			$city_inv = !empty($_POST['city_inv']) ? trim($_POST['city_inv']) : null;

			$save_custom_billing_address = (bool) ((isset($_POST['save_custom_billing_address'])) ? $_POST['save_custom_billing_address'] : 0);

			if ($save_custom_billing_address) {
				$address_data = array(
					'first_name' => $firstname_inv,
					'last_name' => $lastname_inv,
					'address_line_1' => $address1_inv,
					'address_line_2' => $address2_inv,
					'country' => $country_inv,
					'postcode' => $postcode_inv,
					'city' => $city_inv
				);

				db_insert_customer_billing_address($customer_id, $address_data);
			}
		} else {
			$billing_address = db_find_customer_address_by_id($customer_id, $billing_address_id);

			if (!$billing_address) {
				throw new AddressNotFoundException("The billing address you have selected is not in your address book.");
			}

			$firstname_inv = $billing_address['first_name'];
			$lastname_inv = $billing_address['last_name'];
			$address1_inv = $billing_address['address_line_1'];
			$address2_inv = $billing_address['address_line_2'];
			$city_inv = $billing_address['city'];
			$country_inv = $billing_address['country'];
			$postcode_inv = $billing_address['postal_code'];
		}

		if ($use_custom_shipping_address) {
			$firstname = trim($_POST['firstname']);
			$lastname = trim($_POST['lastname']);
			$address1 = trim($_POST['address1']);
			$address2 = !empty($_POST['address2']) ? trim($_POST['address2']) : null;
			$country = trim($_POST['country']);
			$postcode = trim($_POST['postcode']);
			$city = trim($_POST['city']);
			
			$save_custom_shipping_address = (bool) ((isset($_POST['save_custom_address'])) ? $_POST['save_custom_address'] : 0);

			if ($save_custom_shipping_address) {
				$address_data = array(
					'type' => 'S',
					'first_name' => $firstname,
					'last_name' => $lastname,
					'address_line_1' => $address1,
					'address_line_2' => $address2,
					'country' => $country,
					'postcode' => $postcode,
					'city' => $city
				);

				db_insert_customer_shipping_address($customer_id, $address_data);
			}
		} else {
			$shipping_address = db_find_customer_address_by_id($customer_id, $shipping_address_id);

			if (!$shipping_address) {
				throw new AddressNotFoundException("The shipping address you have selected is not in your address book.");
			}

			$firstname = $shipping_address['first_name'];
			$lastname = $shipping_address['last_name'];
			$address1 = $shipping_address['address_line_1'];
			$address2 = $shipping_address['address_line_2'];
			$city = $shipping_address['city'];
			$country = $shipping_address['country']; 
			$postcode = $shipping_address['postal_code'];
		}

		$order_id = intval($_POST['order_id']);

		$order = db_find_order_by_id($order_id);

		$total_price = cart_get_total_price();
		$shipping_price = cart_get_shipping_price($country);
		$grand_total_price = cart_get_grandtotal_price($country);

		if ($order) {
			db_query("DELETE FROM order_product WHERE order_id = $order_id");

			db_query("UPDATE orders SET 
					customer_email = '$customer_email',
					first_name = '$firstname', 
					last_name = '$lastname', 
					address_primary = '$address1', 
					address_secondary = '$address2',
					country = '$country', 
					postcode = '$postcode', 
					city = '$city',
					vatnr = '$vatnr', 
					company_name = '$company_name', 
					firstname_inv = '$firstname_inv',
					lastname_inv = '$lastname_inv',
					address1_inv = '$address1_inv', 
					address2_inv = '$address2_inv', 
					country_inv = '$country_inv',
					postcode_inv = '$postcode_inv', 
					city_inv = '$city_inv',
					total_price = '$total_price',
					shipping_price = '$shipping_price',
					grand_total_price = '$grand_total_price',
					price_currency = '$price_currency',
					lang = '{$env['lang']}'
				WHERE id = $order_id"
			);

			$_SESSION['user_order_id'] = $order_id;
		} else {
			$order_hash = generate_unique_order_hash();

			db_query("INSERT INTO orders(
						hash,
						customer_id,
						customer_email,
						first_name, 
						last_name, 
						address_primary, 
						address_secondary, 
						country, 
						postcode, 
						city, 
						vatnr, 
						company_name, 
						firstname_inv,
						lastname_inv, 
						address1_inv, 
						address2_inv, 
						country_inv, 
						postcode_inv, 
						city_inv,
						total_price,
						shipping_price,
						grand_total_price,
						price_currency,
						lang
					) VALUES (
						'$order_hash',
						'$customer_id',
						'$customer_email',
						'$firstname',
						'$lastname',
						'$address1',
						'$address2',
						'$country',
						'$postcode',
						'$city',
						'$vatnr',
						'$company_name',
						'$firstname_inv',
						'$lastname_inv',
						'$address1_inv',
						'$address2_inv',
						'$country_inv',
						'$postcode_inv', 
						'$city_inv', 
						'$total_price', 
						'$shipping_price', 
						'$grand_total_price',
						'$price_currency',
						'{$env['lang']}'
					)"
				);

			$order_id = $conn->insert_id;
			$_SESSION['user_order_id'] = $order_id;
		}

		db_insert_order_products($order_id, cart_get_order_products());

		header("Location: reviewandpay.php");
		exit();
	} catch (AddressNotFoundException $ex) {
		activate_error_state($ex->getMessage());
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state("Unknown error occured.");
	}
}

if(isset($_GET['get_country'])){
    $country = $_GET['country'];
    $qty = $_GET['qty'];
    $product_id = $_GET['product'];
	
	$_SESSION['shipping_price_en'] = 0;
    $_SESSION['shipping_price_pl'] = 0;

	$new_shipping_cost = calculateshipping ($country, $conn, $europe);
	
    $_SESSION['shipping_price_'.$lng] = $new_shipping_cost;
    // $_SESSION['shipping_price_pl'] = trim($result[1]);

    echo json_encode(['en' => $_SESSION['shipping_price_en'], 'pl' => $_SESSION['shipping_price_pl']]);
}

if(isset($_POST['paypal_submit'])){

    echo "<pre>"; print_r( 'aaa' ); echo "</pre>" ; exit();
    $formData = $_POST;
    $class = new TransactionController();
    echo "<pre>"; print_r( 'tu' ); echo "</pre>" ; exit();
    $class->postPayment($formData);
    echo "<pre>"; print_r( 'nije' ); echo "</pre>" ; exit();
}

if (isset($_POST['resetpass_submit'])) {
	try {
		$password = $_POST['pass'];

		$parameter_hash = (isset($_GET['hash'])? $_GET['hash'] : '');

		$request_row = db_find_forgotten_password_request_by_hash($parameter_hash);

		if (!$request_row) {
			redirect_to_home();
		}

		$email = $request_row['email'];

		db_change_customer_password($email, $password);

		db_remove_forgotten_password_request_by_hash($parameter_hash);

		activate_complete_state('Your password has been changed successfully.');
	} catch (Exception $ex) {
		log_error($ex->getMessage());

		activate_error_state('Unknown error ocurred. Please, try again and if the problem persists: contact us!');
	}
}

if (isset($_POST['changepass_submit'])) {
	try {
		$oldpass = (isset($_POST['oldpass'])) ? $_POST['oldpass'] : '';
		$newpass = (isset($_POST['pass'])) ? $_POST['pass'] : '';

		$auth_user = get_auth_user();

		if ($auth_user['password'] != md5($oldpass)) {
			activate_error_state('You must type your old password correctly.');
		} else {
			if (!validate_customer_password($newpass)) {
				throw new DataNotValidException("");
			}

			db_change_customer_password($auth_user['email'], $newpass);

			activate_complete_state('Your password has been changed successfully.');
		}
	} catch (DataNotValidException $ex) {
		activate_error_state(lang_get_validation_error_message());
	} catch (Exception $ex) {
		log_error($ex->getMessage());

		activate_error_state('Unknown error ocurred. Please, try again and if the problem persists: contact us!');
	}
}

if (isset($_POST['forgottenpass_submit'])) {
	try {
		$emailaddress = $_POST['email'];
		$resetpass_url = generate_forgotten_password_link($emailaddress);
		$subject = "Forgotten Password";
		$message = "Hello, you are receiving this e-mail because someone has clicked on the forgotten password link and provided your e-mail address.<br />
			Click on the link below:<br />
			<a href=\"$resetpass_url\">$resetpass_url</a> <br /><br />
			If you have not initiated this forgotten password request, please contact us directly by replying to this email.
		";
		$from="From: Agaf Design <" . $conf['mail_from'] . ">\r\nReturn-path: " . $conf['mail_from'];

		require 'includes/PHPMailer-master/PHPMailerAutoload.php';

		$mail = new PHPMailer();
		$mail->setFrom($conf['mail_from']);
		$mail->addAddress($emailaddress);
		$mail->addReplyTo($conf['mail_from']);
		$mail->isHTML(true);
		$mail->Subject = $subject;
		$mail->Body = $message;

		if(!$mail->send()) {
			activate_error_state('Unknown error ocurred. Please, try again and if the problem persists: contact us!');

			log_error($mail->ErrorInfo);
		} else {
			activate_complete_state('An email is sent to this e-mail address with information on how to reset your password.');
		}

	} catch (Exception $ex) {
		activate_error_state('Unknown error ocurred. Please, try again and if the problem persists: contact us!');
	}
}

if (isset($_POST['login_submit'])) {


	// Define $myusername and $mypassword
	$username = $_POST['username'];
	$password = $_POST['password'];

	// To protect MySQL injection
	$username = $conn->real_escape_string($username);

	try {
		$row_attempts = db_get_login_attempts($username);

		if (!$row_attempts) {
			db_insert_login_attempt($username);
		} elseif ($row_attempts['attempts'] < $conf['max_login_attempts']) {
			db_update_login_attempt($username);
		} else {
			throw new MaxLoginAttemptsReached("");
		}

		$row_customer = db_find_customer_by_email($username);

		$is_active = (bool) $row_customer['is_active'];

		if (!$row_customer['is_active']) {
			throw new CustomerAccountNotActiveException("");
		}

		if ($row_customer && $row_customer['password'] == md5($password)) {
			login_by_id($row_customer['id']);

			redirect_to_dashboard_index();
		} else {
			throw new InvalidLoginCredentialsException("");
		}
	} catch(MaxLoginAttemptsReached $ex) {
		activate_error_state('You have reached the maximum number of login attempts, please wait awhile.');
	} catch (CustomerAccountNotActiveException $ex) {
		$hash = generate_account_validation_hash($username);
		db_insert_account_validation_request($username, $hash);

		$activation_link = resent_validation_url($hash);

		activate_error_state('Customer account not active. ' . '<a href="' . $activation_link . '">' . 'Resend Activation Email' . '</a>');
	} catch (InvalidLoginCredentialsException $ex) {
		activate_error_state('Invalid credentials.');
	} catch(DbException $ex) {
		log_error(db_get_last_error());
		activate_error_state('Unknown error ocurred');
	} catch(Exception $ex) {
		log_error($ex);
		activate_error_state('Unknown error ocurred');
	}

}

if (isset($_POST['addaddress_submit'])) {
	try {
		$type = (isset($_POST['type'])) ? $_POST['type'] : 'B';
		$firstname = (isset($_POST['first_name'])) ? $_POST['first_name'] : '';
		$lastname = (isset($_POST['last_name'])) ? $_POST['last_name'] : '';
		$address1 = (isset($_POST['address_line_1'])) ? $_POST['address_line_1'] : '';
		$address2 = (isset($_POST['address_line_2'])) ? $_POST['address_line_2'] : '';
		$country = (isset($_POST['country'])) ? $_POST['country'] : '';
		$postcode = (isset($_POST['postcode'])) ? $_POST['postcode'] : '';
		$city = (isset($_POST['city'])) ? $_POST['city'] : '';

		$customer_id = get_auth_user_id();

		$address_data = array(
			'type' => $type,
			'first_name' => $firstname,
			'last_name' => $lastname,
			'address_line_1' => $address1,
			'address_line_2' => $address2,
			'country' => $country,
			'postcode' => $postcode,
			'city' => $city
		);

		set_view_data('address', $address_data);

		if (!validate_address($address_data)) {
			throw new DataNotValidException("");
		}

		db_insert_customer_address($customer_id, $address_data);
		$new_address_id = db_get_last_insert_id();

		$default_address = db_find_customer_default_address($customer_id, $type);

		if (!$default_address) {
			db_set_default_customer_address($customer_id, $new_address_id, $type);
		}

		activate_complete_state('The address is saved!');

		redirect_to_dashboard_addressbook_index();
	} catch (DataNotValidException $ex) {
		activate_error_state(lang_get_validation_error_message());
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state('Unknown error ocurred');
	}
}

if (isset($_POST['editaddress_submit'])) {
	try {
		$id = (isset($_GET['id'])) ? $_GET['id'] : 0;
		$type = (isset($_POST['type'])) ? $_POST['type'] : 'B';
		$firstname = (isset($_POST['first_name'])) ? $_POST['first_name'] : '';
		$lastname = (isset($_POST['last_name'])) ? $_POST['last_name'] : '';
		$address1 = (isset($_POST['address_line_1'])) ? $_POST['address_line_1'] : '';
		$address2 = (isset($_POST['address_line_2'])) ? $_POST['address_line_2'] : '';
		$country = (isset($_POST['country'])) ? $_POST['country'] : '';
		$postcode = (isset($_POST['postcode'])) ? $_POST['postcode'] : '';
		$city = (isset($_POST['city'])) ? $_POST['city'] : '';

		$customer_id = get_auth_user_id();

		$address_data = array(
			'type' => $type,
			'first_name' => $firstname,
			'last_name' => $lastname,
			'address_line_1' => $address1,
			'address_line_2' => $address2,
			'country' => $country,
			'postcode' => $postcode,
			'city' => $city
		);

		if (!validate_address($address_data)) {
			throw new DataNotValidException("");
		}

		$new_address_id = db_update_customer_address($customer_id, $id, $address_data);

		activate_complete_state('The address has been updated successfully!');

		redirect_to_dashboard_addressbook_index();
	} catch (DataNotValidException $ex) {
		activate_error_state(lang_get_validation_error_message());
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state('Unknown error ocurred');
	}
}

if (isset($_POST['removeaddress_submit'])) {
	try {
		$address_id = (isset($_POST['address_id'])) ? $_POST['address_id'] : 0;
		$customer_id = get_auth_user_id();

		do_remove_customer_address($customer_id, $address_id);

		activate_complete_state('The address has been removed!');

		redirect_to_dashboard_addressbook_index();
	} catch (CannotRemoveDefaultAddressException $ex) {
		log_error($ex->getMessage());
		activate_error_state('This address cannot be removed. Please make another address default and then you can remove this one.');
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state('Unknown error ocurred');
	}
}

if (isset($_POST['makedefault_submit'])) {
	try {
		$address_id = (isset($_POST['address_id'])) ? $_POST['address_id'] : 0;
		$customer_id = get_auth_user_id();

		db_set_customer_default_address($customer_id, $address_id);

		redirect_to_dashboard_addressbook_index();
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state('Unknown error ocurred');
	}
}

if (isset($_POST['signup_submit'])) {
	try {
		$firstname = $_POST['firstname'];
		$lastname = $_POST['lastname'];
		$emailaddress = $_POST['emailadress'];
		$password = $_POST['password'];

		if (db_find_customer_by_email($emailaddress)) {
			throw new CustomerExistsException('A customer with such e-mail already exists!');
		}

		register_customer_account(array(
			'email' => $emailaddress,
			'password' => $password,
			'first_name' => $firstname,
			'last_name' => $lastname
		));

		activate_complete_state('Your registration is complete! You will soon receive an e-mail with an activation link. Thank you!');
	} catch (CustomerExistsException $ex) {
		activate_error_state($ex->getMessage());
	} catch (DataNotValidException $ex) {
		activate_error_state(lang_get_validation_error_message());
	} catch (Exception $ex) {
		log_error($ex->getMessage());
		activate_error_state('Unknown error ocurred');
	}
}

if (is_dashboard() && !is_dashboard_login() && !is_authenticated()) {
	redirect_to_dashboard_login();
}

if (is_dashboard_login() && is_authenticated()) {
	redirect_to_dashboard_index();
}

?>
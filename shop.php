<?php
require_once('config.php');

$title="Agaf Design ".$lang['shop-nav-menu']." Online";
$meta_desc = "Area 16 - Shop Meta Description";

// sort out breadcrumbs
$breadcrumbs = array();
$breadcrumbs ['shop.php'] = $lang['shop-bread']; 

$cat="null";
if(isset($_GET["cat"])) { 
	$category=$_GET["cat"]; 
	$category_display = ucwords(str_replace("-"," ",$category));
	
	if ($category_display == "Drinkware" && $lng == "pl") { $category_display = "Drinkware (Kubki i Filiżanki)"; }
	if ($category_display == "Tableware" && $lng == "pl") { $category_display = "Tableware (Misy, Patery i Talerze)"; }
	}

	$sql_products = "SELECT * FROM products WHERE forsale = 'Y' AND product_cat_".$lng." LIKE '%$category%' ORDER BY orderby ASC";
	$result_products = $conn->query($sql_products);
	
	if (!empty($category) && $result_products->num_rows > 0) {
		$title="Agaf Design ".$lang['shop-nav-menu']." - ".$category_display;
		$banner_line_class = $banner_line_class."-".$category;
		
		$this_cat_url = "shop.php?cat=".trim(strtolower($category));
		$breadcrumbs [$this_cat_url] = $category_display;
		$lastElementBreadcrumbs = $category_display;
	}
	
// breadcrumbs

// banner-img
if ($banner_line_class=="banner-line-shop") {
	$banner_img="images/site-images/banner-shop-img.jpg";
} else {
	$banner_img="images/site-images/banner-shop-img-".$category.".jpg";	
}


require_once('header.php');

?>
<div id="banner-line" class="banner-line-shop-all">
	<div id="banner-line-image" style="display: none; height: 375px; width: 100%;">
	</div>
	<?php if (empty($category)) { ?>
	<div id="banner-line-logo" style="position: absolute; top: 50%; left: 50%; margin-top: -45px; margin-left: -55px;">
	<img src="images/site-images/shop-banner-logo.png" style="width: 110px" />
	</div>
	<?php } ?>
</div>



<!--<div id="product-listings">-->
    <div class="container">
	
		<div class="breadcrumbs-wrapper">
				<div class="col-xs-12">
					<div class="row">
						<p>
						<?php
						if (count($breadcrumbs)>1) {
							foreach ($breadcrumbs as $key => $value) {
							echo "<a href=\"".$key."\">".$value."</a>";
							if ( $value!=$lastElementBreadcrumbs )	{ echo " &gt; "; }								
							}
						}
						?>

						</p>
					</div>
				</div>
		 </div> <!-- /.breadcrumbs-wrapper -->
	
	
	
	
	
	
        <div class="row">
            <div class="shop-products-main-wrapper clearfix">
                <div class="col-xs-12">
                    <div class="shop-products-main-inner-wrapper clearfix">
					<?php

										
					if (!empty($category) && $result_products->num_rows > 0) {
					?>
					<h1><?php echo $title; ?></h1>
					<p style="text-align: center"><?php echo $lang['products_made_by']; ?></p>
					
					<?php
					if ($category_display == "Drinkware" || $category_display=="Drinkware (Kubki i Filiżanki)") { echo "<h2>".$lang['im_ceramic']."</h2>";  }
					?>
					
					<div class="product-cell-wrapper">
					<?php
					$counter=0;
					while($row_products = $result_products->fetch_assoc()) {
							
							$product_imgs_array = explode(",",$row_products['product_images_'.$lng]);
							$lead_img = trim($product_imgs_array[0]);

							$price_display = ($lng=="en") ? $currency_symbol.$row_products['product_price_en'] : $row_products['product_price_pl']." ".$currency_symbol;

						?>
								<div class="product-cell" >
								<a href="product.php?product=<?php echo $row_products['product_url_'.$lng]; ?>">
									<div class="product-img"><img src="images/product-images/<?php echo $lead_img; ?>" /></div>
									<div class="product-title">
										<div class="product-title-text"><?php echo $row_products['product_name_'.$lng]; ?></div>
										<div class="product-price"><?php echo $lang['price-main']; ?> <?php 
										
											echo $price_display; 
												if ($row_products['productid']==1 || $row_products['productid'] == 9) {
													if ($lng=="en") { echo " until 14/8"; }
													if ($lng=="pl") { echo " do 14/8"; }
													} ?>
										
										</div>
									</div>
								</a>
								</div>
						<?php
							$counter++;
							if (($counter==4 || $counter==6) && ($category_display == "Drinkware" || $category_display=="Drinkware (Kubki i Filiżanki)")) { ?>
							
							<div class="clearing"></div>
						</div>
							<h2><?php if ($counter==4) { echo $lang['mugs']; } if ($counter==6) { echo $lang['cups']; } ?></h2>
							
						<div class="product-cell-wrapper">
							<?php }
						}
						?>
						<div class="clearing"></div>
					</div>		
						<?php	
						}
						else
						{
						?>	
						<h1>Agaf Design <?php echo $lang['shop-nav-menu']; ?></h1>
												
						<?php if ($lng=="pl") { ?>
		
						<p style="margin-top: 40px;">Formy, którymi się otaczamy i których używamy na co dzień oddziałują na nas. </p>
						
						<p>Mają one wpływ na nasze doznania, kreatywność, poczucie estetyki, a nawet nastrój, dlatego  nie mogą być przypadkowe.</p>

						<p>Powinny być funkcjonalne, trwałe i piękne ale też nas inspirować, zaskakiwać, bawić i opowiadać wciąż nowe historie, nawiązując jednocześnie dialog z odbiorcą.</p>

						<p>Takie formy powstają tylko wtedy, kiedy są tworzone z pasją, ale uwagą na każdy szczegół, świadomie i z wyobraźnią. Dlatego właśnie powstało moje Studio Projektowe Agaf Design - Fun Loving Function.</p>
						
						
						<?php } else { ?>

						<p style="margin-top: 40px;">Forms surround us and affect us every day.</p>

						<p>They have an impact on our experience, creativity, aesthetic sense and even on our mood. For these reasons they have to be created with thought.</p>

						<p>Forms should be functional, durable and beautiful but also have the ability to inspire, surprise, entertain, tell new stories and create a dialogue with the user. </p>

						<p>Such are present only when conceived with passion, imagination, attention to every detail and with respect for the audience. That's why Agaf Design Studio was created.</p>

						<?php } ?>
						
						
						

						<hr style="width: 55%; margin: 10px auto;" />
						

							<div class="shop-categories" style="font-family: 'Arial';">
								<div class="cat-drinkware">
									<div class="semi-trans-white"></div>
									<div class="cat-text">
									<a href="shop.php?cat=drinkware" class="cat-header">Drinkware</a>
									<?php if ($lng=="pl") { ?><br />(Kubki i Filiżanki)<?php } ?>
									</div>
								</div>
							</div>
														
							<div class="shop-categories" style="font-family: 'Arial';">
								<div class="cat-tableware">
									<div class="semi-trans-white"></div>
									<div class="cat-text">								
									<a href="shop.php?cat=tableware" class="cat-header">Tableware</a>
									<?php if ($lng=="pl") { ?><br />(Misy, Patery i Talerze)<?php } ?>
									</div>
								</div>
							</div>

							<!-- <div class="shop-categories">
								<a href="shop.php?cat=clearance" class="cat-header">Clearance</a>
							</div> -->							


						<?php	
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>


<div class="clearing"></div>
<!--</div> -->
<img id="preload" src="<?php echo $banner_img; ?>" style="display:none;" />
<?php	
require_once('footer.php');
?>
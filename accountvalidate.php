<?php

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

$parameter_hash = (isset($_GET['hash'])? $_GET['hash'] : '');

$request_row = db_find_account_validation_request_by_hash($parameter_hash);

if (!$request_row) {
	redirect_to_home();
} else {
	db_update_customer_account_set_active($request_row['email']);

	db_mark_account_validation_request_as_used($parameter_hash);

	activate_complete_state("Your account has been validated, you can proceed to login");
}

require_once(PROJECT_ROOT . '/header.php');

?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="row dashboard-wrapper">
					<div class="dashboard-inner col-md-4 col-md-offset-4">
						<h3>Account Activation</h3>

						<?php if (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php endif; ?>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
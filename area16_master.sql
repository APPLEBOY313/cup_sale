-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 04, 2017 at 07:34 PM
-- Server version: 5.5.50-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `area16_master`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_validation_requests`
--

CREATE TABLE `account_validation_requests` (
  `email` varchar(100) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `is_sent` tinyint(1) NOT NULL DEFAULT '0',
  `is_used` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `account_validation_requests`
--

INSERT INTO `account_validation_requests` (`email`, `hash`, `created_at`, `is_sent`, `is_used`) VALUES
('contact@link2light.com', '45e28b5739522b75e44793f694270b05', '2017-08-16 17:38:23', 0, 1),
('contact@link2light.com', '99aa7b067716629a179ff1e645039ae0', '2017-08-29 13:58:18', 0, 1),
('hm.ych111@hotmail.com', '35abebaa6f58f0dea6e76b5f9c25fa4d', '2017-09-03 09:14:39', 1, 0),
('hm.ych111@hotmail.com', 'b8c151e9af39991b6b63be2619b08140', '2017-09-03 09:14:12', 1, 0),
('hm.ych111@hotmail.com', 'c0f183992af944a4a4e25cfdaaa40e65', '2017-09-03 09:21:36', 1, 0),
('hm.ych111@hotmail.com', 'eb6123e5211b14d747ef70de96848287', '2017-09-03 09:19:27', 0, 0),
('rahuljain1991@live.com', '185cb0a84df576f70781bd423370b7e0', '2017-09-03 09:54:08', 1, 0),
('simpleideasweb@gmail.com', '053a8a40812b23ceb0b38ae5ae907bc7', '2017-08-02 15:10:34', 0, 0),
('simpleideasweb@gmail.com', '06ea7ceabf205d8afc5c3a586355b9a8', '2017-08-02 15:08:02', 0, 0),
('simpleideasweb@gmail.com', '09e7bbe669b079b5962492cf690d70b9', '2017-08-30 16:01:48', 1, 0),
('simpleideasweb@gmail.com', '0b7c5dab43e1400c32e1a68bca80bdd6', '2017-08-30 16:04:04', 1, 1),
('simpleideasweb@gmail.com', '13739f4e5c0f90ef489481f241335d7f', '2017-08-30 15:51:38', 0, 0),
('simpleideasweb@gmail.com', '197402b39e39cc52b9acdee57d65d00e', '2017-08-02 15:09:19', 0, 0),
('simpleideasweb@gmail.com', '313fc6c59dce47301f5a07f01d2bede3', '2017-08-02 15:01:35', 0, 0),
('simpleideasweb@gmail.com', '3bfc1c82f4233bf90781b698883272a8', '2017-08-02 15:17:58', 0, 0),
('simpleideasweb@gmail.com', '4197d0ce9e5b5355d414c0c089befa86', '2017-08-30 15:59:39', 1, 0),
('simpleideasweb@gmail.com', '523af5d500f32d6b91d8fdba3dfb9334', '2017-08-01 21:11:44', 0, 0),
('simpleideasweb@gmail.com', '54408110353d7a2d17494d6da0c133f7', '2017-08-30 15:51:31', 1, 0),
('simpleideasweb@gmail.com', '5d1c9689409804d1883052d6027a1724', '2017-08-30 15:51:45', 0, 0),
('simpleideasweb@gmail.com', '5eba403f3473b16bd80dbd9eeb0b9d15', '2017-08-02 15:18:58', 0, 0),
('simpleideasweb@gmail.com', '604c76d1717c8395f1556418d6754ddb', '2017-08-02 15:07:35', 0, 0),
('simpleideasweb@gmail.com', '6258898a9cb16607a18a87e5e083565e', '2017-08-30 16:01:37', 1, 0),
('simpleideasweb@gmail.com', '75bea1ae131ad290e11d78a9931d0b0f', '2017-08-30 15:50:07', 0, 1),
('simpleideasweb@gmail.com', '76400cf52bbff52997d2642824bb2530', '2017-08-01 22:28:36', 0, 0),
('simpleideasweb@gmail.com', '79c01d8be05b4683ab02b3f36553cc77', '2017-08-02 15:16:18', 0, 0),
('simpleideasweb@gmail.com', '80c817bd5c72feff7c4871fbe325f10b', '2017-08-01 22:31:27', 0, 0),
('simpleideasweb@gmail.com', '82947ba13d80ddb5d138fb30d2b9d27c', '2017-08-02 15:20:49', 0, 0),
('simpleideasweb@gmail.com', '83dcfe857e3a2f4ce54488eda40969e3', '2017-08-01 22:32:20', 0, 0),
('simpleideasweb@gmail.com', '84777e15188d32f03e294e161b6747b0', '2017-08-10 14:34:22', 0, 1),
('simpleideasweb@gmail.com', '880c2d3525666f635e401059aa0c9e93', '2017-08-02 15:20:18', 0, 0),
('simpleideasweb@gmail.com', '88e63614ce56640d650dff58a6592dea', '2017-08-02 15:26:47', 0, 0),
('simpleideasweb@gmail.com', '8b706c39a4de858915a4a6511f704d94', '2017-08-02 15:15:06', 0, 0),
('simpleideasweb@gmail.com', '8f75ba4818e8a5d78b3177fc9ef2d97a', '2017-08-01 22:29:32', 0, 1),
('simpleideasweb@gmail.com', '99be2c37f938630050a29934511b342c', '2017-08-02 15:16:00', 0, 0),
('simpleideasweb@gmail.com', '9a4accfa9807605e22ce18fc1aca80d5', '2017-08-01 22:31:09', 0, 0),
('simpleideasweb@gmail.com', 'b29e5f9cd7c6c0bb8f41ecc90af84e5f', '2017-08-02 15:25:45', 0, 0),
('simpleideasweb@gmail.com', 'b8641fd9bab9b5eda155e6631ead8976', '2017-08-02 15:08:19', 0, 0),
('simpleideasweb@gmail.com', 'c99b749d9751272841bb004152dbeec5', '2017-08-02 15:06:52', 0, 0),
('simpleideasweb@gmail.com', 'ddc2c32893b679896412af3207550553', '2017-08-02 14:59:55', 0, 0),
('simpleideasweb@gmail.com', 'df1ce314634dda15562c88e62ab8a71f', '2017-08-02 15:18:28', 0, 0),
('simpleideasweb@gmail.com', 'e2526e0966c85a9664be46292bcf2546', '2017-08-02 15:25:20', 0, 0),
('support@link2lght.com', '1fc11117d60b561cb4fff84b42cd32ad', '2017-08-16 17:29:04', 0, 0),
('support@link2light.com', '34b3452851b9b6a3846e065860bb007b', '2017-08-15 12:58:30', 0, 0),
('support@link2light.com', 'b80d0aeda97c35a208bc52b45f00b55f', '2017-09-01 10:55:28', 0, 0),
('support@link2light.com', 'bd1a423716a65a7f7061a4497a52c2e8', '2017-08-15 12:57:52', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `first_name` varchar(20) NOT NULL,
  `last_name` varchar(20) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `default_billing_address_id` int(11) NOT NULL,
  `default_shipping_address_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `email`, `password`, `first_name`, `last_name`, `is_active`, `default_billing_address_id`, `default_shipping_address_id`) VALUES
(1, 'ivanzanev477@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 'Ivan', 'Zanev', 0, 0, 0),
(33, 'simpleideasweb@gmail.com', '8287458823facb8ff918dbfabcd22ccb', 'Ivan', 'Zanev', 1, 23, 34),
(40, 'contact@link2light.com', 'ae2046dd99032f99b8f4cc19d60df2cf', 'Bob', 'BeforeCheckout', 1, 40, 41),
(41, 'hm.ych111@hotmail.com', '0e7517141fb53f21ee439b355b5a1d0a', 'YuanHua', 'Chen', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_addresses`
--

CREATE TABLE `customer_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `type` char(1) NOT NULL,
  `city` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postal_code` varchar(12) NOT NULL,
  `address_line_1` varchar(50) NOT NULL,
  `address_line_2` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer_addresses`
--

INSERT INTO `customer_addresses` (`id`, `customer_id`, `first_name`, `last_name`, `type`, `city`, `country`, `state`, `postal_code`, `address_line_1`, `address_line_2`) VALUES
(4, 31, 'Ivan', 'Z', 'S', 'neverland', 'FLK', '', '123897', 'gateway 1', 'gateway 2'),
(18, 31, 'Ivan', 'Zanev', 'B', 'Sofia', 'BGR', '', '1239087', 'ul. B928378', ''),
(23, 33, 'Ivan', 'Zanev', 'B', 'Burgas', 'BGR', '', '8000', 'Burgas, Bulgaria', 'ul. 239827987'),
(34, 33, 'Ivan', 'Zanev', 'S', 'Ipswitch', 'GBR', '', '213312', 'ul. 12938789', 'ul. 239827987'),
(36, 33, 'Ivan', 'Zanev', 'B', 'Ipswitch', 'AFG', '', '213312', 'ul. 123897', 'ul. 239827987'),
(39, 33, 'Ivan', 'Zanev', 'B', 'Burgas', 'ATA', '', '8000', 'Burgas, Bulgaria', ''),
(40, 40, 'Bob', 'Beforecheckout', 'B', 'The Town of Bob', 'ALB', '', 'E3 3NH', 'The address of Bob', ''),
(41, 40, 'Bob', 'Beforecheckout', 'S', 'Other Town', 'AZE', '', '20665', 'First Line Address', ''),
(43, 40, 'deepak', 'lohani', 'B', 'indore', 'IND', '', '23333', 'indore address', 'ad');

-- --------------------------------------------------------

--
-- Table structure for table `forgotten_password_requests`
--

CREATE TABLE `forgotten_password_requests` (
  `email` varchar(100) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `forgotten_password_requests`
--

INSERT INTO `forgotten_password_requests` (`email`, `hash`, `created_at`) VALUES
('ivanzanev477@gmail.com', '2078ed744d07840fa1072b52e1f0b81c', '2017-07-30 22:06:37'),
('ivanzanev477@gmail.com', '79e5a1ea9a3d69806ceeb251a8b17690', '2017-07-30 21:56:14'),
('ivanzanev477@gmail.com', 'bd20465d2aa2206ee143269640a4a1a6', '2017-07-30 22:05:54'),
('ivanzanev477@gmail.com', 'ea348d3dacef35b59b9191e01ac4a6f2', '2017-07-30 22:07:22'),
('ivanzanev477@gmail.com', 'f945f014d636aa0aa30109379ffa9dce', '2017-07-30 22:05:54'),
('ivanzanev477@gmail.com', 'fa54a425050f7bfc6238543d3e4d2a83', '2017-07-30 21:57:01');

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `langid` int(3) NOT NULL,
  `lang_type` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `lang_code` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `en` varchar(240) COLLATE utf8_unicode_ci NOT NULL,
  `pl` varchar(240) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`langid`, `lang_type`, `lang_code`, `en`, `pl`) VALUES
(1, 'lbl', 'home-page', 'Home', 'Strona Główna'),
(2, 'lbl', 'contact', 'Contact', 'Kontakt'),
(5, 'lbl', 'shop-nav-menu', 'Shop', 'Sklep'),
(6, 'lbl', 'about-nav-menu', 'About', 'O Mnie'),
(7, 'lbl', 'stockists-nav-bar', 'Stockists', 'Handlowców'),
(8, 'lbl', 'handmade-in-london', 'Handmade In London', 'Spódniczka w Londynie'),
(9, 'lbl', 'about-home-page', 'about', 'O Mnie'),
(10, 'lbl', 'gilded-speak-vase', 'Gilded Speak Vase', 'Pozłacane Mów wazon'),
(11, 'lbl', 'view-home-page', 'View', 'Widok'),
(12, 'lbl', 'wheel-thrown', 'Wheel Thrown', 'Rzucony Koło'),
(13, 'lbl', 'shop-home-page', 'shop', 'sklep'),
(15, 'lbl', 'view-film', 'view film', 'widok filmu'),
(16, 'lbl', 'one-off-pieces-h-pge', 'One-Off Pieces, Limited Editions', 'Kawałki jednorazowych limitowane'),
(17, 'lbl', 'gilded-finch-hp', 'Gilded Finch Bowl In Black', 'Kawałki jednorazowych limitowane'),
(18, 'lbl', 'shop-now', 'shop now', 'Kupuj teraz'),
(19, 'lbl', 'lrg-speak-vase-hp', 'Large Speak Vase In Black', 'Duży Speak wazon w kolorze czarnym'),
(20, 'lbl', 'mailing-list-footer', 'Mailing List', 'Lista mailingowa'),
(21, 'lbl', 'terms-footer', 'terms of service', 'warunki usługi'),
(22, 'lbl', 'e-mail-footer', 'Email Address', 'Adres e-mail'),
(23, 'lbl', 'sign-up', 'sign up', 'Zapisz się'),
(24, 'lbl', 'name-main', 'Name', 'Nazwa'),
(25, 'lbl', 'first-name', 'First name', 'Imię'),
(26, 'lbl', 'last-name', 'Last name', 'Nazwisko'),
(27, 'lbl', 'e-mail-main', 'Email Address', 'Adres e-mail'),
(28, 'lbl', 'phone-main', 'Phone', 'Telefon'),
(29, 'lbl', 'country-main', 'Country', 'Kraj'),
(30, 'lbl', 'subject-main', 'Subject', 'Temat'),
(31, 'lbl', 'message-main', 'Message', 'Wiadomość'),
(32, 'lbl', 'submit-main', 'submit', 'Zatwierdź'),
(35, 'lbl', 'general-enquiries', 'General enquiries', 'Pytania ogólne'),
(36, 'lbl', 'press', 'Press', 'Naciśnij'),
(38, 'lbl', 'studio', 'Studio', 'Studio'),
(39, 'lbl', 'price-main', 'Price', 'Cena'),
(40, 'lbl', 'shop-bread', 'Shop', 'Sklep'),
(41, 'lbl', 'product-bread', 'Product', 'Produkt'),
(42, 'lbl', 'add-to-cart-main', 'Add to Cart', 'Dodaj do koszyka'),
(43, 'lbl', 'shipping-singl-pr', 'Shipping', 'Wysyłka'),
(44, '', 'shipping-info', 'We ship worldwide within 5 working days. The following charges apply', 'Wysyłamy na całym świecie w ciągu 5 dni roboczych. Obowiązują następujące opłaty'),
(45, 'lbl', 'region-ship', 'Region', 'Region'),
(46, 'lbl', 'ship-alone', 'When shipped alone', 'Koszt wysyłki 1 produktu'),
(47, 'lbl', 'ship-another', 'When shipped with another product', 'Koszt wysyłki 1 szt.(kompletu) jeżeli zamawiany z innym produktem'),
(48, 'lbl', 'poland-ship', 'Poland', 'Polska'),
(49, 'lbl', 'europe-ship', 'Europe', 'Europa'),
(50, 'lbl', 'rest-of-world-ship', 'Rest of World', 'Reszta świata'),
(51, 'lbl', 'shopping-cart', 'Shopping Cart', 'Lista wybranych produktów'),
(52, 'lbl', 'item-main', 'Item', 'Pozycja'),
(53, 'lbl', 'quantity-main', 'Quantity', 'Ilość'),
(54, 'lbl', 'subtotal-main', 'Subtotal', 'Razem'),
(55, 'lbl', 'checkout-main', 'CHECKOUT', 'Zakup'),
(57, 'lbl', 'checkout-page', 'Checkout', 'Zamówienie'),
(58, 'lbl', 'shipping-billing', 'Shipping & Billing', 'Dane do wysyłki'),
(59, 'lbl', 'adress-main', 'Address', 'Adres'),
(60, 'lbl', 'postcode-main', 'Postcode', 'Kod pocztowy'),
(61, 'lbl', 'town-main', 'Town/City', 'Miasto / Miejscowość'),
(62, 'lbl', 'invoice-req-main', 'Invoice Required', 'Faktura'),
(63, 'lbl', 'vat-num-main', 'VAT number', 'numer VAT'),
(64, 'lbl', 'comp-name-main', 'Company Name', 'Nazwa firmy'),
(65, 'lbl', 'order-summary', 'Order Summary', 'Podsumowanie zamówienia'),
(66, 'lbl', 'review-pay-but-main', 'REVIEW &amp; PAY', 'Sprawdż Dane i Zatwierdź'),
(67, 'lbl', 'pop-up-addtocart', 'Product Added to Cart', 'Produkt został dodany do koszyka'),
(68, 'lbl', 'pop-up-carry', 'Carry on Shopping', 'Powrót do sklepu'),
(69, 'lbl', 'main-previous', 'Previous', 'Poprzedni'),
(70, 'lbl', 'main-next', 'Next', 'Następny'),
(71, 'lbl', 'review-and-pay', 'Review &amp; Pay', ''),
(72, 'lbl', 'back', 'Back', 'Powrót'),
(73, 'lbl', 'shipping', 'Shipping', 'Dostawa'),
(74, 'lbl', 'total', 'Total', 'Całkowity'),
(75, 'lbl', 'invoice-details', 'Invoice Details', 'Faktura'),
(76, 'lbl', 'mugs', 'Mugs', 'Kubki'),
(77, 'lbl', 'products', 'Products', 'Produkty'),
(78, 'txt', 'contact_thanks', '<h1>Thanks for getting in touch!</h1><p style=\'text-align: center\'>We\'ll get back to you shortly.</p>', '<h1>Dziękujemy za przesłaną wiadomość.</h1><p style=\'text-align: center\'> Odpowiedź prześlemy jak najszybciej to będzie możliwe.</p>'),
(79, 'lbl', 'order_received', 'Order Received', 'Zamówienie zostało przyjęte'),
(80, 'txt', 'order_msg', '<h1>Thank you for your purchase!</h1><p style=\'text-align: center\'>We\'re currently processing your payment and will be in touch shortly.</p>', '<h1>Dziękujemy za złożenie zamówienia w naszym sklepie.</h1><p style=\'text-align: center\'>Wkrótce skontaktujemy się z Tobą z informacją o przesyłce.</p>'),
(81, 'lbl', 'products_made_by', 'All products are designed and hand made by Agaf Design Studios', 'Produkty oferowane w sklepie  są zaprojektowane i ręcznie wykonane w naszym Studio.  '),
(82, 'lbl', 'cups', 'Cups', 'Filiżanki'),
(83, 'lbl', 'im_ceramic', '\"Yes I am a Ceramic Mug\"', '\"Tak, Jestem z Ceramiki\"'),
(84, 'lbl', 'shipping_tracked', 'We send all our products by tracked shipping.', 'Dla bezpieczeństwa wysyłki wszystkie zamówienia wysyłamy drogą monitorowaną '),
(85, 'lbl', 'continue', 'Continue', 'Dalej'),
(86, 'lbl', 'pay', 'Pay', 'Zapłać'),
(87, 'lbl', 'products_designed_by', 'Designed by Agnieszka Fornalewska, produced by Agaf Design Studio.', 'Projekt: Agnieszka Fornalewska, wykonanie i wykończenie Studio Agaf Design'),
(88, 'lbl', 'paypal_info', 'After selecting Pay you will be taken to PayPal\'s secure checkout where you can use your PayPal account or, if you have not signed up with PayPal, most major credit cards.', ''),
(89, 'lbl', 'password', 'Password', 'Hasło'),
(90, 'txt', 'register_thanks', 'Thank you for registering!', 'Hasło'),
(91, 'txt', 'validation_password_pwcheck', 'Please enter a password with a minimum of 8 characters containing letters, numbers and at least one special character (e.g. >,*,{,etc.)', ''),
(93, 'txt', 'address-line1', 'Address Line 1', ''),
(95, 'txt', 'address-line2', 'Address Line 2', ''),
(96, 'txt', 'country', 'Country', ''),
(97, 'txt', 'postcode', 'Postcode', ''),
(99, 'txt', 'town_city', 'Town / City', ''),
(101, 'txt', 'add_address', 'Add Address', ''),
(102, 'txt', 'edit_address', 'edit_address', '');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `attempts` tinyint(4) NOT NULL DEFAULT '1',
  `lastlogin` datetime NOT NULL,
  `username` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip`, `attempts`, `lastlogin`, `username`) VALUES
(1, '130.204.17.138', 1, '2017-08-16 20:28:44', 'ivanzanev477@gmail.com'),
(4, '130.204.17.138', 3, '2017-08-30 16:04:18', 'simpleideasweb@gmail.com'),
(5, '130.204.17.138', 2, '2017-08-11 20:21:23', 'ss'),
(6, '130.204.17.138', 2, '2017-08-11 20:22:10', ''),
(7, '130.204.17.138', 1, '2017-08-11 20:22:15', 'simpleideaswe'),
(8, '185.18.177.46', 1, '2017-09-01 10:55:28', 'support@link2light.com'),
(9, '185.18.177.46', 1, '2017-09-03 08:16:26', 'contact@link2light.com'),
(10, '43.252.213.38', 3, '2017-09-03 09:21:36', 'hm.ych111@hotmail.com'),
(11, '27.60.4.184', 1, '2017-09-03 09:54:08', 'rahuljain1991@live.com');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` smallint(6) NOT NULL,
  `hash` varchar(100) NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `customer_email` varchar(255) NOT NULL,
  `status` enum('ap','pr','p','s') NOT NULL COMMENT 'awaiting payment, payment received, processed, sent',
  `created_at` datetime DEFAULT NULL,
  `status_change_date` datetime DEFAULT NULL,
  `shipping_courier` enum('dpd','pp') NOT NULL,
  `tracking_no` varchar(16) NOT NULL DEFAULT '',
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `address_primary` varchar(60) NOT NULL,
  `address_secondary` varchar(60) NOT NULL,
  `country` varchar(30) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `city` varchar(60) NOT NULL,
  `vatnr` varchar(60) DEFAULT NULL,
  `company_name` varchar(60) DEFAULT NULL,
  `firstname_inv` varchar(60) DEFAULT NULL,
  `lastname_inv` varchar(60) DEFAULT NULL,
  `address1_inv` varchar(60) DEFAULT NULL,
  `address2_inv` varchar(60) DEFAULT NULL,
  `country_inv` varchar(30) DEFAULT NULL,
  `postcode_inv` varchar(60) DEFAULT NULL,
  `city_inv` varchar(60) DEFAULT NULL,
  `total_price` decimal(13,2) NOT NULL,
  `shipping_price` decimal(13,2) NOT NULL,
  `grand_total_price` decimal(13,2) NOT NULL,
  `price_currency` varchar(3) NOT NULL,
  `lang` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `hash`, `customer_id`, `customer_email`, `status`, `created_at`, `status_change_date`, `shipping_courier`, `tracking_no`, `first_name`, `last_name`, `address_primary`, `address_secondary`, `country`, `postcode`, `city`, `vatnr`, `company_name`, `firstname_inv`, `lastname_inv`, `address1_inv`, `address2_inv`, `country_inv`, `postcode_inv`, `city_inv`, `total_price`, `shipping_price`, `grand_total_price`, `price_currency`, `lang`) VALUES
(50, '625aac6ac4923259bb57bbbac778a485', 33, '', 'ap', NULL, NULL, 'dpd', '', 'Ivan', 'Zanev', 'ul. 123897', 'ul. 239827987', 'GBR', '213312', 'Ipswitch', '', '', 'Ivan', 'Zanev', 'Burgas, Bulgaria', 'fewefwfwe', 'ARM', '8000', 'Burgas', '35.00', '19.00', '54.00', 'EUR', 'en'),
(51, '56090edf17de4315fdef97427e9e623f', 0, '', 'ap', NULL, NULL, 'dpd', '', 'Bob', 'No Reg', 'The Street 1', '', 'GBR', 'E3 3NH', 'London', '', '', '', '', '', '', 'GBR', '', '', '44.00', '19.00', '63.00', 'EUR', 'en'),
(52, '75546df861267090b07f11c67f9fdbec', 0, '', 's', NULL, '2017-08-16 18:22:28', 'dpd', '12345697', 'Mike', 'Regatcheckout', 'The address 4', '', 'GBR', 'E3 3NH', 'Town', '', '', '', '', '', '', 'GBR', '', '', '44.00', '19.00', '63.00', 'EUR', 'en'),
(53, '2085fbb3f7021b5c01f53ef6a189d6d8', 33, '', 'ap', NULL, NULL, 'dpd', '', 'Ivan', 'Zanev', 'ul. 12938789', 'ul. 239827987', 'GBR', '213312', 'Ipswitch', '', '', 'Ivan', 'Zanev', 'Burgas, Bulgaria', 'ul. 239827987', 'BGR', '8000', 'Burgas', '35.00', '19.00', '54.00', 'EUR', 'en'),
(54, '3f2c545a0f62e8f1f3141e5464635067', 0, 'simpleideasweb@gmail.com', 'ap', NULL, NULL, 'dpd', '', '', '', '', '', 'AFG', '', '', '', '', '', '', '', '', 'GBR', '', '', '35.00', '19.00', '54.00', 'EUR', 'en'),
(55, '0261eb5c83b171514dd1628e39591e23', 40, '', 'ap', NULL, NULL, 'dpd', '', 'Bob', 'Beforecheckout', 'New Address 1', '', 'ALB', '22443', 'New Town', '', '', 'Bob', 'Beforecheckout', 'The address of Bob', '', 'ALB', 'E3 3NH', 'The Town of Bob', '44.00', '19.00', '63.00', 'EUR', 'en'),
(56, '974d8a1a003caf183db00b6b8e3bb528', 40, '', 'ap', NULL, NULL, 'dpd', '', 'Bob', 'BeforeCheckout', '123', '', '', '12345', 'Test', '', '', 'Bob', 'Beforecheckout', 'The address of Bob', '', 'ALB', 'E3 3NH', 'The Town of Bob', '35.00', '19.00', '54.00', 'EUR', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `product_id` int(3) NOT NULL,
  `order_id` smallint(6) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_price` decimal(13,2) NOT NULL,
  `currency` varchar(3) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_product`
--

INSERT INTO `order_product` (`id`, `product_id`, `order_id`, `qty`, `product_price`, `currency`, `created_at`) VALUES
(116, 1, 44, 2, '0.00', '', '2017-08-02 14:25:45'),
(117, 1, 45, 2, '0.00', '', '2017-08-02 14:26:47'),
(118, 1, 46, 2, '0.00', '', '2017-08-02 14:27:15'),
(120, 1, 47, 2, '0.00', '', '2017-08-02 17:29:13'),
(121, 1, 48, 1, '0.00', '', '2017-08-02 18:30:05'),
(132, 1, 49, 1, '35.00', 'EUR', '2017-08-05 21:10:54'),
(134, 7, 51, 1, '44.00', 'EUR', '2017-08-15 04:40:38'),
(135, 1, 50, 2, '35.00', 'EUR', '2017-08-15 12:16:05'),
(136, 7, 52, 1, '44.00', 'EUR', '2017-08-16 16:50:29'),
(138, 1, 53, 1, '35.00', 'EUR', '2017-08-18 20:31:09'),
(139, 1, 54, 1, '35.00', 'EUR', '2017-08-21 15:53:23'),
(142, 7, 55, 1, '44.00', 'EUR', '2017-09-03 07:26:19'),
(143, 1, 56, 1, '35.00', 'EUR', '2017-09-03 11:00:54');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `productid` int(3) NOT NULL,
  `orderby` int(4) NOT NULL,
  `forsale` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `product_name_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_url_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_name_pl` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_url_pl` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `product_cat_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_cat_pl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_description_en` text COLLATE utf8_unicode_ci NOT NULL,
  `product_description_pl` text COLLATE utf8_unicode_ci NOT NULL,
  `product_price_en` int(3) NOT NULL,
  `product_price_pl` int(3) NOT NULL,
  `product_images_en` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `product_images_pl` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `stock_level` int(3) NOT NULL,
  `lead_time` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1 - 4',
  `shipping_alone_poland_en` int(3) NOT NULL,
  `shipping_with_other_product_poland_en` int(3) NOT NULL,
  `shipping_alone_europe_en` int(3) NOT NULL,
  `shipping_with_other_product_europe_en` int(3) NOT NULL,
  `shipping_alone_worldwide_en` int(3) NOT NULL,
  `shipping_with_other_product_worldwide_en` int(3) NOT NULL,
  `shipping_alone_poland_pl` int(3) NOT NULL,
  `shipping_with_other_product_poland_pl` int(3) NOT NULL,
  `shipping_alone_europe_pl` int(3) NOT NULL,
  `shipping_with_other_product_europe_pl` int(3) NOT NULL,
  `shipping_alone_worldwide_pl` int(3) NOT NULL,
  `shipping_with_other_product_worldwide_pl` int(11) NOT NULL,
  `catalogue_nr` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `cat_title_en` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cat_title_pl` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `cat_desc_en` text COLLATE utf8_unicode_ci NOT NULL,
  `cat_desc_pl` text COLLATE utf8_unicode_ci NOT NULL,
  `bullet_points_en` text COLLATE utf8_unicode_ci NOT NULL,
  `bullet_points_pl` text COLLATE utf8_unicode_ci NOT NULL,
  `rrp_en` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `wsale_en` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `rrp_pl` varchar(9) COLLATE utf8_unicode_ci NOT NULL,
  `wsale_pl` varchar(9) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`productid`, `orderby`, `forsale`, `product_name_en`, `product_url_en`, `product_name_pl`, `product_url_pl`, `product_cat_en`, `product_cat_pl`, `product_description_en`, `product_description_pl`, `product_price_en`, `product_price_pl`, `product_images_en`, `product_images_pl`, `stock_level`, `lead_time`, `shipping_alone_poland_en`, `shipping_with_other_product_poland_en`, `shipping_alone_europe_en`, `shipping_with_other_product_europe_en`, `shipping_alone_worldwide_en`, `shipping_with_other_product_worldwide_en`, `shipping_alone_poland_pl`, `shipping_with_other_product_poland_pl`, `shipping_alone_europe_pl`, `shipping_with_other_product_europe_pl`, `shipping_alone_worldwide_pl`, `shipping_with_other_product_worldwide_pl`, `catalogue_nr`, `cat_title_en`, `cat_title_pl`, `cat_desc_en`, `cat_desc_pl`, `bullet_points_en`, `bullet_points_pl`, `rrp_en`, `wsale_en`, `rrp_pl`, `wsale_pl`) VALUES
(1, 10, 'Y', '\"Yes I am a Ceramic Mug\" with real gold', 'im-ceramic-mug-gold-330ml', 'Kubek „Tak, Jestem z Ceramiki” - zdobiony złotem', 'jestem-z-ceramiki-zdobiony-zlotem', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\n\n<p>Please note that, as with all metallic paint finished ceramics, dishwashers should be avoided.</p>', '<p>Odlewane, szkliwione ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każdy jest przez to unikalny. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Kubki odporne są na pękania, nie \"zachodzą\" trwale. Można je z łatwością myć ręcznie. Malowane ręcznie złotem. Nie zaleca się myć w zmywarkach! </p>', 35, 156, 'gold-promo-20-aug2017en.jpg,gold-std-5.jpg, gold-std-1.jpg, gold-std-3.jpg, gold-std-6.jpg, gold-std-9-en.jpg', 'gold-promo-20-aug2017pl.jpg,gold-std-5.jpg, gold-std-1.jpg, gold-std-3.jpg,  gold-std-6.jpg, gold-std-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 40, 25, 80, 50, '001', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\r\n\r\n<p>Color: White, hand painted with high quality genuine gold.</p>', 'Odlewane i szkliwione i ręcznie. Dekorowany ręcznie, techniką nakładania pędzlem, każdy jest przez to unikalny  nie ma dwóch takich samych. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Kubki odporne są na pękania, nie \"zachodzą\" trwale. Można je z łatwością myć ręcznie. <b>Malowane ręcznie wysokiej jakości prawdziwym złotem.</b>', 'Colour: White, hand painted with high quality genuine gold; Weight: 200g; Height: 9 cm; Width: 8 cm; Volume: 330 ml; Material: Earthenware', 'Kolor: Biały, dekorowany prawdziwym złotem; Pojemność: 350 ml; Waga: 200g; Wysokość: 9 cm; Średnica: 8 cm; Materiał: Ceramika', '44.00', '23.25', '195,00', '103,50'),
(7, 20, 'Y', '\"Yes I am a Ceramic\" with real platinum', 'im-ceramic-mug-platinum-330ml', 'Kubek „Tak, Jestem z Ceramiki” - zdobiony platyną', 'bialy-kubek-jestem-z-ceramiki-platyna', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually lightweight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\n\n<p>Please note that, as with all metallic paint finished ceramics, dishwashers should be avoided.</p>', '<p>Odlewane, szkliwione ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każdy jest przez to unikalny. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Kubki odporne są na pękania, nie \"zachodzą\" trwale. Można je z łatwością myć ręcznie! Malowane ręcznie wysokiej jakości prawdziwą platyną. Nie zaleca się myć w zmywarce!</p>', 44, 195, 'platinum-std-7.jpg, platinum-std-4.jpg, platinum-std-1.jpg, platinum-std-3.jpg, platinum-std-6.jpg, platinum-std-9-en.jpg', 'platinum-std-7.jpg, platinum-std-4.jpg, platinum-std-1.jpg, platinum-std-3.jpg,  platinum-std-6.jpg, platinum-std-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '002', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually lightweight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewana, szkliwiona ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każda jest przez to unikalna. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wypał (1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie. Malowana ręcznie wysokiej jakości prawdziwym złotem. Nie zaleca się mycie w zmywarce!</p>', 'Colour: White, hand painted with high quality genuine platinum; Weight: 200g; Height: 9 cm; Width: 8 cm; Volume: 330 ml; Material: Earthenware', 'Kolor: Biały, dekorowany prawdziwą platyną; Waga: 200g; Wysokość: 9 cm; Średnica: 8 cm; Pojemność: 350 ml; Materiał: Ceramika', '44.00', '23.25', '195,00', '103,50'),
(8, 30, 'Y', '\"Yes I am a Ceramic Mug\" with real copper', 'im-ceramic-mug-copper-330ml', 'Kubek „Tak, Jestem z Ceramiki” - zdobiony miedzią', 'bialy-kubek-jestem-z-ceramiki-miedzia', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\n\n<p>Please note that, as with all metallic paint finished ceramics, dishwashers should be avoided.</p>', '<p>Odlewane, szkliwione ręcznie. Dekorowane ręcznie, techniką nakłądania pędzlem przez co każdy jest unikalny. Nie ma dwóch takich samych  Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Kubki odporne są na pękania, nie \"zachodzą\" trwale. Malowane ręcznie miedzią o wyjątkowym połysku. Nie zaleca się myć w zmywarkach!</p>', 44, 195, 'copper-std-7.jpg, copper-std-5.jpg,copper-std-1.jpg, copper-std-2.jpg,  copper-std-6.jpg, copper-std-9-en.jpg', 'copper-std-7.jpg, copper-std-5.jpg, copper-std-1.jpg, copper-std-2.jpg,  copper-std-6.jpg, copper-std-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '003', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', 'Odlewane, szkliwione ręcznie. Dekorowane ręcznie techniką nakładania pędzlem przez co każdy jest unikalny. Nie ma dwóch takich samych. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność dobrej jakości. Kubki odporne są na pękania, nie \"zachodzą\" trwale. Można je z łatwością myć ręcznie. <b>Malowane ręcznie miedzią o wyjątkowym połysku, wypalane trzykrotnie </b> Nie zaleca się myć w zmywarkach!', 'Colour: White, hand painted with high quality genuine copper; Weight: 200g; Height: 9 cm; Width: 8 cm; Volume: 330 ml; Material: Earthenware', 'Kolor: Biały, dekorowany miedzią o pięknym połysku; Waga: 200g; Wysokość: 9 cm; Średnica: 8 cm; Pojemność: 350 ml; Materiał: Ceramika', '44.00', '23.25', '195,00', '103,50'),
(9, 200, 'Y', '\"Yes I am a Ceramic Cup\" with real gold', 'im-ceramic-cup-gold', 'Filiżanka „Tak Jestem z Ceramiki” - zdobiona złotem ', 'filizanka-jestem-z-ceramiki-ze-zlotem', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewana, szkliwiona ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każda jest przez to unikalna. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wypał (1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie. Malowana ręcznie wysokiej jakości prawdziwym złotem.</p>', 27, 120, 'goldcup-promo-20-aug2017en.jpg,gold-sml-1.jpg, gold-sml-3.jpg,  gold-sml-6.jpg, gold-sml-9-en.jpg', 'goldcup-promo-20-aug2017pl.jpg, gold-sml-1.jpg,gold-sml-3.jpg, gold-sml-6.jpg, gold-sml-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '004', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewana, szkliwiona ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każda jest przez to unikalna. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wypał (1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie. Malowana ręcznie wysokiej jakości prawdziwym złotem.', 'Colour: White, hand painted with high quality genuine gold; Weight: 110g; Height: 6 cm; Width: 6 cm; Volume: 150 ml; Material: Earthenware', 'Kolor: Biała dekorowana prawdziwym złotem; Waga: 110g; Wysokość: 6 cm; Średnica: 6 cm; Pojemność: 150 ml; Materiał: Ceramika', '34.00', '17.97', '150,00', '79,27'),
(10, 210, 'Y', '\"Yes I am a Ceramic Cup\" with real platinum', 'im-ceramic-cup-platinum', 'Filiżanka „Tak, Jestem z Ceramiki” - zdobiony platyną', 'filizanka-jestem-z-ceramiki-platyna', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewany i szkliwiony ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każdy jest przez to unikalny. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jesy na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie!. Malowane ręcznie wysokiej jakości prawdziwą platyną. Nie zaleca się myć w zmywarce!</p>', 34, 150, 'platinum-sml-7.jpg, platinum-sml-1.jpg, platinum-sml-3.jpg,  platinum-sml-6.jpg, platinum-sml-9-en.jpg', 'platinum-sml-7.jpg, platinum-sml-1.jpg, platinum-sml-3.jpg, platinum-sml-6.jpg, platinum-sml-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '005', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewany i szkliwiony ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każdy jest przez to unikalny. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jesy na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie!. Malowane ręcznie wysokiej jakości prawdziwą platyną. Nie zaleca się myć w zmywarce!', 'Colour: White, hand painted with high quality genuine platinum; Weight: 110g; Height: 6 cm; Width: 6 cm; Volume: 150 ml; Material: Earthenware', 'Kolor: Biała, dekorowana prawdziwą platyną; Waga: 110g; Wysokość: 6 cm; Średnica: 6 cm; Pojemność: 150 ml; Materiał: Ceramika', '34.00', '17.97', '150,00', '79,27'),
(11, 220, 'Y', '\"Yes I am a Ceramic Cup\" with real copper', 'im-ceramic-cup-copper', 'Filiżanka „Tak, Jestem z Ceramiki” - zdobiona miedzią ', 'filizanka-jestem-z-ceramiki-z-miedzia', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewana i szkliwiona ręcznie. Dekoracje malowane ręcznie techniką nakładania pędzlem, więc nie ma dwóch takich samych. Każda jest przez to unikalna. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wypał (1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie. Malowana ręcznie miedzią o pięknym połysku.</p>\n', 34, 150, 'copper-sml-7.jpg,copper-sml-1.jpg, copper-sml-3.jpg,  copper-sml-6.jpg, copper-sml-9-en.jpg', 'copper-sml-7.jpg, copper-sml-1.jpg, copper-sml-3.jpg,  copper-sml-6.jpg, copper-sml-9.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '006', '', '', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewana i szkliwiona ręcznie. Dekoracje malowane ręcznie techniką nakładania pędzlem, więc nie ma dwóch takich samych. Każda jest przez to unikalna. Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wypał (1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzi\" trwale. Można ją z łatwością myć ręcznie. Malowana ręcznie miedzią o pięknym połysku.  ', 'Colour: White, hand painted with high quality genuine copper; Weight: 110g; Height: 6 cm; Width: 6 cm; Volume: 150 ml; Material: Earthenware', 'Kolor: Biała, dekorowana miedzią; Waga: 110g; Wysokość: 6 cm; Średnica: 6 cm; Pojemność: 150 ml; Materiał: Ceramika', '34.00', '17.97', '150,00', '79,27'),
(12, 40, 'Y', '2 x The \"Yes I am a Ceramic Mug\" Classic', 'im-ceramic-mug-classic-pair', 'Komplet 2 Kubki - \'Tak, Jestem z Ceramiki\' - zdobione czernią', '2-biale-kubki-jestem-z-ceramiki', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\r\n\r\n<p>Color: White with hand painted black finish.</p>\r\n\r\n<p>Designed by Agnieszka Fornalewska, produced by Agaf Design Studio.</p> ', '<p>Komplet - 2 Kubki \"Tak, Jestem z Ceramiki\".</p>\n<p>Odlewane, szkliwione i dekorowane ręcznie. Dekoracje malowane techniką nakładania pędzlem. Nie ma dwóch takich samych. Każdy jest unikalny!</p>\n\n<p>Ścianki odlewane cienko co dodaje im lekkości i pozwala na lepszą ergonomię kubka. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność trwałości i dobrej jakości. Kubki odporne są na pękanie, nie \"zachodzą\" trwale. </p>', 43, 190, '100-classic-lead.jpg, classic-10-en.jpg, 100-classic-2-en.jpg, 100-classic-4.jpg', '100-classic-lead.jpg, classic-10-pl.jpg, 100-classic-9.jpg, 100-classic-4.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '007', '\"Yes I am a Ceramic Mug\" Classic', 'Kubek „Tak, Jestem z Ceramiki” - zdobiony czernią', '<p>Cast, glazed and decorated by hand so no two are the same, every mug is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>\r\n', 'Odlewany, szkliwiony i dekorowany ręcznie. Dekoracje malowane techniką nakładania pędzlem. Nie ma dwóch takich samych. Każdy jest unikalny!\r\n\r\nŚcianki odlewane cienko co dodaje mu lekkości i pozwala na lepszą ergonomię kubka. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność trwałości i dobrej jakości. Kubek odporny jest na pękanie, nie \"zachodzi\" trwale.', 'Colour: White with hand painted black finish; Weight: 200g; Height: 9 cm; Width: 8 cm; Volume: 330 ml; Material: Earthenware', 'Kolor: Biały zdobiony czernią; Waga: 200g; Wysokość: 9 cm; Średnica: 8 cm; Pojemność: 350 ml; Materiał: Ceramika', '21.50', '11.36', '95,00', '50,20'),
(13, 230, 'Y', '2 x The \"Yes I am a Ceramic Cup\" Classic', 'im-ceramic-cup-classic-pair', '2 Filiżanki  \"Tak, Jestem z Ceramiki\" ', '2-biale-filizanki-jestem-z-ceramiki', 'Drinkware', 'Drinkware', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\n\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', '<p>Odlewane, szkliwione i dekorowane ręcznie. Dekoracje malowane ręcznie więc nie ma dwóch takich samych. Każda jest przez to unikalna! Wymiary: Szer. 60 mm x wys. 60 mm, poj. ok. 150 ml. Idealne na poranne double espresso lub poobiedni krem brule. Ścianki odlewane cienko co dodaje im lekkości i pozwala na lepszą ergonomię kubka. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Kubki odporne są na pękanie, nie \"zachodzą\" trwale. Można je z łatwością myć w zmywarkach, włożyć do piekarnika czy mikrofali!</p>', 27, 130, '100-classic-sml-7.jpg, 100-classic-sml-4.jpg, 100-classic-sml-1-en.jpg, 100-classic-sml-9b-en.jpg', '100-classic-sml-7.jpg, 100-classic-sml-4.jpg,100-classic-sml-9-pl.jpg, 100-classic-sml-9a.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '008', '\"Yes, I am a Ceramic Cup\" Classic ', 'Filiżanka „Tak, Jestem z Ceramiki” zdobiona czernią', '<p>Cast, glazed and decorated by hand so no two are the same, every cup is a unique one off piece. The meticulously thought through design is perfectly ergonomic including thin walls that create an unusually light weight ceramic.</p>\r\n\r\n<p>Fired to 1,060°C with a carefully selected glaze ensures the highest quality of finishes that is both crack and stain resistant.</p>', 'Odlewana i szkliwiona ręcznie. Dekoracje malowane ręcznie techniką nakładania pędzlem. Nie ma dwóch takich samych. Każda jest unikalna.  Ścianki odlewane cienko co dodaje im lekkości i lepszej ergonomi. Wysoki wypał (do 1060 stopni ) i dobrane idealnie szkliwo daje pewność wysokiej jakości. Filiżanka odporna jest na pękania, nie \"zachodzą\" trwale. Można ją z łatwością myć ręcznie. ', 'Colour: White with hand painted black finish; Weight: 110g; Height: 6 cm; Width: 6 cm; Volume: 150 ml; Material: Earthenware', 'Kolor: Biały, zdobiona klasyczną czernią; Waga: 110g; Wysokość: 6 cm; Średnica: 6 cm; Pojemność: 150 ml; Materiał: Ceramika', '13.50', '7.13', '65,00', '34,35'),
(14, 240, 'Y', 'Pure Art Cup', 'pure-art-cup', 'Filiżanka „Pure Art”', 'filizanka-pure-art', 'Drinkware', 'Drinkware', '<p>The Pure Art Cup form is, like all others created in our studio, not custom. The small ceramic mug is transferred to another dimension, creating an unconventional, multi-functional piece.</p>\n\n<p>The prototype was exhibited in Gdynia (2013), Warsaw (2015) and London (2015) before entering production.</p>\n\n<p>Cast in a high quality ceramic material, it is perfectly glazed pure white.</p>', '<p>Filiżanka Pure Art to forma użytkowa, która jest, podobnie jak wszystkie inne powstające w naszym Studio na pewno niestandardowa. Mały ceramiczny kubek zostaje przeniesiony w inny wymiar tworząc niekonwencjonalną, wielofunkcyjną formę nie łatwą do \"zaszuflatkowania\".</p>\n\n<p>Prototyp projektu pokazywany był na festiwalowych i wystawach designu w Gdyni 2013, Warszawie 2014 i Londynie 2015.</p>\n\n<p>Odlana z wysokiej jakości materiału ceramicznego, poszkliwiona jest idealnie dopasowanym szkliwem w kolorze czystej bieli.</p>', 40, 165, 'pure-art-1.jpg, pure-art-2.jpg, pure-art-3.jpg, pure-art-4.jpg, pure-art-5.jpg', 'pure-art-1.jpg, pure-art-2.jpg, pure-art-3.jpg, pure-art-4.jpg, pure-art-5.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '009', '', '', '<p>The Pure Art Cup form is, like all others created in our studio, not custom. The small ceramic mug is transferred to another dimension, creating an unconventional, multi-functional piece.</p>\r\n\r\n<p>The prototype was exhibited in Gdynia (2013), Warsaw (2015) and London (2015) before entering production.</p>\r\n\r\n<p>Cast in a high quality ceramic material, it is perfectly glazed pure white.</p>', '<p>\"Pure Art\"to forma użytkowa, która jest, podobnie jak wszystkie inne powstające w naszym Studio na pewno niestandardowa. Mały ceramiczny kubek zostaje przeniesiony w inny wymiar tworząc niekonwencjonalną, wielofunkcyjną formę nie łatwą do \"zaszuflatkowania\".</p>\r\n\r\n<p>Prototyp projektu pokazywany był na festiwalowych i wystawach designu m. in. w Gdyni 2013, Warszawie 2014 czy Londynie 2015.</p>\r\n  \r\n<p>Odlana z wysokiej jakości materiału ceramicznego, poszkliwiona idealnie dopasowanym szkliwem w kolorze czystej bieli.</p>\r\n', 'Colour: White; Diameter: 7.5cm / 3\"; Width (with handle): 10cm / 3.9\"; Height: 15.5cm / 6\"; Volume: 160ml / 5.5oz; Material: Ceramic', 'Kolor: Biały; Waga: g; Średnica korpusu i ucha: 10cm; Średnica korpusu bez ucha: 7.5cm; Wysokość 15.5cm; Pojemność: 160ml; Materiał: Ceramika ', '', '21.14', '', '87,20'),
(15, 110, 'Y', 'The \"Delicious\" Can', 'delicious-can', 'Kubek - Puszka Delikates - zdobiona złotem', 'puszka-delikates', 'Drinkware', 'Drinkware', '<p>From the \"Delicious\" collection this piece is hand cast and glazed. The unusual porcelain used in its production creates a warm off-white colour and the carefully selected glaze delivers a pearl like luster.</p>\n\n<p>The mug has thin, translucent walls which add to its overall lightweight feel. Delicate hand decoration with real gold emphasizes its uniqueness.</p>', '<p>Kubek z kolekcji \"Delikatesy\".Jest odlewany i szkliwiony ręcznie. Porcelana użyta do produkcji ma unikalną lekko łamaną biel w ciepłym odcieniu. Odpowiednio dobrane, nietypowe, lekko błyszczące szkliwo daje satynowo- perłowy połysk. Kubek ma cieńkie, prześwitujące ścianki co dodaje mu lekkoci. Delikatne, ręczne zdobienia złotem i ciepła biel powierzchni podkreślają jego wyjątkowość.</p>', 28, 115, 'delicious-can.jpg, delicious-can-1.jpg, delicious-can-2.jpg, delicious-can-3.jpg', 'delicious-can.jpg, delicious-can-1.jpg, delicious-can-2.jpg, delicious-can-3.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 20, 10, 45, 30, 80, 50, '010', '', '', '<p>From the \"Delicious\" collection this piece is hand cast and glazed. The unusual porcelain used in its production creates a warm off-white colour and the carefully selected glaze delivers a pearl like luster.</p>\r\n\r\n<p>The mug has thin, translucent walls which add to its overall lightweight feel. Delicate hand decoration with real gold emphasizes its uniqueness.</p>', '<p>Kubek z kolekcji \"Delikatesy\".Jest odlewany i szkliwiony ręcznie. Porcelana użyta do produkcji ma unikalną lekko łamaną biel w ciepłym odcieniu. Odpowiednio dobrane, lekko błyszczące szkliwo daje satynowo- perłowy połysk. Kubek ma cieńkie, prześwitujące ścianki co dodaje mu lekkoci. Delikatne, ręczne zdobienia złotem i ciepła biel powierzchni podkreślają jego wyjątkowość.</p>', 'Color: Warm white, satin glaze, hand painted with high quality genuine gold; Weight: 130 grams / 4.5 oz; Height: 9.3 cm / 3.7\"; Diameter: 6.5cm / 2.6\"; Volume: 200 ml / 6.8 fl. oz; Material: Porcelain.', 'Kolor: Cipła biel, satynowe wykończenie, dekorowany złotem; Waga: 130 g; Wysokość: 9,3 cm; Średnica: 6,5 cm; Pojemność: 200 ml; Materiał: Porcelana', '', '14.80', '', '60,77'),
(16, 520, 'Y', '\"Ripple Plate\"', 'ripple-plate', 'Talerz \"Ripple\"', 'rippled-plate-pl', 'Tableware', 'Tableware', '<p>Agaf Design\'s plates collection is a \"tasting\" of new forms to enhance our visual and culinary experiences.and experiences.</p>\n\n<p>In designing we strive to reject old, classified possibilities and see out non-standard solutions. The Ripple Plate is a perfect example. It underscores the uniqueness of each and every simple and sophisticated dish, contributing to the presentation.</p>\n\n<p>The name inspires recipients\' associations with the preparatory stage, the shape of a water\'s surface when disturbed by falling droplet of water.</p>dropping water on the water.</p> \n\n<p>Each plate is hand-molded and glazed.</p>\n', '<p>Kolekcje talerzy Agaf Design to \"degustacja\" nowych form i doznań których mogą nam dostarczać również formy codziennego użytku.</p>  \n\n<p>W projektowaniu staramy się odkywać nowe, nieklasyczne możliwości i poszukiwać niestandardowych rozwiązań. Talerz Ripple idealnie spełnił nasze założenia. Podkreśla wyjątkowość każdego, zarówno prostego jak i wyszukane dania, współkreując jego prezentację.</p> \n\n<p>Nazwę zainspirowały skojarzenia odbiorców na etapie prac przygotowawczych, z kształtem jaki zostawia na wodzie spadająca kropla.</p>\n\n<p>Każdy z talerzy jest ręcznie formowany i szkliwiony. Mogą wystąpić nieznaczne różnice w jego wykończeniu  i grubości,</p>\n ', 60, 245, 'ripple-plate-1.jpg, ripple-plate-6.jpg, ripple-plate-3.jpg, ripple-plate-2.jpg, ripple-plate-4.jpg, ripple-plate-5.jpg', 'ripple-plate-1.jpg, ripple-plate-6.jpg, ripple-plate-3.jpg, ripple-plate-2.jpg, ripple-plate-4.jpg, ripple-plate-5.jpg', 10, '4 - 6', 10, 6, 14, 9, 22, 15, 35, 20, 60, 35, 90, 60, '011', '', '', '', 'Projektując formę staramy się odkywać nowe możliwości i poszukiwać niestandardowych rozwiązań. Talerz Ripple idealnie spełnił nasze założenia. Podkreśla wyjątkowość każdego, zarówno prostego jak i wyszukane dania, współkreując jego prezentację. \r\n\r\nNazwę talerza zainspirował jego wygląd, który kojarzy się z kształtem jaki zostawia na lustrze wody spadająca kropla.\r\n\r\nKażdy z talerzy jest ręcznie formowany i szkliwiony. Mogą wystąpić nieznaczne różnice w jego wykończeniu  i grubości,\r\n\r\n', 'Color: White; Diameter 28cm; Thickness: 0.01 to 0.02 cm; Material: Stoneware.', 'Kolor: Biały; Wymiary: Średnica 28 cm; Grubość od 0,01 do 0,02 cm; Materiał: Kamionka', '', '31.71', '', '129,47'),
(17, 530, 'Y', '\"Bowl not a Bowl\"', 'bowl-not-bowl', '\"Bowl not a Bowl\"', 'bowl-not-bowl-pl', 'Tableware', 'Tableware', '<p>This courageous, and at the same time perverse, pronunciation of form, will not only draw your guests attention but will also be a part of your daily essence. It can be a unique decoration on any table but it is also perfect to serve any dish, salad or dessert.</p>\n\n<p>\"Bowl not a Bowl\" is made freehand with no form to follow so no two are alike. Each one is unique.</p>\n\n<p>Thin walls, high heat firing (1,240 degrees) and a perfectly selected glaze give you confidence that this is a product of the highest quality.</p>\n\n<p>Lead Time: This product is made to order in approximately 4-6 weeks. We occasionally have a small selection in stock, please contact us for photos of current models.</p>', '<p>Ta odważna i zarazem przekorna w swej wymowie forma, nie tylko zwróci uwagę twoich gości iale też z powodzeniem będzie częścią twojej codziennej zastawy. Może być wyjątkową ozdobą każdego stołu, ale również idealnie sprawdzić się w serwowaniu wszelkich dań, sałatek czy deserów. </p>\n\n<p>\"Bowl not a Bowl\" to forma wykonywana ręcznie, dlatego nie ma dwóch o identycznie wyprofilowanym brzegu. Każada jest unikalna.</p> \n\n<p>Cienkie ścianki, wysoki wypał (1240 stopni) i idealnie dobrane szkliwo daje pewność wysokiej jakości produktu.</p>\n\n<p>Termin Realizacji: na zamówienie 4 - 6 tygodni (w celu zakupu egzemplarzy dostępnych \"od ręki\" proszę o kontakt\")</p>', 79, 325, 'bowl-not-bowl-1.jpg, bowl-not-bowl-2.jpg, bowl-not-bowl-5.jpg, bowl-not-bowl-4.jpg, bowl-not-bowl-6.jpg, bowl-not-bowl-7.jpg, bowl-not-bowl-8.jpg', 'bowl-not-bowl-1.jpg, bowl-not-bowl-2.jpg, bowl-not-bowl-5.jpg, bowl-not-bowl-4.jpg, bowl-not-bowl-6.jpg, bowl-not-bowl-7.jpg, bowl-not-bowl-8.jpg', 10, '4 - 6', 10, 6, 14, 9, 22, 15, 35, 20, 60, 35, 90, 60, '012', '', '', '', 'Ta odważna i zarazem przekorna w swojej wymowie forma, nie tylko zwraca uwagę ale również z powodzeniem może być częścią codziennej zastawy.\r\n \r\nIdealnie sprawdza się w serwowaniu wszelkich dań, sałatek czy deserów. \r\n\r\nFormowana i szkliwiona ręcznie, Każda wyprofilowanie może się różnić dla pokreślenia jej  unikalności.\r\nCienkie ścianki, wysoki wypał (1240 stopni) i idealnie dobrane szkliwo daje pewność wysokiej jakości produktu.\r\n', 'Color: White; Dimensions: Approximately 30cm x 20cm; Thickness: 0.01 cm; Material: Earthernware', 'Kolor: Biały; Wymiary: ok. 30/33 cm x 17/18 cm; Waga: ok.xxg (Wzależności od wielkości formy)  ;Grubość:ok: 0,01 cm; Materiał: Kamionka', '', '41.75', '', '171,75'),
(18, 540, 'Y', 'Crispy Plate', 'crispy-plate', 'Crispy Plate', 'crispy-plate-pl', 'Tableware', 'Tableware', '', '', 155, 650, 'crispy-plate-5.jpg, crispy-plate-2.jpg, crispy-plate-1.jpg', 'crispy-plate-5.jpg, crispy-plate-2.jpg, crispy-plate-1.jpg', 10, '4 - 6', 10, 6, 14, 9, 22, 15, 35, 20, 60, 35, 90, 60, '013', '', '', '', '', '', '', '', '81.91', '', '343,50'),
(19, 250, 'Y', 'The \"Kap Kap\" Cup', 'kap-kap-cup', 'Filiżanka „Kap Kap”', 'filizanka-kap-kap', 'Drinkware', 'Drinkware', '<p>The porcelain Kap Kap cup is produced using a unique off white porcelain and a satin-pearl glaze before adding a delicate decoration of real gold to emphasize its uniqueness.</p>\n\n<p>Small, light and translucent. Hand cast, glazed and painted to ensure thin walls and a slender finish suitable for any occasion.</p>\n\n<p>Ideal not only for a morning espresso, an evening nightcap and every beverage in between.</p>', '<p>Porcelanowa Filiżanka Kap Kap - delikatnie zdobiona prawdziwym złotem</p>\n<p>Unikalna biel porcelany w ciepłym odcieniu, lekko błyszczące szkliwo o satynowo - perłowym połysku i delikatne muśnięcie złotem podkreśla jej wyjątkowość.</p> \n\n<p>Małą, lekka i prześwitująca. Odlana i poszkliwiona ręcznie zdobiona. Cienkie ścianki i lekka prosta forma, pozwala na jej uniwarsalne zastosowanie. Idealna nie tylko na poranne espresso czy wykwintną \"szklaneczkę\" wieczornych specjałów.</p>', 28, 115, 'kap-kap-1.jpg, kap-kap-2.jpg, kap-kap-3.jpg, kap-kap-4.jpg', 'kap-kap-1.jpg, kap-kap-2.jpg, kap-kap-3.jpg, kap-kap-4.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 30, 20, 45, 30, 80, 50, '014', '', '', '', 'Porcelanowa Filiżanka Kap Kap - zdobiona prawdziwym złotem\r\nUnikalna biel porcelany w ciepłym odcieniu, lekko błyszczące szkliwo o satynowo - perłowym połysku i delikatne muśnięcie złotem podkreśla jej wyjątkowość. \r\n\r\nMałą, lekka i prześwitująca. Odlana i poszkliwiona ręcznie zdobiona. Cienkie ścianki i lekka prosta forma, pozwala na jej uniwarsalne zastosowanie. Idealna nie tylko na poranne espresso czy wykwintną \"szklaneczkę\" wieczornych specjałów.\r\n\r\n\r\n\r\n\r\n', 'Color: Off-white with satin glaze; Weight: 100g (3.5 oz); Height: 9cm; Width: 6.5 cm; Volume: 120ml (4 US fl. oz.); Material: Porcelain ', 'Kolor: Ciepła biel, szkliwo transparentne z satynowo- perłowym połyskiem; Średnica: 6.5cm; Wysokość 9cm; Pojemność: 120ml; Materiał: Porcelan', '', '22.76', '', '60,77'),
(20, 95, 'Y', 'Fancy Can', 'fancy-can', 'Kubek - Puszka Fancy', 'fancy-can-pl', 'Drinkware', 'Drinkware', '<p>A unique ceramic mug designed and handmade at the Agaf Design Studio. The impressive appearance, lightness and perfect ergonomics of the piece highlight the careful production and finish.</p>\n\n<p>The precisely profiled handle is not only decorative but superbly comfortable to use.</p>\n\n<p>The simple outline of the body, shaped as a metal can, combined with the stylish handle, together create a unique effect. The symbolism of the project becomes self evident: two seemingly different worlds combine to form a surprising but very coherent and, above all, functional whole.</p>', '<p>Unikalny kubek ceramiczny, zaprojektowany I wykonany w Studio Agaf Design. Efektowny wygląd, lekkość I prawidłowa ergonomia kubka, podkreśla staranne wykończenie i cienko odlewane ścianki. Pieczołowicie wyprofilowane i wykonane ucho jest nie tylko dekoracyjne, ale również bardzo wygodne w użyciu. Prosty kształt korpusu nawiązujący formą do metalowej puszki, w połączeniu z dekoracyjnym, stylizowanym uchem, tworzą razem wyjątkowy efekt. Symbolika projektu wydaje się być oczywista: dwa z pozoru inne światy, w połączeniu tworzą zaskakującą lecz bardzo spójną, a przede wszystkim, funkcjonalną całość.</p>', 23, 95, 'fancy-can-4.jpg, fancy-can-1.jpg, fancy-can-3.jpg, fancy-can-2.jpg', 'fancy-can-4.jpg, fancy-can-1.jpg, fancy-can-3.jpg, fancy-can-2.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 30, 20, 45, 30, 80, 50, '015', '', '', '', '<p>Unikalny kubek ceramiczny, zaprojektowany I wykonany w Studio Agaf Design. Efektowny wygląd, lekkość I prawidłowa ergonomia kubka, podkreśla staranne wykończenie i cienko odlewane ścianki. Pieczołowicie wyprofilowane i wykonane ucho jest nie tylko dekoracyjne, ale również bardzo wygodne w użyciu. Prosty kształt korpusu nawiązujący formą do metalowej puszki, w połączeniu z dekoracyjnym, stylizowanym uchem, tworzą razem wyjątkowy efekt.</p>\r\n\r\n', 'Colour: White; Diameter: 7cm / 2.8\"; Diameter with handle: 12cm / 4.7\"; Height: 10.25cm / 4\"; Volume: 280ml / 9.5 oz volume; Material: Ceramic', 'Kolor: Biały; Waga: 200g; Średnica korpusu i ucha: 12cm; Wysokość 10.25cm; Pojemność ok.: 280ml; Materiał: Ceramika\n\n', '', '12.15', '', '50,20'),
(21, 130, 'N', 'MultiBowl', 'multibowl', 'MultiBowl', 'multibowl-pl', 'Tableware', 'Tableware', '', '', 195, 980, 'multibowl-1.jpg', 'multibowl-1.jpg', 10, '4 - 6', 20, 12, 30, 20, 50, 35, 80, 60, 120, 80, 200, 140, '016', '', '', '', '', '', '', '', '', '', ''),
(22, 105, 'Y', 'Bowl Mug', 'bowl-mug', 'Kubek/ Filiżanka - Bowl', 'bowl-mug-pl', 'Drinkware', 'Drinkware', '<p>Surprisingly in form and comfortable to use, the inspiration for this carefully crafted design is from the exquisite from of an enamel teapot. Both size and shape enhance the value of the shape and its multi-functionality.</p>\n\n<p>Molded and profiled by hand before applying a perfectly selected, high quality, transparent glaze. The entire production process emphasizes attention to detail and a careful finish for a unique product.</p>\n\n<p>Comfortable in use, durable and resistant to cracks and stains.</p>', '<p>Zaskakujący w formie i wygodny w użyciu.</p>\n<p>Inspiracją do tego pieczołowicie wyprofilowanego projektu jest wykwitna forma emaliowanych czajniczków. Zarówno wielkość jak i kształt uwydatniają walory formy i jej wielofunkcyjność. </p>\n<p>Odlewany, profilowany i szkliwiony ręcznie. Wysoki wypał i idealnie dobrane, dobrej jakości, transparentne szklwo podkreśla staranne wykończenie z dbałością o każdy detal unikalnego produktu. Wygodny w użuciu, trwały, nie nagrzewa siię i jest odporny na spękania. </p>', 40, 165, 'bowl-mug-1.jpg, bowl-mug-2.jpg, bowl-mug-4.jpg, bowl-mug-5.jpg', 'bowl-mug-1.jpg, bowl-mug-2.jpg, bowl-mug-4.jpg, bowl-mug-5.jpg', 10, '1 - 4', 5, 4, 12, 7, 19, 12, 30, 20, 45, 30, 80, 50, '017', '', '', '', '<p>Odlewany, profilowany i szkliwiony ręcznie. Wysoki wypał i idealnie dobrane, dobrej jakości, transparentne szklwo podkreśla staranne wykończenie z dbałością o każdy detal unikalnego produktu. Wygodny w użuciu, trwały, nie nagrzewa siię i jest odporny na spękania. </p>', 'Color: Cream; Weight: 175g; Height: 10cm; Width: 15cm; Volume: 280ml; Material: Ceramic', 'Kolor: Kremowy; Waga: 175g; Wysokość: 10,5 cm; Średnica: 15 cm; Pojemność: 280 ml; Materiał: Ceramika', '', '21.14', '', '87,20'),
(23, 560, 'Y', 'Perfect Plate', 'perfect-plate', 'Porcelanowy Talerz  \"Perfekt\"', 'perfect-plate-pl', 'Tableware', 'Tableware', '<p>This eye-pleasing but unconventional form is not accidental. Every millimeter, the smallest part of a bend or contour is exactly measured. Thanks to this it can be used as an elegant saucer for a morning coffee and accompanying sandwich or to enhance the aesthetic presentation of any main dish.</p>\n\n<p>Arranged in groups on larger surfaces they can create mouth-watering, multi-course presentations. On smaller tables they can be skillfully arranged to maximise the available space.</p>\n\n<p>The design is intended for everyday use and produced with the highest quality porcelain and glaze. The shape is modern but timeless, focused on the essence of function and a minimalism for universal applications.</p> \n\n', '<p>Ta przyjemna dla oka, lecz nietypowa dla talerza forma, nie jest przypadkowa. Każdy milimetr, najmniejsza część zagięcia i wgięcia jest dokładnie wymierzona. Dzięki temu może być użyty np. jako podstawka na kubek z kawą w towarzystwie porannej kanapki lub do estetycznej prezentacji dań.</p>\n\n<p>Na większych powierzchniach i w grupie pozwala tworzyć przepysznie kształtne, prezentacje z wielu dań. Na stołach o mniejszej powierzchni, umiejętnie wykorzystuje każdą wolną przestrzeń blatu.</p>\n\n<p>Założenie projektu talerza: forma ceramiczna, do codziennego użytku, w jednolitej kolorystyce z wykorzystaniem wysokiej jakości materiału, kształt – nowoczesny ale ponadczasowy, skupiony na istocie funkcji, minimalizm pozwalający na uniwersalne zastosowanie formy. </p>\n\n', 60, 245, 'perfect-plate-1.jpg, perfect-plate-2a.jpg, perfect-plate-3a.jpg, perfect-plate-4.jpg', 'perfect-plate-1.jpg, perfect-plate-2a.jpg, perfect-plate-3a.jpg, perfect-plate-4.jpg', 10, '4 - 6', 10, 6, 14, 9, 22, 15, 35, 20, 60, 35, 90, 60, '018', '', '', '', 'Ta przyjemna dla oka, lecz nietypowa dla talerza forma, nie jest przypadkowa. Każdy milimetr, najmniejsza część zagięcia i wgięcia jest dokładnie wymierzona. Dzięki temu może być użyty np. jako podstawka na kubek z kawą w towarzystwie porannej kanapki lub do estetycznej prezentacji dań.\r\n\r\nNa większych powierzchniach i w grupie pozwala tworzyć przepysznie kształtne, prezentacje z wielu dań. Na stołach o mniejszej powierzchni, umiejętnie wykorzystuje każdą wolną przestrzeń blatu.\r\n\r\nOdlewany ręcznie, cienko z prześwitującej porcelany o ciepłym odcieniu bieli, wykończenie ręczne szliwem tranparentnym z satynowo - perłowym połyskiem.', 'Color: Off white with satin glaze; Weight: 330g; Width: 30cm; Depth: 18cm; Height: 1.5cm; Thickness: 0.01 to 0.02 cm.  Material: Porcelain', 'Kolor: Ciepła biel, pokryta transparentnym satynowo – perłowym szkliwem; Waga: 330g; Wymiary: 30 x 18 cm; Grugość: od 0,01 - 0,02 cm; Materiał: Prześwitująca porcelana', '', '31.71', '', '129,47'),
(24, 570, 'Y', 'Perfect Bowl', 'perfect-bowl', 'Perfekt Miska', 'perfect-bowl-pl', 'Tableware', 'Tableware', '<p>Slim, irregular, lightweight and with thin, translucent walls this porcelain bowl offers a unique setting for any salad, soup or dessert. Visually stunning as an individual piece as well as a perfect complement to the \"Perfect\" plate. </p>\n\n<p>Hand cast and hand glazed, each bowl is slightly different in shape and profile, each a unique product in their own right.</p>\n\n<p>Hand cast, finished and glazed. Each profile is different, so each one is unique.</p>', '<p>Porcelanowa, smukłą  odrobinę nieregularna, Lekka,o cienkich prześwitujących ściankach jest wyjątkową oprawą do zaserwowania każdej sałatki, zupy czy deseru. Idealnie prezentuje się sama lub w komlecie z porcelanowym talerzem Perfekt.</p>\n\n<p>Ręcznie odlewana, wykańczana i szkliwiona. Wyprofilowanie każdej odrobinę się różni dlatego każda jest unikalna. </p>', 60, 245, 'perfect-bowl-1a.jpg, perfect-bowl-4a.jpg, perfect-bowl-3a.jpg, perfect-bowl-5a.jpg', 'perfect-bowl-1a.jpg, perfect-bowl-4a.jpg, perfect-bowl-3a.jpg, perfect-bowl-5a.jpg', 10, '4 - 6', 10, 6, 14, 9, 22, 15, 35, 20, 60, 35, 90, 60, '019', '', '', '', '<p>Porcelanowa, smukłą  odrobinę nieregularna, Lekka,o cienkich prześwitujących ściankach jest wyjątkową oprawą do zaserwowania każdej sałatki, zupy czy deseru. Idealnie prezentuje się sama lub w komlecie z porcelanowym talerzem Perfekt.</p>\r\n\r\n<p>Ręcznie odlewana, wykańczana i szkliwiona. Wyprofilowanie każdej odrobinę się różni dlatego każda jest unikalna. </p>', 'Color: Off-white with satin glaze; Volume: 700ml; Weight: 250g; Height: 8.5cm; Width: 18cm; Depth: 14.5; Volume: 700ml; Material: Porcelain', 'Kolor: Ciepła biel, pokryta transparentnym satynowo – perłowym szkliwem; Pojemność: 700 ml; Waga: 250g; Długość: 14,5 cm; Szrokość: 18 cm; Wysokość: 8.5 cm; Materiał: Prześwitująca porcelana', '', '31.71', '', '129,47');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_validation_requests`
--
ALTER TABLE `account_validation_requests`
  ADD PRIMARY KEY (`email`,`hash`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forgotten_password_requests`
--
ALTER TABLE `forgotten_password_requests`
  ADD PRIMARY KEY (`email`,`hash`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`langid`),
  ADD UNIQUE KEY `lang_code` (`lang_code`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`productid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `customer_addresses`
--
ALTER TABLE `customer_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `langid` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `productid` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

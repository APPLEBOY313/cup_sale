<?php
require_once('config.php');
//echo "<pre>"; print_r( $url ); echo "</pre>" ; exit();
	$url_arr = explode("=",$url);
	$product_url = $url_arr[1];

	$sql_product = "SELECT * FROM products WHERE product_url_".$lng."='$product_url' ORDER BY orderby ASC";
	
	$result_product = $conn->query($sql_product);
	if ($result_product->num_rows > 0) { 
	
		$row_product = $result_product->fetch_assoc();
		$product_title = $row_product['product_name_'.$lng];
		$product_cat = $row_product['product_cat_'.$lng];
		$product_orderby = $row_product['orderby'];

		$product_cat_en = $row_product['product_cat_en'];
		$product_cat_pl = $row_product['product_cat_pl'];
		$product_url_en = $row_product['product_url_en'];
		$product_url_pl = $row_product['product_url_pl'];
		$price_display = pricedisplay($row_product['product_price_'.$lng],$lng);
		
		$shipping_alone_poland = pricedisplay($row_product['shipping_alone_poland_'.$lng],$lng);
		$shipping_alone_europe =  pricedisplay($row_product['shipping_alone_europe_'.$lng],$lng); 
		$shipping_alone_world =  pricedisplay($row_product['shipping_alone_worldwide_'.$lng],$lng); 
		
		$shipping_multi_poland = pricedisplay($row_product['shipping_with_other_product_poland_'.$lng],$lng); 
		$shipping_multi_europe =  pricedisplay($row_product['shipping_with_other_product_europe_'.$lng],$lng); 
		$shipping_multi_world =  pricedisplay($row_product['shipping_with_other_product_worldwide_'.$lng],$lng); 
		
		$product_imgs_array = explode(",",$row_product['product_images_'.$lng]);
//		echo "<pre>"; print_r( $row_product['productid'] + 1 ); echo "</pre>" ; exit();

		
		$next_product_url = null;
		$sql_next_product = "select * from products where orderby = (select min(orderby) from products where orderby > $product_orderby )";
		$result_next_product = $conn->query($sql_next_product);
		if ($result_next_product->num_rows > 0) { 
			$row_next_product = $result_next_product->fetch_assoc();
			$next_product_url = $row_next_product['product_url_'.$lng];
		}

		$prev_product_url = null;
		$sql_prev_product = "select * from products where orderby = (select max(orderby) from products where orderby < $product_orderby)";
		$result_prev_product = $conn->query($sql_prev_product);
		if ($result_prev_product->num_rows > 0) { 
			$row_prev_product = $result_prev_product->fetch_assoc();
			$prev_product_url = $row_prev_product['product_url_'.$lng];
		}
	}


$title=$product_title;
$meta_desc = "Area 16 - Product Meta Description";

// get category
$product_cats = explode(",",$product_cat);
$this_cat = end($product_cats);
$this_cat_display = ucwords(str_replace("-"," ",$this_cat));

if ($this_cat_display == "Drinkware" && $lng == "pl") { $this_cat_display = "Drinkware (Kubki i Filiżanki)"; }
if ($this_cat_display == "Tableware" && $lng == "pl") { $this_cat_display = "Tableware (Misy, Patery i Talerze)"; }

$this_cat_url = "shop.php?cat=".trim(strtolower($this_cat));

// sort out breadcrumbs
$breadcrumbs = array();
$breadcrumbs ['shop.php'] = $lang['shop-bread']; 
$breadcrumbs [$this_cat_url] = $this_cat_display;
$breadcrumbs ['#'] = $product_title;
$lastElementBreadcrumbs = $product_title;


require_once('header.php');

?>

 
		<div class="breadcrumbs-wrapper">

				<div class="col-xs-9">
					<div class="row">
						<p>
						<?php
						
						foreach ($breadcrumbs as $key => $value) {
						echo "<a href=\"".$key."\">".$value."</a>";
						if ( $value!=$lastElementBreadcrumbs )	{ echo " &gt; "; }								
						}
											
						?>

						</p>
					</div>
				</div>
				<div class="col-xs-3">
					 <div class="row">
						 <div class="prew-next-wrapper">
						 <p>
						 <?php if(!empty($prev_product_url)) {  ?>  <a href="product.php?product=<?php echo $prev_product_url; ?>"><?php echo $lang['main-previous'] ?></a> <?php } ?>
						 <?php if(!empty($prev_product_url) && !empty($next_product_url)) {  ?> / <?php } ?>
						 <?php if(!empty($next_product_url)) {  ?>  <a href="product.php?product=<?php echo $next_product_url; ?>"><?php echo $lang['main-next'] ?> </a> <?php } ?>
						 </p>
						</div>
					</div>
				</div>

		 </div> <!-- /.breadcrumbs-wrapper -->
     <div class="product-main-wrapper">
     <div class="col-xs-12">
     <div class="row">
     <div class="product-inner-main-wrapper">
     <div class="col-md-6 col-sm-6 col-xs-12">
     <div class="row">
     <div class="product-img-main-wrapper">


	<section
      data-featherlight-gallery
      data-featherlight-filter="a"
    >
	<?php
	foreach ($product_imgs_array as $img) {
	?>
	<a href="images/product-images/<?php echo trim($img); ?>"><img src="images/product-images/<?php echo trim($img); ?>" class="product-img-custom gallery" /></a>
	<?php } ?>
	</section>

</div>
</div>
</div>
    <div class="col-md-6 col-sm-6 col-xs-12">
    <div class="row">
    <div class="product-content-main-wrapper">

		<div class="padding10pc">
		<form action="functions.php" method="post" id="product_to_cart">
		<h1><?php echo $product_title; ?></h1>
		<p class="single-product-price">
			
			<?php 
			echo $price_display; 
			if ($row_product['productid']==1 || $row_product['productid'] == 9) {
				if ($lng=="en") { echo " until 14/8"; }
				if ($lng=="pl") { echo " do 14/8"; }
 				}
			?>
		
		</p>

			<input type="hidden" name="product_id" value="<?php echo $row_product['productid']; ?>">
			<div class="select-box">
				<input size="3" max="9999" min="1" value="1" step="1" type="number" name="quantity" autocomplete="off">
			</div>

			<div class="input-button">
				<button type="submit" name="product_submit" id="product_submit"> <?php echo $lang['add-to-cart-main']; ?> </button>
			</div>

		</form>

		<?php 
		
		echo $row_product['product_description_'.$lng];
		
		$bullet_points = explode(";",$row_product['bullet_points_'.$lng]);
		
		echo "<ul style='list-style-type: circle;'>";
		foreach ($bullet_points as $bullet_point) {
			if (trim($bullet_point)!="") {
				echo "<li>".trim($bullet_point)."</li>";
			}
			
		}
		 echo "</ul>";
		 ?>
		
		<p><?php echo $lang['products_designed_by']; ?></p>

		<h2><?php echo $lang['shipping-singl-pr']; ?></h2>

		<p><?php echo $lang['shipping_tracked']; ?></p>

		<table class="table-full-width postage-table">
		<tr>
			<th><?php echo $lang['region-ship']; ?></th>			<th><?php echo $lang['ship-alone']; ?></th><th><?php echo $lang['ship-another']; ?></th></tr>
			<tr><td><?php echo $lang['poland-ship']; ?></td>			<td class="centered"><?php echo $shipping_alone_poland; ?></td><td class="centered"><?php echo $shipping_multi_poland; ?></td></tr>
			<tr><td><?php echo $lang['europe-ship']; ?></td>			<td class="centered"><?php echo $shipping_alone_europe; ?></td><td class="centered"><?php echo $shipping_multi_europe; ?></td></tr>
			<tr><td><?php echo $lang['rest-of-world-ship']; ?></td>	<td class="centered"><?php echo $shipping_alone_world; ?></td><td class="centered"><?php echo $shipping_multi_world; ?></td></tr>

		</table>
		<div class="product-page-social-icons-wrapper">
		<p>
            <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>">
                <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
            </a>

            <a href="http://twitter.com/share?text=Awesome product:&amp;url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;&hashtags=<?php echo $product_title; ?>" target="_blank">
                <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
            </a>
            <a href="https://plus.google.com/share?url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
                <span><i class="fa fa-google-plus" aria-hidden="true"></i></span>
            </a>
            <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&title=<?php echo $product_title; ?>" target="_blank">
                <span><i class="fa fa-linkedin" aria-hidden="true"></i></span>
            </a>
            <a href="http://www.stumbleupon.com/submit?url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
                <span><i class="fa fa-stumbleupon" aria-hidden="true"></i></span>
            </a>
            <a href="https://reddit.com/submit?url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>" target="_blank">
                <span><i class="fa fa-reddit-alien" aria-hidden="true"></i></span>
            </a>
            <a href="http://www.tumblr.com/share/link?url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI']; ?>&amp;name=<?php echo $product_title; ?>&amp;description=<?php echo $product_title; ?>" target="_blank">
                <span><i class="fa fa-tumblr" aria-hidden="true"></i></span>
            </a>
            <a href="http://pinterest.com/pin/create/button/?url=<?php echo 'http://'.$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];?>" target="_blank">
                <span><i class="fa fa-pinterest" aria-hidden="true"></i></span>
            </a>
		</p>


</div>
		</div>


</div>
</div>
</div>
</div>
</div>
</div>
</div>

	

	

<div class="clearing"></div>
	<a id="triger-featherlight" data-featherlight="#light-box" style="display: none;">Trigger</a>


	<div class="product-added-to-card-main-wrapper" id="light-box">
		<div class="product-added-inner-wrapper">
			<div class="product-added-heading-wrapper">
				<h2><?php echo @$lang['pop-up-addtocart']; ?></h2>
			</div>
			<div class="product-added-options-wrapper">
				<div class="product-added-options-inner-wrapper clearfix">
					<div class="col-md-6">
						<button type="button" class="featherlight-close"> <?php echo $lang['pop-up-carry']; ?> </button>
					</div>
					<div class="col-md-6">
						<a href="cart.php"><button type="button"> <?php echo $lang['checkout-main']; ?> </button></a>
					</div>
				</div>
			</div>
		</div>
	</div>


<?php
require_once('footer.php');
?>
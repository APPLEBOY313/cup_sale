<!DOCTYPE html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
  <title><?php echo $title; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php echo $meta_desc; ?>" />

	<meta name="language" content="English" />
	<meta name="copyright" content="link2light 2004-7" />
	<meta name="charset" content="utf-8" />

	<link href="<?php echo $rootURL; ?>css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo $rootURL; ?>css/themify-icons.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/featherlight.min.css" />
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/featherlight.gallery.min.css" />
	<link href="<?php echo $rootURL; ?>css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $rootURL; ?>css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $rootURL; ?>css/slick-theme.css"/>

</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51828539-1', 'auto');
  ga('send', 'pageview');

</script>

<script>
	var conf = <?php echo render_header_json(); ?>
</script>

<div id="page-wrapper">
<div id="header-wrapper">
	<div id="header">
		<div class="row">
			<div class="col-md-12">
				<div id="user-lang">
					<?php if (is_authenticated()): ?>
						<?php $auth_user = get_auth_user(); ?>
						<span>Hello <span class="fullname"><?php echo $auth_user['first_name'] . ' ' . $auth_user['last_name']; ?></span></span>,
						<a href="<?php echo dashboard_index_url(); ?>">Dashboard</a>,
						<a href="<?php echo dashboard_logout_url(); ?>">Logout</a>
						<!-- <a href="./login.php">Logout</a> -->
					<?php else: ?>
						<span>Hello, Guest<span>,
						<a href="<?php echo dashboard_login_url(); ?>">Login</a>, 
						<!-- <a href="./dashboard/login.php">Login</a>, -->
						<a href="./register.php">Register</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	<div id="cart-lang">
		<a href="<?php echo root_url(); ?>cart.php">
			<div class="mini-cart">
				<div class="mini-cart-img">
					<div class="mini-cart-counter">
						<div class="number-holder">							
							<?php isset($_SESSION['product_num']) ? print_r( $_SESSION['product_num']) : print_r(0); ?>							
						</div>
					</div>
					<img src="<?php echo $rootURL; ?>images/site-images/shopping-bag.png" />
				</div>
			</div>
		</a>
		<?php
		if (!isset($category_form_action)) {
			$category_form_action = '';
		}

		if( empty($no_lang_flags) ){
			if (!empty($category) || !empty($product_url)) {
				if (!empty($category)) {
					if ($category == "mugs") {$category_form_action = "kubki"; }
					if ($category == "kubki") {$category_form_action = "mugs"; }
					$form_action="shop.php?cat=".$category_form_action;
				}
				if (!empty($product_url)) {
					if ($lng=="en") { $product_url_form_action = $product_url_pl; } else { $product_url_form_action = $product_url_en; }
					$form_action="product.php?product=".$product_url_form_action;
				}
			}
			else
			{
				$form_action="#";
			}
			
			?>

			<div id="change_lng">
				<?php if ($lng == "en") { ?>
					<form action="<?php echo $form_action; ?>" method="post">
						<input type="hidden" name="lng" value="pl"/>
						<input type="submit" name="submit" value="PL" />
					</form>
					<form action="<?php echo $form_action; ?>" method="post">
						<input type="hidden" name="lng" value="en"/>
						<input type="submit" name="submit" value="EN" />
					</form>
				<?php }
				if ($lng == "pl") { ?>
					<form action="<?php echo $form_action; ?>" method="post">
						<input type="hidden" name="lng" value="en"/>
						<input type="submit" name="submit" value="EN" />
					</form>
					
					<form action="<?php echo $form_action; ?>" method="post">
						<input type="hidden" name="lng" value="pl"/>
						<input type="submit" name="submit" value="PL" />
					</form>
				<?php } ?>
			</div>
		<?php } ?>	
	
	</div>
		<div class="container">
			<div class="row">
				<div class="header-inner-wrapper">
					<div class="col-md-12">
						<div class="row">
							<div id="header_content">
								<div id="new-logo">
									<a href="<?php echo root_url(); ?>index.php"><img src="<?php echo $rootURL; ?>images/site-images/logo-black-on-white.jpg" alt="logo-picture" /></a>
									
									<div class="menu-button-wrapper">
										<span><i id="navicon" class="fa fa-bars" aria-hidden="true"></i></span>
									</div>		
									
									<div id="nav">
										<ul>
											<li><a href="<?php echo root_url(); ?>index.php"><?php echo $lang['home-page']; ?></a></li>
											<li><a href="<?php echo root_url(); ?>about.php"><?php echo $lang['about-nav-menu']; ?></a></li>
											<li><a href="<?php echo root_url(); ?>shop.php"><?php echo $lang['shop-bread']; ?></a></li> 
											<!-- <li><a href="./shop.php"><?php echo $lang['shop-bread']; ?></a></li> -->
											<li><a href="<?php echo root_url(); ?>contact.php"><?php echo $lang['contact']; ?></a></li>
											<li class="mobile-only"><a href="<?php echo root_url(); ?>cart.php">Cart ( <?php isset($_SESSION['product_num']) ? print_r( $_SESSION['product_num']) : print_r(0); ?> )</a></li>
											<li class="mobile-only">
											<form action="<?php echo $form_action; ?>" method="post">
												<input type="hidden" name="lng" value="en"/>
												<input type="submit" name="submit" value="EN" />
											</form>
											
											<form action="<?php echo $form_action; ?>" method="post">
												<input type="hidden" name="lng" value="pl"/>
												<input type="submit" name="submit" value="PL" />
											</form>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="contents-wrapper">
	<div id="contents-holder">
<?php

<?php

header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Content-Type: application/xml; charset=utf-8");

session_start();
error_reporting(E_ALL);

$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
$og_type="website";

// $rootURL = "http://xbbzbb.co.uk/";
$rootURL = "http://localhost/";

date_default_timezone_set('Europe/London');
header('Content-type: text/html; charset=utf-8');

	$env = array();
	$env['remote_ip'] = $_SERVER['REMOTE_ADDR'];
	$env['state'] = 'normal';
	$env['state_msg'] = '';

	$conf = array();
	$conf['max_login_attempts'] = 3;
	$conf['login_timeout'] = 300;
	$conf['mail_from'] = 'area16_guest19@xbbzbb.co.uk';
	$conf['paypal_recipient_email'] = 'noreply@agafdesign.pl';
	$conf['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
	$conf['paypal_return_url'] = 'http://agafdesign.pl/order-complete.php';
	$conf['paypal_cancel_return_url'] = 'http://agafdesign.pl/reviewandpay.php';
	$conf['paypal_notify_url'] = 'http://agafdesign.pl/ipn.php';
	$conf['paypal_sandbox_recipient_email'] = 'ivan.ivanov.zanev-facilitator@gmail.com';
	$conf['paypal_sandbox_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
	$conf['paypal_sandbox_return_url'] = 'http://xbbzbb.co.uk/order-complete.php';	
	$conf['paypal_sandbox_cancel_return_url'] = 'http://xbbzbb.co.uk/reviewandpay.php';
	$conf['paypal_sandbox_notify_url'] = 'http://xbbzbb.co.uk/ipn.php';

	$conf['paypal_is_sandbox'] = true;

	$view = array();

// database connection
	// $username="master";
	// $password="dPr2u3$7";
	$username="root";
	$password="";
	$database="area16_master";
	$servername = "localhost";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $database);
	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	}
	
	mysqli_set_charset( $conn, 'utf8');

	$cache = array();

	$countries = [];

	$countries ['AFG'] = 'Afghanistan';
	$countries ['ALA'] = 'Åland Islands';
	$countries ['ALB'] = 'Albania';
	$countries ['DZA'] = 'Algeria';
	$countries ['ASM'] = 'American Samoa';
	$countries ['AND'] = 'Andorra';
	$countries ['AGO'] = 'Angola';
	$countries ['AIA'] = 'Anguilla';
	$countries ['ATA'] = 'Antarctica';
	$countries ['ATG'] = 'Antigua and Barbuda';
	$countries ['ARG'] = 'Argentina';
	$countries ['ARM'] = 'Armenia';
	$countries ['ABW'] = 'Aruba';
	$countries ['AUS'] = 'Australia';
	$countries ['AUT'] = 'Austria';
	$countries ['AZE'] = 'Azerbaijan';
	$countries ['BHS'] = 'Bahamas';
	$countries ['BHR'] = 'Bahrain';
	$countries ['BGD'] = 'Bangladesh';
	$countries ['BRB'] = 'Barbados';
	$countries ['BLR'] = 'Belarus';
	$countries ['BEL'] = 'Belgium';
	$countries ['BLZ'] = 'Belize';
	$countries ['BEN'] = 'Benin';
	$countries ['BMU'] = 'Bermuda';
	$countries ['BTN'] = 'Bhutan';
	$countries ['BOL'] = 'Bolivia, Plurinational State of';
	$countries ['BES'] = 'Bonaire, Sint Eustatius and Saba';
	$countries ['BIH'] = 'Bosnia and Herzegovina';
	$countries ['BWA'] = 'Botswana';
	$countries ['BVT'] = 'Bouvet Island';
	$countries ['BRA'] = 'Brazil';
	$countries ['IOT'] = 'British Indian Ocean Territory';
	$countries ['BRN'] = 'Brunei Darussalam';
	$countries ['BGR'] = 'Bulgaria';
	$countries ['BFA'] = 'Burkina Faso';
	$countries ['BDI'] = 'Burundi';
	$countries ['KHM'] = 'Cambodia';
	$countries ['CMR'] = 'Cameroon';
	$countries ['CAN'] = 'Canada';
	$countries ['CPV'] = 'Cape Verde';
	$countries ['CYM'] = 'Cayman Islands';
	$countries ['CAF'] = 'Central African Republic';
	$countries ['TCD'] = 'Chad';
	$countries ['CHL'] = 'Chile';
	$countries ['CHN'] = 'China';
	$countries ['CXR'] = 'Christmas Island';
	$countries ['CCK'] = 'Cocos (Keeling) Islands';
	$countries ['COL'] = 'Colombia';
	$countries ['COM'] = 'Comoros';
	$countries ['COG'] = 'Congo';
	$countries ['COD'] = 'Congo, the Democratic Republic of the';
	$countries ['COK'] = 'Cook Islands';
	$countries ['CRI'] = 'Costa Rica';
	$countries ['CIV'] = 'Côte d\'Ivoire';
	$countries ['HRV'] = 'Croatia';
	$countries ['CUB'] = 'Cuba';
	$countries ['CUW'] = 'Curaçao';
	$countries ['CYP'] = 'Cyprus';
	$countries ['CZE'] = 'Czech Republic';
	$countries ['DNK'] = 'Denmark';
	$countries ['DJI'] = 'Djibouti';
	$countries ['DMA'] = 'Dominica';
	$countries ['DOM'] = 'Dominican Republic';
	$countries ['ECU'] = 'Ecuador';
	$countries ['EGY'] = 'Egypt';
	$countries ['SLV'] = 'El Salvador';
	$countries ['GNQ'] = 'Equatorial Guinea';
	$countries ['ERI'] = 'Eritrea';
	$countries ['EST'] = 'Estonia';
	$countries ['ETH'] = 'Ethiopia';
	$countries ['FLK'] = 'Falkland Islands (Malvinas)';
	$countries ['FRO'] = 'Faroe Islands';
	$countries ['FJI'] = 'Fiji';
	$countries ['FIN'] = 'Finland';
	$countries ['FRA'] = 'France';
	$countries ['GUF'] = 'French Guiana';
	$countries ['PYF'] = 'French Polynesia';
	$countries ['ATF'] = 'French Southern Territories';
	$countries ['GAB'] = 'Gabon';
	$countries ['GMB'] = 'Gambia';
	$countries ['GEO'] = 'Georgia';
	$countries ['DEU'] = 'Germany';
	$countries ['GHA'] = 'Ghana';
	$countries ['GIB'] = 'Gibraltar';
	$countries ['GRC'] = 'Greece';
	$countries ['GRL'] = 'Greenland';
	$countries ['GRD'] = 'Grenada';
	$countries ['GLP'] = 'Guadeloupe';
	$countries ['GUM'] = 'Guam';
	$countries ['GTM'] = 'Guatemala';
	$countries ['GGY'] = 'Guernsey';
	$countries ['GIN'] = 'Guinea';
	$countries ['GNB'] = 'Guinea-Bissau';
	$countries ['GUY'] = 'Guyana';
	$countries ['HTI'] = 'Haiti';
	$countries ['HMD'] = 'Heard Island and McDonald Islands';
	$countries ['VAT'] = 'Holy See (Vatican City State';
	$countries ['HND'] = 'Honduras';
	$countries ['HKG'] = 'Hong Kong';
	$countries ['HUN'] = 'Hungary';
	$countries ['ISL'] = 'Iceland';
	$countries ['IND'] = 'India';
	$countries ['IDN'] = 'Indonesia';
	$countries ['IRN'] = 'Iran, Islamic Republic of';
	$countries ['IRQ'] = 'Iraq';
	$countries ['IRL'] = 'Ireland';
	$countries ['IMN'] = 'Isle of Man';
	$countries ['ISR'] = 'Israel';
	$countries ['ITA'] = 'Italy';
	$countries ['JAM'] = 'Jamaica';
	$countries ['JPN'] = 'Japan';
	$countries ['JEY'] = 'Jersey';
	$countries ['JOR'] = 'Jordan';
	$countries ['KAZ'] = 'Kazakhstan';
	$countries ['KEN'] = 'Kenya';
	$countries ['KIR'] = 'Kiribati';
	$countries ['PRK'] = 'Korea, Democratic People\'s Republic of';
	$countries ['KOR'] = 'Korea, Republic of';
	$countries ['KWT'] = 'Kuwait';
	$countries ['KGZ'] = 'Kyrgyzstan';
	$countries ['LAO'] = 'Lao People\'s Democratic Republic';
	$countries ['LVA'] = 'Latvia';
	$countries ['LBN'] = 'Lebanon';
	$countries ['LSO'] = 'Lesotho';
	$countries ['LBR'] = 'Liberia';
	$countries ['LBY'] = 'Libya';
	$countries ['LIE'] = 'Liechtenstein';
	$countries ['LTU'] = 'Lithuania';
	$countries ['LUX'] = 'Luxembourg';
	$countries ['MAC'] = 'Macao';
	$countries ['MKD'] = 'Macedonia, the former Yugoslav Republic of';
	$countries ['MDG'] = 'Madagascar';
	$countries ['MWI'] = 'Malawi';
	$countries ['MYS'] = 'Malaysia';
	$countries ['MDV'] = 'Maldives';
	$countries ['MLI'] = 'Mali';
	$countries ['MLT'] = 'Malta';
	$countries ['MHL'] = 'Marshall Islands';
	$countries ['MTQ'] = 'Martinique';
	$countries ['MRT'] = 'Mauritania';
	$countries ['MUS'] = 'Mauritius';
	$countries ['MYT'] = 'Mayotte';
	$countries ['MEX'] = 'Mexico';
	$countries ['FSM'] = 'Micronesia, Federated States of';
	$countries ['MDA'] = 'Moldova, Republic of';
	$countries ['MCO'] = 'Monaco';
	$countries ['MNG'] = 'Mongolia';
	$countries ['MNE'] = 'Montenegro';
	$countries ['MSR'] = 'Montserrat';
	$countries ['MAR'] = 'Morocco';
	$countries ['MOZ'] = 'Mozambique';
	$countries ['MMR'] = 'Myanmar';
	$countries ['NAM'] = 'Namibia';
	$countries ['NRU'] = 'Nauru';
	$countries ['NPL'] = 'Nepal';
	$countries ['NLD'] = 'Netherlands';
	$countries ['NCL'] = 'New Caledonia';
	$countries ['NZL'] = 'New Zealand';
	$countries ['NIC'] = 'Nicaragua';
	$countries ['NER'] = 'Niger';
	$countries ['NGA'] = 'Nigeria';
	$countries ['NIU'] = 'Niue';
	$countries ['NFK'] = 'Norfolk Island';
	$countries ['MNP'] = 'Northern Mariana Islands';
	$countries ['NOR'] = 'Norway';
	$countries ['OMN'] = 'Oman';
	$countries ['PAK'] = 'Pakistan';
	$countries ['PLW'] = 'Palau';
	$countries ['PSE'] = 'Palestinian Territory, Occupied';
	$countries ['PAN'] = 'Panama';
	$countries ['PNG'] = 'Papua New Guinea';
	$countries ['PRY'] = 'Paraguay';
	$countries ['PER'] = 'Peru';
	$countries ['PHL'] = 'Philippines';
	$countries ['PCN'] = 'Pitcairn';
	$countries ['POL'] = 'Poland';
	$countries ['PRT'] = 'Portugal';
	$countries ['PRI'] = 'Puerto Rico';
	$countries ['QAT'] = 'Qatar';
	$countries ['REU'] = 'Réunion';
	$countries ['ROU'] = 'Romania';
	$countries ['RUS'] = 'Russian Federation';
	$countries ['RWA'] = 'Rwanda';
	$countries ['BLM'] = 'Saint Barthélemy';
	$countries ['SHN'] = 'Saint Helena, Ascension and Tristan da Cunha';
	$countries ['KNA'] = 'Saint Kitts and Nevis';
	$countries ['LCA'] = 'Saint Lucia';
	$countries ['MAF'] = 'Saint Martin (French part';
	$countries ['SPM'] = 'Saint Pierre and Miquelon';
	$countries ['VCT'] = 'Saint Vincent and the Grenadines';
	$countries ['WSM'] = 'Samoa';
	$countries ['SMR'] = 'San Marino';
	$countries ['STP'] = 'Sao Tome and Principe';
	$countries ['SAU'] = 'Saudi Arabia';
	$countries ['SEN'] = 'Senegal';
	$countries ['SRB'] = 'Serbia';
	$countries ['SYC'] = 'Seychelles';
	$countries ['SLE'] = 'Sierra Leone';
	$countries ['SGP'] = 'Singapore';
	$countries ['SXM'] = 'Sint Maarten (Dutch part';
	$countries ['SVK'] = 'Slovakia';
	$countries ['SVN'] = 'Slovenia';
	$countries ['SLB'] = 'Solomon Islands';
	$countries ['SOM'] = 'Somalia';
	$countries ['ZAF'] = 'South Africa';
	$countries ['SGS'] = 'South Georgia and the South Sandwich Islands';
	$countries ['SSD'] = 'South Sudan';
	$countries ['ESP'] = 'Spain';
	$countries ['LKA'] = 'Sri Lanka';
	$countries ['SDN'] = 'Sudan';
	$countries ['SUR'] = 'Suriname';
	$countries ['SJM'] = 'Svalbard and Jan Mayen';
	$countries ['SWZ'] = 'Swaziland';
	$countries ['SWE'] = 'Sweden';
	$countries ['CHE'] = 'Switzerland';
	$countries ['SYR'] = 'Syrian Arab Republic';
	$countries ['TWN'] = 'Taiwan, Province of China';
	$countries ['TJK'] = 'Tajikistan';
	$countries ['TZA'] = 'Tanzania, United Republic of';
	$countries ['THA'] = 'Thailand';
	$countries ['TLS'] = 'Timor-Leste';
	$countries ['TGO'] = 'Togo';
	$countries ['TKL'] = 'Tokelau';
	$countries ['TON'] = 'Tonga';
	$countries ['TTO'] = 'Trinidad and Tobago';
	$countries ['TUN'] = 'Tunisia';
	$countries ['TUR'] = 'Turkey';
	$countries ['TKM'] = 'Turkmenistan';
	$countries ['TCA'] = 'Turks and Caicos Islands';
	$countries ['TUV'] = 'Tuvalu';
	$countries ['UGA'] = 'Uganda';
	$countries ['UKR'] = 'Ukraine';
	$countries ['ARE'] = 'United Arab Emirates';
	$countries ['GBR'] = 'United Kingdom';
	$countries ['USA'] = 'United States';
	$countries ['UMI'] = 'United States Minor Outlying Islands';
	$countries ['URY'] = 'Uruguay';
	$countries ['UZB'] = 'Uzbekistan';
	$countries ['VUT'] = 'Vanuatu';
	$countries ['VEN'] = 'Venezuela, Bolivarian Republic of';
	$countries ['VNM'] = 'Viet Nam';
	$countries ['VGB'] = 'Virgin Islands, British';
	$countries ['VIR'] = 'Virgin Islands, U.S';
	$countries ['WLF'] = 'Wallis and Futuna';
	$countries ['ESH'] = 'Western Sahara';
	$countries ['YEM'] = 'Yemen';
	$countries ['ZMB'] = 'Zambia';
	$countries ['ZWE'] = 'Zimbabwe';

// sorting out language
	$lng="";
	if(@$_POST['lng']){ 
		if(@$_POST['lng']!=$_SESSION['lng'])
		{
		$_SESSION['lng']=$_POST['lng'];
		}
	}else{
		if(!isset($_SESSION['lng'])){
			$system_language=!empty($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? strtok(strip_tags($_SERVER['HTTP_ACCEPT_LANGUAGE']), ',') : '';
			$system_language = substr($system_language, 0,2); 
			if($system_language!=''){
				if($system_language=='pl'){
					$_SESSION['lng']='pl';
				}else{
					$_SESSION['lng']='en';
				}
			}else{
				$_SESSION['lng']='en';
			}
		}
	}

	$lng = $_SESSION['lng'];
	
	$env['lang'] = $lng;

// call language elements from the database
	$sql_lang = "SELECT * FROM languages";
	$result_lang = $conn->query($sql_lang);

	while($row_lang = $result_lang->fetch_assoc()) {
		$lang[$row_lang['lang_code']] = $row_lang[$lng];
	}

// sort out logo line height
$logo_line_height = 135;
$full_width_header_img = null;
$category = null;
$h2_title=null;

$url_arr = explode("/",$url);

if (strpos(end($url_arr),"shop.php")!==false) { $banner_line_class="banner-line-shop"; }
if (strpos(end($url_arr),"about.php")!==false) { $banner_line_class="banner-line-about"; }
		
$currency_symbol = ($lng=="en") ? '&euro;' : 'zł'; 

$europe = [
    'Albania', 'Andorra', 'Armenia', 'Austria', 'Azerbaijan', 'Belarus', 'Belgium', 'Bosnia and Herzegovina',
    'Bulgaria', 'Croatia', 'Czech Republic', 'Denmark', 'Estonia', 'Faroe Islands', 'Finland', 'France', 'Georgia',
    'Gibraltar', 'Hungary', 'Iceland', 'Ireland', 'Italy', 'Latvia', 'Liechtenstein', 'Lithuania', 'Luxembourg',
    'Macedonia, the former Yugoslav Republic of', 'Malta', 'Moldova, Republic of', 'Monaco', 'Montenegro',
    'Netherlands', 'Norway', 'Portugal', 'Romania', 'San Marino', 'Serbia', 'Slovakia', 'Slovenia', 'Spain',
    'Swaziland', 'Sweden', 'Switzerland', 'Turkey', 'Ukraine', 'United Kingdom'
];

$banner_img = '';

require_once('func.php');

//cart emulator
$products_in_cart = array("1"=>"3","3"=>"1");

$nr_products_in_cart = count($products_in_cart);




?>

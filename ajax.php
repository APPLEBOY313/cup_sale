<?php 

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

global $env;

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 0;
$output = array();

if ($action == 'check_email_unique') {
	try {
		// echo($_POST['email']);
		$email = (isset($_POST['email'])) ? $_POST['email'] : '';
		$customer = db_find_customer_by_email($email);

		if ($customer) {
			$output['unique'] = true;
		} else {
			$output['unique'] = false;
		}
	} catch (Exception $ex) {
		activate_error_state(lang_get_unknownerror_message());
	}
}

if ($action == 'makedefault_address') {
	try {
		$customerid = get_auth_user_id();
		$addressid = (int) ((isset($_POST['address_id'])) ? $_POST['address_id'] : 0);

		db_set_customer_default_address($customer_id, $address_id);

	} catch (Exception $ex) {
		activate_error_state(lang_get_unknownerror_message());
	}
}

$ajax_output = array(
	'env' => array(
		'state' => $env['state'],
		'state_msg' => $env['state_msg'] 
	),
	'output' => $output
);

header("Content-Type: application/json");

echo json_encode($ajax_output);

exit;

<?php
require_once('functions.php');

$title = "Agaf Design - Order Complete";
$meta_desc = "Agaf Design - Order Complete";

$banner_img="images/site-images/banner-contact-img.jpg";

$orderid = (isset($_SESSION['user_order_id']) ? $_SESSION['user_order_id'] : 0);

unset($_SESSION['cart']);
unset($_SESSION['product_num']);
unset($_SESSION['shipping_price_en']);
unset($_SESSION['shipping_price']);
unset($_SESSION['total_price']);
unset($_SESSION['user_order_id']);

require_once('header.php');
?>

<div id="banner-line" class="banner-line-shop-all">
	<div id="banner-line-image" style="display: none; height: 375px; width: 100%;">
	</div>
</div>

<div class="contact-main-wrapper"> <!-- START OF FORM AND CONTACT -->
    <div class="contact-main-inner-wrapper">
        <div class="form-and-contact-main-wrapper">
            <div class="form-and-contact-inner-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="form-main-wrapper clearfix"> <!-- START OF FORM MAIN WRAPPER -->
                            <div class="col-md-offset-1 col-md-6">
                                <div class="row">
                                    <div class="form-wrapper">
										<div class="about-us-main-wrapper">
										<?php echo $lang['order_msg']; ?>
										
										<?php if ($lng="") { ?>
										<p style='text-align: center'>For reference your order id is <b><?php echo $orderid."-".date('Y'); ?></b></p>
										<?php } ?>
										</div>										
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="about-us-main-wrapper clearfix"> <!-- START OF ABOUT US MAIN WRAPPER -->
                                        <div class="about-us-main-inner-wrapper"> <!-- START of about us inner -->
                                            <div class="col-xs-12"> <!-- START Phone info -->
                                                <div class="row">
                                                    <div class="about-us-phone-info about-us-info">
                                                        <p>+48 663 435 111<p>

                                                        <p>agafdesign@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div> <!-- END Phone info -->
                                            <div class="col-xs-12"> <!-- START email info -->
                                                <div class="row">
                                                    <div class="about-us-email-info about-us-info">
                                                     <!--   <p>Archidiakońska 6</p>

                                                        <p>20-112 Lublin</p> -->

                                                    </div>
                                                </div>
                                            </div> <!-- END email info -->
                                        </div> <!-- END of about us inner -->
                                    </div>
                                </div>
                            </div> <!-- END OF ABOUT US MAIN WRAPPER -->
                        </div> <!-- END OF FORM MAIN WRAPPER -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- END OF FORM AND CONTACT -->

<img id="preload" src="<?php echo $banner_img; ?>" style="display:none;" />
<?php
require_once('footer.php');
?>

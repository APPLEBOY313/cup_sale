<?php

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

$parameter_hash = (isset($_GET['hash'])? $_GET['hash'] : '');

$request_row = db_find_forgotten_password_request_by_hash($parameter_hash);

if (!$request_row) {
	redirect_to_home();
}

require_once(PROJECT_ROOT . '/header.php');

?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="row dashboard-wrapper">
					<div class="dashboard-inner col-md-4 col-md-offset-4">
						<h3>Reset Password</h3>

						<?php if (is_error_state()): ?>
							<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
						<?php elseif (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php endif; ?>
						<form action="" method="post" name="resetpass-form">
							<div class="form-field form-field-holder-full-width">
								<input name="pass" type="password" class="text-input disabled-on-check" placeholder="Password" value="">
							</div>

							<button class="submit-button resetpass-button" id="resetpass-submit" type="submit" name="resetpass_submit" value="submit">Reset</button>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
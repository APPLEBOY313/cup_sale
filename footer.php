		</div>
	</div>

	<div id="footer-wrapper"> <!-- START OF FOOTER-WRAPPER ID -->
		<div id="footer-content">
			<div class="container">
				<div class="row">
					<div class="footer-content-inner-wrapper"> <!-- Start of footer-content-inner-wrapper -->
						<!-- <div class="col-md-12">
							<div class="row">
								<div class="mailing-list-wrapper">
									<h2><?php echo $lang['mailing-list-footer']; ?></h2>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="sign-up-main-wrapper">
									<div class="sign-up-inner-wrapper">
										<input type="text" name="email-address" placeholder="<?php echo $lang['e-mail-footer']; ?>">
										<input type="submit" value="<?php echo $lang['sign-up']; ?>">
									</div>
								</div>
							</div>
						</div> -->
						<div class="col-md-12">
							<div class="row">
								<div class="social-icons-wrapper">
									<!-- <a href="#"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a> -->
									<a href="https://www.facebook.com/agafdesign"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a>
									<a href="https://www.instagram.com/agafdesign/"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a>
									<a href="https://www.linkedin.com/in/agnieszka-fornalewska-47495967/"><span><i class="fa fa-linkedin" aria-hidden="true"></i></span></a>
								</div>
							</div>
						</div>
					</div> <!-- End of footer-content-inner-wrapper -->
				</div>
			</div>
		</div>    
	</div>  <!-- END OF FOOTER-WRAPPER ID -->
</div> <!-- END OF PAGE-WRAPPER ID -->

<script type="application/ld+json">
{
  "@context": "http://www.schema.org",
  "@type": "Organization",
  "name": "Agaf Design,
  "url": "http://agafdesign.pl",
  "logo": "http://agafdesign.pl/images/site-images/logo-black-on-white.jpg",
  "description": "Product Design and Ceramic Studio",
  	"sameAs": [
	  "https://www.facebook.com/agafdesign",
	  "https://www.instagram.com/agafdesign/"
	  ],
   "founder": {
    "@type": "Person",
    "name": "Agnieszka Fornalewska",
	"image": "http://agafdesign.pl/images/site-images/agnieszka-fornalewska.jpg",
	"url": "http://example.com",
	"sameAs": [
	  "https://www.linkedin.com/in/agnieszka-fornalewska-47495967/"
	  ]
  }
 }
</script>
	<script src="<?php echo $rootURL; ?>js/jquery-3.1.1.min.js"></script>
	<script src="<?php echo $rootURL; ?>js/jquery.validate.min.js"></script>
	<script src="<?php echo $rootURL; ?>js/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo $rootURL; ?>js/featherlight.gallery.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?php echo $rootURL; ?>plugins/featherlight-1.5.0/release/featherlight.min.js"></script>
	<script src="<?php echo $rootURL; ?>js/ddslick.min.js"></script>
	<script src="<?php echo $rootURL; ?>js/bootstrap-3.3.7.min.js"></script>

	<script src="<?php echo $rootURL; ?>js/main.js"></script>
	<!-- <script src="js/main.js"></script> -->
	<script type="text/javascript" src="<?php echo $rootURL; ?>js/slick.min.js"></script>



	
	<script type="text/javascript">
    $(document).ready(function(){
      $('.front-page-slide-show').slick({
		  infinite: true,
		  speed: 2000,
		  slidesToShow: 1,
		  adaptiveHeight: true,
		  autoplay: true,
		  autoplaySpeed: 3000,
		  mobileFirst: true
      });
	  $('.gallery').featherlightGallery({

		});
    // $("#preload").on('load', function(e){
    //    $('div#banner-line-image').css('background','url(<?php echo $banner_img; ?>) no-repeat center top fixed');
    //    $('div#banner-line-image').fadeIn(1000);
    // });
    });
	
	$( window ).on("load",function(e){
        $('div#banner-line-image').css('background','url(<?php echo $banner_img; ?>) no-repeat center top fixed');
        $('div#banner-line-image').fadeIn(1000);
		
       // $('div#slide16').css('background','url(images/site-images/mobile-slide-im-ceramic-mug.jpg) center center','background-size','cover');
       // $('div#slide16').fadeIn(1000);		
		
	});

	</script>

	<!-- https://www.schemaapp.com/tools/jsonld-schema-generator/Organization/ -->
<?php
$conn->close();
?>
</body>
</html>
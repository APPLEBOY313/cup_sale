<?php
require_once('config.php');

$message_posted = false;

if(isset($_GET['status']) ) {
	if ($_GET['status'] == "message-posted") { $message_posted = true; }
}

$title = "Agaf Design - ".$lang['contact'];
$meta_desc = "Contact Agaf Design Studio";

$banner_img="images/site-images/banner-contact-img.jpg";

require_once('header.php');
?>
<div id="banner-line" class="banner-line-shop-all">
	<div id="banner-line-image" style="display: none; height: 375px; width: 100%;">
	</div>
</div>

<div class="contact-main-wrapper"> <!-- START OF FORM AND CONTACT -->
    <div class="contact-main-inner-wrapper">
        <div class="form-and-contact-main-wrapper">
            <div class="form-and-contact-inner-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="form-main-wrapper clearfix"> <!-- START OF FORM MAIN WRAPPER -->
                            <div class="col-md-offset-1 col-md-6">
                                <div class="row">
                                    <div class="form-wrapper">
										<?php if ($message_posted == false) { ?>
                                        <form action="/includes/contact-form-emailer.php" method="post" id="submit-contact">
                                            <div class="form-inner-wrapper clearfix">
                                                <!-- Name -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="input-field-inner-wrapper clearfix">

                                                            <p class="contact-full-name"><?php echo $lang['name-main']; ?> <span>*</span></p>
                                                            <div class="col-md-6 col-xs-12 no-padding-left">
                                                                    <div class="first-name-custom">
                                                                        <input name="firstname" type="text"
                                                                               placeholder="" id="contact-fname" value="">
                                                                        <label><?php echo $lang['first-name']; ?></label>
                                                                    </div>
                                                            </div>
                                                            <div class="col-md-6 col-xs-12 no-padding">
                                                                    <div class="last-name-custom">
                                                                        <input name="lastname" type="text"
                                                                               placeholder="" id="contact-lname" value="">
                                                                       <label><?php echo $lang['last-name']; ?></label>
                                                                    </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Email -->
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="input-field-inner-wrapper">
                                                            <p><?php echo $lang['e-mail-main']; ?> <span>*</span></p>
                                                            <input class="email-custom" type="text" name="emailadress" id="contact-email" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Phone -->
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="input-field-inner-wrapper">
                                                            <p><?php echo $lang['phone-main']; ?></p>
                                                            <div class="phone-country">
                                                                <label for=""><?php echo $lang['country-main']; ?> <input  type="text" name="phone1" id="contact-phone-country"></label>
                                                            </div>
                                                            <div class="phone-operator">
                                                                <label for="">(###)<input type="text" name="phone2" id="contact-phone-operator"></label>
                                                            </div>
                                                            <div class="phone-first-three">
                                                                <label for="">###<input  type="text" name="phone3" id="contact-phone-first-three"></label>
                                                            </div>
                                                            <div class="phone-last-four">
                                                                <label for="">####<input type="text" name="phone4" id="contact-phone-last-four"></label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Subject -->
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="input-field-inner-wrapper">
                                                            <p><?php echo $lang['subject-main']; ?></p>
                                                            <input class="subject" type="text" name="subject" id="contact-subject">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Message area -->
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="input-field-inner-wrapper">
                                                            <p><?php echo $lang['message-main']; ?> <span>*</span></p>
                                                            <textarea class="message-area" name="message"
                                                                       id="contact-message"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="submit-button-wrapper">
                                                            <input type="submit" value="<?php echo $lang['submit-main']; ?>" id="contact-submit-<?php echo $lng; ?>">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
										<?php }
										else
										{ ?>
										<div class="about-us-main-wrapper">
										<?php echo $lang['contact_thanks']; ?>
										</div>										
										<?php
										}


										?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="row">
                                    <div class="about-us-main-wrapper clearfix"> <!-- START OF ABOUT US MAIN WRAPPER -->
                                        <div class="about-us-main-inner-wrapper"> <!-- START of about us inner -->
                                            <div class="col-xs-12"> <!-- START Phone info -->
                                                <div class="row">
                                                    <div class="about-us-phone-info about-us-info">
                                                        <p>+48 663 435 111<p>

                                                        <p>agafdesign@gmail.com</p>
                                                    </div>
                                                </div>
                                            </div> <!-- END Phone info -->
                                            <div class="col-xs-12"> <!-- START email info -->
                                                <div class="row">
                                                    <div class="about-us-email-info about-us-info">
                                                     <!--   <p>Archidiakońska 6</p>

                                                        <p>20-112 Lublin</p> -->

                                                    </div>
                                                </div>
                                            </div> <!-- END email info -->
                                        </div> <!-- END of about us inner -->
                                    </div>
                                </div>
                            </div> <!-- END OF ABOUT US MAIN WRAPPER -->
                        </div> <!-- END OF FORM MAIN WRAPPER -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- END OF FORM AND CONTACT -->

<img id="preload" src="<?php echo $banner_img; ?>" style="display:none;" />
<?php
require_once('footer.php');
?>

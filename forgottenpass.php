<?php

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

require_once(PROJECT_ROOT . '/header.php');

?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="row dashboard-wrapper">
					<div class="dashboard-inner col-md-4 col-md-offset-4">
						<h3>Forgotten Password</h3>

						<?php if (is_error_state()): ?>
							<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
						<?php elseif (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php endif; ?>
						<form action="" method="post" name="forgottenpass-form" class="forgottenpass-form">
							<div class="form-field form-field-holder-full-width">
								<input name="email" type="text" class="text-input disabled-on-check" placeholder="E-mail" value="">
							</div>

							<button class="submit-button forgottenpass-button" id="forgottenpass-submit" type="submit" name="forgottenpass_submit" value="submit">Go</button>
						</form>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
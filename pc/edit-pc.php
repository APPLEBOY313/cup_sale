<?php
require_once('../config.php');

$productid=0;

if( isset($_POST['product_name_pl'])) {
	$productid = $_POST['productid']; 
	$product_name_pl = addslashes($_POST['product_name_pl']);
	$product_description_en = addslashes($_POST['product_description_en']);
	$product_description_pl = addslashes($_POST['product_description_pl']);
	$cat_title_en = addslashes($_POST['cat_title_en']);
	$cat_title_pl = addslashes($_POST['cat_title_pl']);
	$cat_desc_en = addslashes($_POST['cat_desc_en']);
	$cat_desc_pl = addslashes($_POST['cat_desc_pl']);
	$bullet_points_en = $_POST['bullet_points_en'];
	$bullet_points_pl = $_POST['bullet_points_pl'];
	
	$sql_update_product = "UPDATE products SET 
						product_name_pl='$product_name_pl', 
						product_description_en='$product_description_en', 
						product_description_pl='$product_description_pl', 
						cat_title_en='$cat_title_en', 
						cat_title_pl='$cat_title_pl', 
						cat_desc_en='$cat_desc_en', 
						cat_desc_pl='$cat_desc_pl', 
						bullet_points_en='$bullet_points_en', 
						bullet_points_pl='$bullet_points_pl'					
						WHERE productid='$productid'";
				 $conn->query($sql_update_product);
	
}

if( isset($_POST['products']) || $productid>0) {
	if (isset($_POST['products'])) {$productid = $_POST['products'];}
	
	$sql_product = "SELECT * FROM products WHERE productid='$productid'";
	$result_product  = $conn->query($sql_product);
	$row_product  = mysqli_fetch_assoc($result_product);
	
	
}




?>
<!doctype html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agaf Design Catalogue January 2016</title>

<style type="text/css">	
td {padding: 20px; border-bottom: 1px solid #ccc;}
input[type=text], textarea {width: 800px; font-family: "Arial"; font-size: 16px;}	
textarea {height: 200px;}	
	
</style>	
</head>
<body>

<div id="wrapper" style="width: 90%; border: 1px solid #ccc; margin: 20px auto; padding: 40px;" >

<?php if ($productid==0) { ?>
<form action="#" method="post">
<select name="products">
<?php
	$sql_products = "SELECT * FROM products ORDER BY orderby ASC";
	$result_products  = $conn->query($sql_products);
	while($row_products = $result_products->fetch_assoc()) {
?>
<option value="<?php echo $row_products['productid']; ?>"><?php echo $row_products['product_name_en']; ?></option>




	<?php } ?>
</select>
<input type="submit" value="Go">
</form>

<?php
}
else
{
?>
<a href="edit-pc.php">Back to Menu</a>
<form action="#" method="post">
<input type="hidden" name="productid" value="<?php echo $productid; ?>">
<h1><?php echo $row_product['product_name_en']; ?></h1>

<table>
<tr>
	<td>Product Name PL</td>
	<td><input type="text" name="product_name_pl" value="<?php echo htmlentities(stripslashes($row_product['product_name_pl'])); ?>"></td>
</tr>

<tr>
	<td>Website Product Description EN</td>
	<td><textarea name="product_description_en"><?php echo $row_product['product_description_en']; ?></textarea></td>
</tr>

<tr>
	<td>Website Product Description PL</td>
	<td><textarea name="product_description_pl"><?php echo htmlentities(stripslashes($row_product['product_description_pl'])); ?></textarea></td>
</tr>

<tr>
	<td>Catalogue Name EN</td>
	<td><input type="text" name="cat_title_en" value="<?php echo htmlentities(stripslashes($row_product['cat_title_en'])); ?>"></td>
</tr>

<tr>
	<td>Catalogue Name PL</td>
	<td><input type="text" name="cat_title_pl" value="<?php echo $row_product['cat_title_pl']; ?>"></td>
</tr>

<tr>
	<td>Catalogue Product Description EN</td>
	<td><textarea name="cat_desc_en"><?php echo $row_product['cat_desc_en']; ?></textarea></td>
</tr>

<tr>
	<td>Catalogue Product Description PL</td>
	<td><textarea name="cat_desc_pl"><?php echo htmlentities(stripslashes($row_product['cat_desc_pl'])); ?></textarea></td>
</tr>

<tr>
	<td>Bullet Points EN</td>
	<td><textarea name="bullet_points_en" style="height: 50px"><?php echo $row_product['bullet_points_en']; ?></textarea></td>
</tr>

<tr>
	<td>Bullet Points PL</td>
	<td><textarea name="bullet_points_pl" style="height: 50px"><?php echo stripslashes($row_product['bullet_points_pl']); ?></textarea></td>
</tr>


</table>

<input type="submit" value="Save">
</form>
<?php 
}
?>
</div>
<body>
</html>
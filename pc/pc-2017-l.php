<?php
require_once('../config.php');

if( isset($_GET['lng']) ) { $lng=$_GET['lng'];}
?>
<!doctype html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Agaf Design Catalogue January 2017</title>
	<!-- <link rel="stylesheet" type="text/css" href="pc_styles.css" /> -->

<style type="text/css">
body {font-family: arial;font-size: 12px; }

.catalogue-wrapper {width: 1030px; margin: 0 auto; }

.front-cover { text-align: center; font-family: 'Open Sans Condensed', sans-serif;  font-size: 36px; }

.section-title-page { font-family: 'Open Sans Condensed', sans-serif;  font-size: 36px; }

.product-page-container { page-break-before: always; clear:both;  }
.product-page-content { padding: 20px 15px; position: relative; height: 725px; }

.product-title {font-size: 24px; font-weight:bold; float:left}
.product-nr { margin-bottom: 20px; font-size: 1.5em; float: right; }
.product-images { clear:both; width: 585px; float: right; }
.product-images img { border: 1px solid #c0c0c0; margin: 10px 0px; width:280px; float: left;}
.product-images img:nth-of-type(even) {float: right;}

.product-description-line {float: left; width: 300px; margin-top: 20px; font-family: 'Open Sans Condensed', sans-serif;}
.product-description {font-size: 18px; }
.product-description p {margin-top: 0; margin-bottom: 10px; }
.product-bullet-points { font-size: 18px;}
.product-bullet-points ul {margin: 30px 0px 20px 0px; }


.product-price-line {clear: both; margin-top: 20px; height: 210px;}
.product-notes {float:left; width: 50%}
.product-pricing {float:right; width: 50%; font-family: "Times New Roman";  font-size: 18px;}

.product-pricing .rrp{font-size: 22px;}



.product-logo-line {position: absolute; bottom: 0px; left: 10px;}

.logo-holder {float: right;}



    .Table
    {
        display: table; width: 100%
    }
    .Heading
    {
        display: table-row;
        font-weight: bold;
        text-align: center;
		line-height: 1;
    }
	.Heading-Left { border-bottom: 1px solid #000; border-right: 1px solid #000; }
	.Heading-Right { border-bottom: 1px solid #000; }
    .Row
    {
        display: table-row;
		line-height: 1;
    }
    .Cell
    {
        display: table-cell;
        width: 50%;
		text-align: center;
    }
	
	.Cell p {margin: 5px; padding: 0; }
	
	.Cell-Left { border-right: 1px solid #000; }

	.clearing {clear:both;}
</style>	
	
	
</head>
<body>
<?php
// product info setup
  $currency = "pln"; $lng = "pl";
// $currency = "gbp"; $lng = "en";
// $currency = "eur"; $lang = "en";

 $collection = "standard";
// $collection = "limitededition";


function price_maker($currency, $price ) {
	if ($currency=="gbp") {echo "&pound;"; } 
	if ($currency=="eur") { echo "&euro;"; } 
	echo $price; 
	if ($currency=="pln") { echo " zł"; }
	}


if ($lng=="en") { $prod_cat = "Product Catalogue"; $cat_date = "2017"; $cat_number = "Catalogue Number:"; }
	else
	{  $prod_cat = "Katalog Produktowy"; $cat_date = "2017"; $cat_number = "Numer Katalogowy";  }	
	
?>

<div style="position: relative; height: 750px; margin: 0 auto; width: 1030px" class="section-title-page">
	<div style="position: absolute; border: 1px solid #ccc; height: 750px; width: 1030px; background: url('images/header-intro-bg-cat.jpg') center -200px; background-size: 1400px; opacity: .5;">
	</div>
	
	<div style="position:absolute; width: 1030px; margin-top: 145px; text-align:center;">
	<img src="agaf-logo.png" style="width: 250px;"/>
	
		<div>
		<?php echo $prod_cat; ?>
		<br /><br />
		<?php echo $cat_date; ?>
		</div>
	
	
	</div>
</div>

<!-- Products Loop -->
<div class="catalogue-wrapper">
<?php
	$sql_products = "SELECT * FROM products WHERE forsale = 'Y' ORDER BY orderby ASC";
	$result_products  = $conn->query($sql_products);
	while($row_products = $result_products->fetch_assoc()) {
		
	$images_arr = explode(",",$row_products['product_images_'.$lng]); 
	$images_arr = array_map('trim',$images_arr);
	if ($row_products['product_cat_en']=="Drinkware") {
		$images_arr = array_slice($images_arr, 0, 6); // limit to first 6 images
	} else {
		$images_arr = array_slice($images_arr, 0, 3); // limit to first 3 images
	}
		
?>
	<div class="product-page-container">
		<div class="product-page-content">
			<div class="product-title"><?php if (!empty($row_products['cat_title_'.$lng])) { echo stripslashes($row_products['cat_title_'.$lng]); } else { echo stripslashes($row_products['product_name_'.$lng]); } ?></div>
			<div class="product-nr"><?php echo $cat_number.": ".$row_products['catalogue_nr']; ?></div>
			<div class="product-images">
				<?php $counter=0; foreach($images_arr as $image) {  $counter++;?>
					<img src="../images/product-images/<?php echo $image; ?>" <?php if ($row_products['product_cat_en']=="Tableware" && $counter==1) { echo " style=\"width: 585px;\""; } ?>/>
				<?php	
					}
				?>	
				<div class="clearing"></div>
			</div>
			
			<div class="product-description-line">
				<div class="product-description">
				<?php
				if ($row_products['cat_desc_'.$lng]!="") {
					echo stripslashes($row_products['cat_desc_'.$lng]); 
				}
				else
				{
					echo stripslashes($row_products['product_description_'.$lng]); 
				}
				?>		
				</div>
				
				<div class="product-bullet-points">
				<?php 
				if (!empty($row_products['bullet_points_'.$lng])) {
				
				$bullet_points_arr = explode(";", $row_products['bullet_points_'.$lng]);
				$bullet_points_arr = array_map('trim',$bullet_points_arr);
				
				echo "<ul>";
				foreach($bullet_points_arr as $bullet_point) {
					echo "<li>".$bullet_point."</li>";
					}
				echo "</ul>";
				} ?>	
				</div>
						
				<div class="product-logo-line">
					<div class="logo-holder">
						<img src="agaf-logo.png" style="width: 125px;" />
					</div>
					<div class="clearing"></div>
				</div>	
				
				<div class="clearing"></div>
			</div>


			
		<div class="clearing"></div>
		</div>
	</div>
	<?php
	}
	?>
</div>







<body>
</html>
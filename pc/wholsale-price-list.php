<?php
require_once('../config.php');
if($lng=="pl") {
	
	$currency_symbol = "zł";
	
	
	} else { 
	$title = "EURO Retailer Price List – May 2017";
	$currency_symbol = "&euro;"; 
	$lng_arr = array('product' => 'Product', 'inc_vat' => 'inc. VAT', 'vat_zero' => 'VAT 0%', 'wholesale_price' => 'Wholesale Price', 'lead_time' => 'Lead Time', 'weeks' => 'Weeks' );
	}
?>
<!doctype html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title><?php echo $title; ?></title>

<style type="text/css">
body {font-family: "Arial"; font-size: 12px;}	
td, th {padding: 5px;}
table, th, td {border: 1px solid #ccc;}
	
</style>	
</head>
<body>

<div style="position: relative; height: 750px; margin: 0 auto; width: 1030px" class="section-title-page">
	<div style="float:left">
		<img src="agaf-logo.png" style="width: 200px; margin-bottom: 30px;"/>
	</div>
	
	<div style="float:right; width:750px; text-align:center;">
		<h1><?php echo $title; ?></h1>
	</div>
	
	<table style="clear:both;border-collapse: collapse;">
	<tr>
		<th style="text-align:center; width:70px;">#</th>
		<th style="text-align:left; width:500px;"><?php echo $lng_arr['product']; ?></th>
		<th style="text-align:center; width: 150px;">RRP (<?php echo $currency_symbol; ?>)<br /><?php echo $lng_arr['inc_vat']; ?></th>
		<th style="text-align:center; width: 150px;"><?php echo $lng_arr['wholesale_price']; ?> (<?php echo $currency_symbol; ?>)<br /><?php echo $lng_arr['vat_zero']; ?></th>
		<th style="text-align:center; width: 150px;"><?php echo $lng_arr['lead_time']; ?> </th>
	</tr>
		

<?php
	$sql_products = "SELECT * FROM products WHERE forsale='Y' ORDER BY orderby ASC";
	$result_products  = $conn->query($sql_products);
	while($row_products = $result_products->fetch_assoc()) {
?>
	<tr>
		<td style="text-align:center;"><?php echo $row_products['catalogue_nr']; ?></td>
		<td><?php if (!empty($row_products['cat_title_'.$lng])) { echo stripslashes($row_products['cat_title_'.$lng]); } else { echo stripslashes($row_products['product_name_'.$lng]); } ?></td>
		<td style="text-align:center;"><?php echo $row_products['product_price_'.$lng]; ?></td>
		<td style="text-align:center;"><?php echo $row_products['wsale_'.$lng]; ?></td>
		<td style="text-align:center;"><?php echo $row_products['lead_time']." ".$lng_arr['weeks']; ?></td>
	</tr>
<?php } ?>
	</table>

<ol>
	<li>Lead times depend on current stock availability</li>
	<li>Minimum order: 20 units</li>
	<li>Transport costs (via DPD courier ground) to EU countries outside Poland are not included</li>
</ol>	
	
</div>
<body>
</html>
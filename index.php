<?php
require_once('config.php');
$title = "Agaf Design Studio";
$meta_desc = "Agaf Design Studio";
require_once('header.php');

?>

<div class="front-page-slide-show" style="width: 100%;">
	<div class="slide" id="slide16"><img id="slide16img" src="images/site-images/slide-transparent.gif" /></div>
	<div class="slide" id="slide10"><img id="slide10img" src="images/site-images/slide-transparent.gif" /></div>
	<div class="slide" id="slide14"><img id="slide14img" src="images/site-images/slide-transparent.gif" /></div>
	<div class="slide" id="slide12"><img id="slide12img" src="images/site-images/slide-transparent.gif" /></div>
	<div class="slide" id="slide11"><img id="slide11img" src="images/site-images/slide-transparent.gif" /></div>
	<div class="slide" id="slide6"><img id="slide6img" src="images/site-images/slide-transparent.gif" /></div>
</div>

<div class="clearing"></div>
<?php
require_once('footer.php');
?>
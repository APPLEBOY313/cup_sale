<?php
require_once('functions.php');

$title = "Agaf Design - Review and Pay";
$meta_desc = "Agaf Design - Review your order and pay";


$no_lang_flags = 'no_flags';
require_once('header.php');
if(!empty($_SESSION['user_order_id'])){
	$order_id = $_SESSION['user_order_id'];

	// $con=mysqli_connect("localhost","root", "" ,"timpurephp") or die("Connecting to MySQL failed");

	$query = "SELECT products.productid AS 'product_id', products.product_name_en AS 'product_name_en',
              products.product_name_pl AS 'product_name_pl', products.product_url_en AS 'product_url_en',
              products.product_url_pl AS 'product_url_pl', products.product_description_en AS 'product_description_en',
              products.product_description_pl AS 'product_description_pl', products.product_price_en AS 'product_price_en',
              products.product_price_pl AS 'product_price_pl', products.product_images_en AS 'product_images_en',
              products.product_images_pl AS 'product_images_pl',  order_product.qty as 'qty' FROM products
              INNER JOIN order_product ON products.productid = order_product.product_id
              INNER JOIN orders ON order_product.order_id = orders.id
              WHERE orders.id = $order_id";

	$data=mysqli_query($conn, $query);

	$order_query = "SELECT * FROM orders WHERE id = $order_id";

	$order_data=mysqli_query($conn, $order_query);

	$order = mysqli_fetch_assoc($order_data);
//    echo "<pre>"; print_r( $order  ); echo "</pre>" ; exit();
}

?>
<div class="checkout-main-wrapper clearfix">
	<div class="container">
		<div class="row">
			<div id="checkout">
				<div class="checkout-heading-wrapper clearfix">
					<div class="col-xs-12">
						<div class="row">
							<a href="checkout.php#back"><button class="btn btn-primary pull-right" type="button"><?php echo $lang['back']; ?></button></a><h1 class="inline-block review-text-wrapper"><?php echo $lang['review-and-pay']; ?></h1>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="customer-info-wrapper">
							<div class="col-xs-12">
								<div class="row">
									<div class="customer-info">
										<div class="col-xs-12">
											<div class="row">
												<div class="shipping-billing-heading-wrapper">
													<h2><?php echo $lang['shipping']; ?></h2>
													<div class="shipping-billing-info">
													<p><b><?php echo $order['first_name']." ".$order['last_name']; ?></b></p>
													<p>
													<?php 
													echo $order['address_primary']."<br />"; 
													if (!empty($order['address_secondary'])) { echo $order['address_secondary']."<br />"; }
													if ($lng=="pl") {  
														echo $order['postcode']." ".$order['city']."<br />";
														} 
														else 
														{  
														echo $order['city']."<br />";
														echo $order['postcode']."<br />";
														}													
													echo $countries[$order['country']]."<br />";
													
													?>
													</p>
													<?php 
														if($_SESSION['invoice_req_flag'] == true){?>
														<?php 
															$_SESSION['invoice_req_flag'] = false;
															if (!empty($order['address1_inv'])) { ?>
															<h2><?php echo $lang['invoice-details']; ?></h2>
															<p><b><?php echo $order['company_name']."<br />".$order['firstname_inv']." ".$order['lastname_inv']; ?></b></p>
															<p>
															<?php 
																echo $order['address1_inv']."<br />"; 
																if (!empty($order['address2_inv'])) { echo $order['address2_inv']."<br />"; }
																if ($lng=="pl") {  
																	echo $order['postcode_inv']." ".$order['city_inv']."<br />";
																} 
																else 
																{  
																	echo $order['city_inv']."<br />";
																	echo $order['postcode_inv']."<br />";
																}
																echo $countries[$order['country_inv']]."<br />";
													
																?>
															</p>
													
														<?php } ?>
													<?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearing"></div>
						</div>
					</div>
				</div> <!--/ .col-md-6 -->
				<div class="col-md-6"> <!-- .col-md-6 -->
					<div class="row">
						<div class="order-summary-wrapper">
							<div class="order-summary">
								<div class="col-xs-12">
									<div class="row">
										<div class="order-summary-heading-wrapper">
											<h2><?php echo $lang['order-summary']; ?></h2>
										</div>
									</div>
								</div>
								<div class="cart-listings-main-wrapper clearfix">
									<div class="col-xs-12">
										<div class="row">
											<div class="cart-listings-inner-wrapper">
												<div id="cart-listings">
													<div class="col-xs-12"> <!-- .product-info-wrapper -->
														<div class="row">
															<div class="product-info-wrapper clearfix">
																<div class="col-xs-6">
																	<div class="row">
																		<div class="cart-img-header">
																			<p><?php echo $lang['item-main']; ?></p>
																		</div>
																	</div>
																</div>
																<div class="col-xs-3">
																	<div class="row">
																		<div class="cart-qty-price-header">
																			<p><?php echo $lang['quantity-main']; ?></p>
																		</div>
																	</div>
																</div>
																<div class="col-xs-3">
																	<div class="row">
																		<div class="cart-qty-price-header">
																			<p><?php echo $lang['price-main']; ?></p>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div> <!-- /.product-info-wrapper -->

													<?php
													$cart_subtotal = 0;

													for ($i = 0; $i < count(@$_SESSION['cart']['id']); $i++) {

														$sql_product = "SELECT * FROM products WHERE productid={$_SESSION['cart']['id'][$i]}";
														$result_product = $conn->query($sql_product);
														$row_product = $result_product->fetch_assoc();
//                                                        echo "<pre>"; print_r(  $row_product['productid'] ); echo "</pre>" ; exit();
														$product_title = $row_product['product_name_' . $lng];
														$product_imgs_array = explode(",",
																$row_product['product_images_' . $lng]);
														$price_display = pricedisplay(($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]),
																$lng);

														$cart_subtotal = $cart_subtotal + ($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]);

														?>
														<div class="col-xs-12">
															<div class="row">
																<div class="single-product-info-wrapper clearfix">

																	<div> <!-- glavni div wrapper produkta -->
																		<div class="col-xs-6">
																			<!-- .product-img-name-wrapper -->
																			<div class="row">
																				<div
																						class="product-img-name-wrapper clearfix">
																					<div class="col-xs-3">
																						<div class="row">
																							<div class="cart-img">
																								<img
																										class="width100"
																										src="images/product-images/<?php echo $product_imgs_array[0]; ?>"/>
																							</div>
																						</div>
																					</div>
																					<div class="col-xs-9">
																						<div class="row">
																							<div class="cart-item">
																								<a href="product.php?product=<?php echo $row_product['product_url_' . $lng]; ?>">
																									<p><?php echo $product_title; ?></p>
																								</a>
																								<input type="hidden"
																									   name="products[]"
																									   value="<?php echo $row_product['productid']; ?>">
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div> <!-- /.product-img-name-wrapper -->
																		<div class="col-xs-6">
																			<div class="row">
																				<div
																						class="product-qnty-prc-wrapper clearfix">
																					<div class="col-sm-6">
																						<div class="row">
																							<div class="cart-qty-price qty-small">
																							<?php echo $_SESSION['cart']['quantity'][$i]; ?>
																							</div>
																						</div>
																					</div>
																					<div class="col-sm-6">
																						<div class="row">
																							<div
																									class="cart-qty-price">
																								<p id="product-<?php echo $row_product['productid']; ?>-price"><?php echo $price_display; ?></p>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>

																	</div>

																</div>
															</div>
														</div>


														<?php
													}

													//                                                    else if(!empty($_SESSION['user_order_id'])){
													//
													//                                                        while( $row_product = mysqli_fetch_assoc($data)) :
													////                                                        echo "<pre>"; print_r(  $row_product['productid'] ); echo "</pre>" ; exit();
													//                                                            $product_title = $row_product['product_name_' . $lng];
													//                                                            $product_imgs_array = explode(",",
													//                                                                $row_product['product_images_' . $lng]);
													//                                                            $price_display = pricedisplay(($row_product['product_price_' . $lng] * $row_product['qty']),
													//                                                                $lng);
													//
													//                                                            $cart_subtotal = $cart_subtotal + ($row_product['product_price_' . $lng] * $row_product['qty']);
													//                                                            ?>
													<!--                                                            <div class="col-xs-12">-->
													<!--                                                                <div class="row">-->
													<!--                                                                    <div class="single-product-info-wrapper clearfix">-->
													<!---->
													<!--                                                                        <div> <!-- glavni div wrapper produkta -->
													<!--                                                                            <div class="col-xs-7">-->
													<!--                                                                                <!-- .product-img-name-wrapper -->
													<!--                                                                                <div class="row">-->
													<!--                                                                                    <div-->
													<!--                                                                                        class="product-img-name-wrapper">-->
													<!--                                                                                        <div class="col-xs-3">-->
													<!--                                                                                            <div class="row">-->
													<!--                                                                                                <div class="cart-img">-->
													<!--                                                                                                    <img-->
													<!--                                                                                                        class="width100"-->
													<!--                                                                                                        src="images/product-images/--><?php //echo $product_imgs_array[0]; ?><!--"/>-->
													<!--                                                                                                </div>-->
													<!--                                                                                            </div>-->
													<!--                                                                                        </div>-->
													<!--                                                                                        <div class="col-xs-9">-->
													<!--                                                                                            <div class="row">-->
													<!--                                                                                                <div class="cart-item">-->
													<!--                                                                                                    <input type="hidden"-->
													<!--                                                                                                           name="pl_price"-->
													<!--                                                                                                           value="--><?php //echo $row_product['product_price_pl']; ?><!--">-->
													<!--                                                                                                    <input type="hidden"-->
													<!--                                                                                                           name="en_price"-->
													<!--                                                                                                           value="--><?php //echo $row_product['product_price_en']; ?><!--">-->
													<!--                                                                                                    <a href="product.php?product=--><?php //echo $row_product['product_url_' . $lng]; ?><!--">-->
													<!--                                                                                                        <p>--><?php //echo $product_title; ?><!--</p>-->
													<!--                                                                                                    </a>-->
													<!--                                                                                                    <input type="hidden"-->
													<!--                                                                                                           name="products[]"-->
													<!--                                                                                                           value="--><?php //echo $row_product['product_id']; ?><!--">-->
													<!--                                                                                                </div>-->
													<!--                                                                                            </div>-->
													<!--                                                                                        </div>-->
													<!--                                                                                    </div>-->
													<!--                                                                                </div>-->
													<!--                                                                            </div> <!-- /.product-img-name-wrapper -->
													<!--                                                                            <div class="col-xs-5">-->
													<!--                                                                                <div class="row">-->
													<!--                                                                                    <div-->
													<!--                                                                                        class="product-qnty-prc-wrapper clearfix">-->
													<!--                                                                                        <div class="col-xs-5">-->
													<!--                                                                                            <div class="row">-->
													<!--                                                                                                <div-->
													<!--                                                                                                    class="cart-qty-price">-->
													<!--                                                                                                    <input-->
													<!--                                                                                                        class="width35 input-qty-val"-->
													<!--                                                                                                        name="qtys[]"-->
													<!--                                                                                                        type="number"-->
													<!--                                                                                                        id="product---><?php //echo $row_product['product_id']; ?><!---qty"-->
													<!--                                                                                                        value="--><?php //echo $row_product['qty']; ?><!--">-->
													<!--                                                                                                </div>-->
													<!--                                                                                            </div>-->
													<!--                                                                                        </div>-->
													<!--                                                                                        <div class="col-xs-5">-->
													<!--                                                                                            <div class="row">-->
													<!--                                                                                                <div-->
													<!--                                                                                                    class="cart-qty-price">-->
													<!--                                                                                                    <p id="product---><?php //echo $row_product['product_id']; ?><!---price">--><?php //echo $price_display; ?><!--</p>-->
													<!--                                                                                                </div>-->
													<!--                                                                                            </div>-->
													<!--                                                                                        </div>-->
													<!--                                                                                        <div class="col-xs-2">-->
													<!--                                                                                            <div class="row">-->
													<!--                                                                                                <div-->
													<!--                                                                                                    class="cart-remove">-->
													<!--                                                                                                    <div-->
													<!--                                                                                                        class="delete-product-cart"-->
													<!--                                                                                                        id="--><?php //echo $row_product['product_id']; ?><!--">
<!--                                                                                                        x-->
													<!--                                                                                                    </div>-->
													<!--                                                                                                </div>-->
													<!--                                                                                            </div>-->
													<!--                                                                                        </div>-->
													<!--                                                                                    </div>-->
													<!--                                                                                </div>-->
													<!--                                                                            </div>-->
													<!---->
													<!--                                                                        </div>-->
													<!---->
													<!--                                                                    </div>-->
													<!--                                                                </div>-->
													<!--                                                            </div>-->
													<!---->
													<!---->
													<!--                                                            --><?php
													//
													//
													//
													//                                                            endwhile;
													//
													//
													$cart_shipping = $order['shipping_price'];
													$cart_total = $order['grand_total_price'];
													$cart_subtotal_display = pricedisplay($cart_subtotal, $lng);
													$cart_shipping_display = pricedisplay(trim($cart_shipping), $lng);
													$cart_total_display = pricedisplay($cart_total, $lng);
													?>
													<input type="hidden" name="order_id" value="<?php echo $_SESSION['user_order_id'] ?>">
													<input type="hidden" name="lang" value="<?php echo $lng; ?>">
													<div class="col-xs-12">
														<div class="row">
															<div class="subtotal-section-wrapper">
																<p><?php echo $lang['subtotal-main']; ?> : <span id="subtotal-price"> <?php echo $cart_subtotal_display; ?></span></p>
															</div>
															<div class="subtotal-section-wrapper">
																<p> <?php echo $lang['shipping']; ?> : <span id="shipping-price"> <?php echo $cart_shipping_display; ?></span></p>
															</div>
															<div class="subtotal-section-wrapper">
																<p> <?php echo $lang['total']; ?> : <span id="total-price"> <?php echo $cart_total_display; ?></span></p>
															</div>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearing"></div>
						</div>
					</div>
				</div> <!--/ .col-md-6 -->
				<div class="col-md-6">
				</div>
				<div class="col-md-6">
					<div class="checkout-line">
					<p>After selecting Pay you will be taken to PayPal's secure checkout where you can use your PayPal account or, if you have not signed up with PayPal, most major credit cards.</p>
<form action="<?php echo (($conf['paypal_is_sandbox'])? $conf['paypal_sandbox_url'] : $conf['paypal_url']); ?>" method="post">

  <!-- Identify your business so that you can collect the payments. -->
  <input type="hidden" name="business" value="<?php echo ($conf['paypal_is_sandbox']? $conf['paypal_sandbox_recipient_email'] : $conf['paypal_recipient_email']); ?>">

  <!-- Specify a Buy Now button. -->
  <input type="hidden" name="cmd" value="_xclick">

  <input type="hidden" name="invoice" value="<?php echo $order['hash']; ?>" />
 
  <!-- Specify details about the item that buyers will purchase. -->
  <input type="hidden" name="item_name" value="Agaf Design Order <?php echo $order_id; ?>">
  <input type="hidden" name="amount" value="<?php echo trim($cart_total); ?>">
  <input type="hidden" name="currency_code" value="<?php if ($lng=="pl") { echo "PLN"; } else { echo "EUR"; }?>">

  <!-- URLs -->
  <input type="hidden" name="return" value="<?php echo ($conf['paypal_is_sandbox']? $conf['paypal_sandbox_return_url'] : $conf['paypal_return_url']); ?>">
  <input type="hidden" name="cancel_return" value="<?php echo ($conf['paypal_is_sandbox']? $conf['paypal_sandbox_cancel_return_url'] : $conf['paypal_cancel_return_url']); ?>">
   
  <input type="hidden" name="notify_url" value="<?php echo ($conf['paypal_is_sandbox']? $conf['paypal_sandbox_notify_url'] : $conf['paypal_notify_url']); ?>">

  <!-- Display the payment button. -->
  <input type="image" name="submit" border="0" onclick="pay_submit()" 
  src="http://agafdesign.pl/images/site-images/buy-button-en.jpg"
  alt="Buy Now" style="float: right">
  <img alt="" border="0" width="1" height="1"
  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>


	
						
						
						<!-- <a href="reviewandpay.php">
							<button class="checkout-button" id="review-submit" type="submit" name="paypal_submit">
								<?php echo $lang['pay']; ?>
							</button>
							</form>
						</a> -->

						<div class="clearing"></div>
					</div>
				</div>

				<div class="clearing"></div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
    function pay_submit()
    {    	
        <?php $_SESSION['user_order_id'] = 0; ?>;
    }
</script>

<?php
require_once('footer.php');
?>


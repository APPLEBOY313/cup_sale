<?php
require_once('config.php');

$title="Agaf Design Products";
$meta_desc = "Area 16 - Shop Meta Description";

// sort out breadcrumbs
$breadcrumbs = array();
$breadcrumbs ['shop.php'] = $lang['shop-bread']; 

$cat="null";
if(isset($_GET["cat"])) { 
	$category=$_GET["cat"]; 
	$category_display = ucwords(str_replace("-"," ",$category));
	
	if ($category_display == "Serving Plates Bowls") { $category_display = "Serving Plates &amp; Bowls"; }
	if ($category_display == "Mugs") { $h2_title = "I'm a Ceramic Mug"; }
	
	}

	$sql_products = "SELECT * FROM products WHERE stock_level > 0 AND product_cat_".$lng." LIKE '%$category%' ORDER BY orderby ASC";
	$result_products = $conn->query($sql_products);
	
	if (!empty($category) && $result_products->num_rows > 0) {
		$title="Agaf Design Products - ".$category_display;
		$banner_line_class = $banner_line_class."-".$category;
		
		$this_cat_url = "shop.php?cat=".trim(strtolower($category));
		$breadcrumbs [$this_cat_url] = $category_display;
		$lastElementBreadcrumbs = $category_display;
	}
	
// breadcrumbs
// get category



require_once('header.php');

?>
<div id="banner-line"<?php if (!empty($banner_line_class)) { echo "class=\"".$banner_line_class." banner-line-shop-all\""; } ?>>
	<?php if (empty($category)) { ?>
	<div id="banner-line-logo" style="position: absolute; top: 50%; left: 50%; margin-top: -75px; margin-left: -75px;">
	<img src="images/site-images/shop-banner-logo.png" style="width: 150px" />
	</div>
	<?php } ?>
</div>



<!--<div id="product-listings">-->
    <div class="container">
	
		<div class="breadcrumbs-wrapper">
				<div class="col-xs-12">
					<div class="row">
						<p>
						<?php
						if (count($breadcrumbs)>1) {
							foreach ($breadcrumbs as $key => $value) {
							echo "<a href=\"".$key."\">".$value."</a>";
							if ( $value!=$lastElementBreadcrumbs )	{ echo " &gt; "; }								
							}
						}
						?>

						</p>
					</div>
				</div>
		 </div> <!-- /.breadcrumbs-wrapper -->
	
	
	
	
	
	
        <div class="row">
            <div class="shop-products-main-wrapper clearfix">
                <div class="col-xs-12">
                    <div class="shop-products-main-inner-wrapper clearfix">
					<?php

										
					if (!empty($category) && $result_products->num_rows > 0) {
					?>
					<h1><?php echo $title; ?></h1>
					<p style="text-align: center">All products are designed and hand made by Agaf Design Studios.</p>
					
					<?php
					if (!empty($h2_title)) { echo "<h2>".$h2_title."</h2>"; }

					while($row_products = $result_products->fetch_assoc()) {
							$product_imgs_array = explode(",",$row_products['product_images_en']);
							$lead_img = trim($product_imgs_array[0]);

							$price_display = ($lng=="en") ? $currency_symbol.$row_products['product_price_en'] : $row_products['product_price_pl']." ".$currency_symbol;

						?>
						<a href="product.php?product=<?php echo $row_products['product_url_'.$lng]; ?>">
						<div class="col-sm-4 col-xs-6">
							<div class="row">
								<div class="product-cell">
									<div class="product-img"><img src="images/product-images/<?php echo $lead_img; ?>" /></div>
									<div class="product-title">
										<div class="product-title-text"><?php echo $row_products['product_name_'.$lng]; ?></div>
										<div class="product-price"><?php echo $lang['price-main']; ?> <?php echo $price_display; ?></div>
									</div>
								</div>
							</div>
						</div>
						</a>


						<?php
							}
						}
						else
						{
						?>	
						<h1>Agaf Design Products</h1>
						
						<p>Forms surround us and affect us every day.

						<p>They have an impact on our experience, creativity, aesthetic sense and even on our mood. For these reasons they have to be created with thought.</p>

						<p>Forms should be functional, durable and beautiful but also have the ability to inspire, surprise, entertain, tell new stories and create a dialogue with the user.</p>

						<p>Such are present only when conceived with passion, imagination, attention to every detail and with respect for the audience. That's why Agaf Design Studio was created.</p>
						
						<p>All products which we have on offer in our shop are designed and hand made by Agaf Design Studio.</p>

						<div class="shop-categories-wrapper" style="width: 50%; margin: 0 auto;">
							<div class="shop-categories">
								<span class="cat-header">Drinkware</span>
								<ul>
								<li><a href="shop.php?cat=mugs">Mugs</a></li>
								<li><a href="shop.php?cat=cups">Cups</a></li>
								</ul>
							</div>
							
							<div class="shop-categories">
								<span class="cat-header">Tableware</span>
								<ul>
								<li><a href="shop.php?cat=serving-plates-bowls">Serving Plates &amp; Bowls</a></li>
								<li><a href="shop.php?cat=platters-bowls">Platters &amp; Bowls</a></li>
								</ul>
							</div>
							
							<div class="shop-categories">
								<a href="shop.php?cat=unique" class="cat-header">Unique</a>
							</div>
							
							<div class="shop-categories">
								<a href="shop.php?cat=interior-pieces" class="cat-header">Interior Pieces</a>
							</div>
						</div>
						<?php	
						}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
//
//	$sql_products = "SELECT * FROM products WHERE stock_level > 0 ORDER BY productid DESC";
//	$result_products = $conn->query($sql_products);
//
//	while($row_products = $result_products->fetch_assoc()) {
//		$product_imgs_array = explode(",",$row_products['product_images_en']);
//		$lead_img = trim($product_imgs_array[0]);
//
//		$price_display = ($lng=="en") ? $currency_symbol.$row_products['product_price_en'] : $row_products['product_price_pl']." ".$currency_symbol;
//
//	?>
<!--	<a href="product.php?product=--><?php //echo $row_products['product_url_'.$lng]; ?><!--">-->
<!--	<div class="product-cell">-->
<!--		<div class="product"></div>-->
<!--		<div class="product-img"><img src="images/product-images/--><?php //echo $lead_img; ?><!--" /></div>-->
<!--		<div class="product-title">-->
<!--			<div class="product-title-text">--><?php //echo $row_products['product_name_'.$lng]; ?><!--</div>-->
<!--			<div class="product-price">--><?php //echo $lang['price-main']; ?><!-- --><?php //echo $price_display; ?><!--</div>-->
<!--		</div>-->
<!--	</div>-->
<!--	</a>-->
<!--	-->
<!--	-->
<!--	--><?php	//
//	}
//	?>

<div class="clearing""></div>
<!--</div> -->
	
<?php	
require_once('footer.php');
?>
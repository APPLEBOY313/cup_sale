<!DOCTYPE html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
  <title><?php echo $title; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1" />  
	<meta name="description" content="<?php echo $meta_desc; ?>" />
	
	<meta name="language" content="English" />
	<meta name="copyright" content="link2light 2004-7" />
	<meta name="charset" content="utf-8" />
	
	<link href="css/themify-icons.css" rel="stylesheet">
	
<style type="text/css">
* { padding: 0; margin: 0; }

body {  font-family: 'Open Sans Condensed', sans-serif; color: #555; font-size: 14px;}

.clearing {clear:both;}

#header-wrapper { background: #fff; }
#header { width: 1100px; margin: 0 auto; height: 85px; }
#header-content { padding: 21px; }

#change_lng { float: left; }

#nav { font-family: "Arial","Helvetica","sans-serif"; font-size: 12px; text-transform: uppercase; letter-spacing: .07em; margin: 20px auto; width: 500px; text-align:center; }
#nav ul { list-style-type: none; padding: 10px 0; }
#nav ul li{ display: inline-block; padding: 0.6em; }
#nav ul li a { text-decoration: none; color: #707070; }
#nav ul li a:hover { color: #000; }

<?php if (!empty($full_width_header_img)) { $logo_line_height = ($homepage==true) ? '470' : '375';?>
#logo-line { height: <?php echo $logo_line_height; ?>px; text-align: center; background: url('images/site-images/<?php echo $full_width_header_img; ?>.jpg') no-repeat center center fixed; background-size: cover;}
<?php } else { ?>
#logo-line { text-align: center; background: #fff; }
<?php } ?>
#logo-holder { margin: 0 auto; }
#logo-holder img { margin: 30px auto; }

#contents-wrapper { background: #fff; }
#contents-holder { position: relative; width: 100% }

#product-listings { position: relative; margin: 50px auto; max-width: 1400px; }
#product-listings a{ color: #111, text-decoration: none; }

.product-cell { position: relative; float:left; width: 30%; padding-bottom: 30%; border: 1px solid #000; margin: 1%;  }
	.product-cell:hover .product-img, .product-cell.hover .product-img { opacity: 0.5;transition: opacity .2s ease-in-out; }
	.product-cell .product-img { opacity: 1;transition: opacity .2s ease-in-out; }
	.product-cell:hover .product-title, .product-cell.hover .product-title { opacity: 1;transition: opacity .2s ease-in-out; }
	.product-cell .product-title { opacity: 0;transition: opacity .2s ease-in-out; }	


.product-title { position: absolute; width: 100%; height: 100%; text-align:center; top: 50%;left: 50%; height: 30%; width: 50%; margin: -15% 0 0 -25%;}
.product-title-text { color: #111; text-decoration: none;}
.product { position: absolute; width: 100%; height: 100%;}
.product-img { position: absolute; width: 100%; height: 100%;}
.product-img img{ width: 100%;}

.product-title-text {font-size: 21px; font-weight: bold;}

#footer-wrapper { color: #555; background-color: #d4d4d4; }


.select-box number{	

    padding: 5px 8px;
    border: none;
    box-shadow: none;
    background: transparent;
    background-image: none;
}

/* -------------- cart styling ---------------- */
.mini-cart { float:right; position: relative; width: 50px; height: 55px; }
.mini-cart-img { position: absolute; }
.mini-cart-img img { width: 50px; }
.mini-cart-counter { position: absolute; width: 100%; height: 100%; text-align:center; top: 50%;left: 50%; height: 30%; width: 50%; margin: -15% 0 0 -25%; font-weight: 10px;}

#cart-listings { max-width: 1400px; margin: 0 auto; }
#cart-listings table {width: 100%; }
#cart-listings table th, #cart-listings table td { padding: 15px 0; border-bottom: 1px solid #ddd; }
#cart-listings table a {text-decoration: none; color: #555;}
#cart-listings table .cart-item {font-size: 17px; padding-left: 15px; } 
#cart-listings table .cart-img {text-align: left; }
#cart-listings table .cart-img, #cart-listings table .cart-remove {width: 100px;}
#cart-listings table .cart-qty-price, #cart-listings table .cart-remove {width: 100px; text-align: right;}
#cart-listings table .cart-qty-price{width: 150px;}

#checkout { max-width: 1200px; margin: 0 auto; }
.checkout-line { margin: 15px 5px 35px 0px; }
.checkout-button { clear:both; float: right; border-radius: 0; color: #272727; border: 2px solid #272727; padding: 1em 2.5em; letter-spacing: 1px; font-weight: bold; }
.checkout-button:hover { color: #fff; background-color: #272727; transition: background-color .2s ease-in-out; }

.customer-info-wrapper { float: left; width: 45%; border: 1px solid #ccc; border-radius: 5px; }
.order-summary-wrapper { float: right; width: 45%; border: 1px solid #ccc; border-radius: 5px; }

/* ------------------form styling -------------- */
.form-field { border-radius: 3px; border: 1px solid #ccc; margin: 10px 0px; }
.form-field-holder-half-width-left, .form-field-holder-half-width-right {width: 49%}
.form-field-holder-half-width-left {float: left;}
.form-field-holder-half-width-right {float: right;}
.form-field-holder-full-width {clear: both;}
.form-field .text-input { margin: 15px 9px 14px 9px;border: none; width: calc(100% - 30px); }
.form-field select { color: #555; }
.form-field .checkbox { margin: 15px 9px 14px 9px; }
</style>	
	
	
</head>
<body>
<div id="header-wrapper">
	<div id="header">
		<div id="header_content">
			<div id="change_lng">
			<?php if ($lng=="en") { ?>
			<form action="#" method="post">
			<input type="hidden" name="lng" value="pl"/>
			<input type="image" name="submit" src="images/site-images/flag_pl.png" alt="Polski" />
			</form>
			<?php } 
			if ($lng=="pl") { ?>
			<form action="#" method="post">
			<input type="hidden" name="lng" value="en"/>
			<input type="image" name="submit" src="images/site-images/flag_en.png" alt="English" />
			</form>
			<?php } ?>
			</div>
		
		
			<a href="cart.php">
			<div class="mini-cart">
				<div class="mini-cart-img">
				<img src="images/site-images/shopping-bag.png" />
				</div>
				<div class="mini-cart-counter">
				2
				</div>		
			</div>
			</a>
			<div id="nav">
				<ul>
					<li><a href="index.php"><?php echo $lang['home-page']; ?></a></li>
					<li><a href="shop.php">Shop</a></li>
					<li><a href="">About</a></li>
					<li><a href="">Stockists</a></li>
					<li><a href=""><?php echo $lang['contact']; ?></a></li>
			
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="logo-line">
	<div id="logo-holder">
	<img src="images/site-images/logo.png" />
	</div>
</div>

<div id="contents-wrapper">
	<div id="contents-holder">

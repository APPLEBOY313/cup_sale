<?php 

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/../functions.php');

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 0;
$json_output = '';

if ($action == 'update_order_status') {
	$orderid = (int) ((isset($_POST['order_id'])) ? $_POST['order_id'] : 0);
	$newstatus = (isset($_POST['new_status'])) ? $_POST['new_status'] : '';
	$shipping_info = (array) ((isset($_POST['shipping_info'])) ? $_POST['shipping_info'] : array());

	if (!$orderid | !$newstatus) {
		http_response_code(400);
	}

	update_order_status($orderid, $newstatus, $shipping_info);
}

header("Content-Type: application/json");

echo $json_output;

exit;

?>
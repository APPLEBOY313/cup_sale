<?php
define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/../functions.php');

$title = "Agaf Design Orders";
$meta_desc = "Agnieszka Fornalewska - Agaf Design";

require_once('./template/header.php');

function render_order_date($date) {
	if ($date) {
		return date("d.m.Y", strtotime($date));
	} else {
		return "N/A";
	}
}

function order_status_options_array() {
	return array(
		"ap" => "Waiting Payment",
		"pr" => "Payment Received",
		"p" => "Order Processed",
		"s" => "Order Sent"
	);
}

function order_status_options_html($selected_key) {
	$options = order_status_options_array();

	$output = '';

	foreach ($options as $key => $value) {
		$selected = '';

		if ($key == $selected_key) {
			$selected = "selected";
		}

		$output .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
	}

	return $output;
}

function order_courier_options_array() {
	return array(
		"dpd" => "DPD",
		"pp" => "Poczta Polska"
	);
}

function order_courier_options_html($selected_key) {
	$options = order_courier_options_array();

	$output = '';

	foreach ($options as $key => $value) {
		$selected = '';

		if ($key == $selected_key) {
			$selected = "selected";
		}

		$output .= '<option value="' . $key . '" ' . $selected . '>' . $value . '</option>';
	}

	return $output;
}

?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="header">
						<h4 class="title">Recent Orders</h4>
						<p class="category">List of recent orders (3 months span)</p>
					</div>
					<div class="content table-responsive table-full-width">
						<table class="table table-striped">
							<tr>
								<th>Order ID</th>
								<th>Date</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Address</th>
								<th>Invoice</th>
								<th>Order</th>
								<th>Status</th>
							</tr>
		
							<?php
								$sql_orders = "SELECT * FROM orders WHERE created_at <= (NOW() - INTERVAL 3 MONTH) OR created_at IS NULL ORDER BY id DESC ";
								$result_orders = $conn->query($sql_orders);
								
								if ($result_orders->num_rows > 0) {
									while($row_orders = $result_orders->fetch_assoc()) {
										$order_id = $row_orders['id'];
									echo "<tr>";
										echo "<td>".$row_orders['id']."</td>";
										echo "<td>".render_order_date($row_orders['created_at'])."</td>";
										echo "<td>".$row_orders['first_name']."</td>";
										echo "<td>".$row_orders['last_name']."</td>";
										echo "<td>".$row_orders['address_primary'];
										if (!empty($row_orders['address_secondary'])) { echo ",<br />".$row_orders['address_secondary'].",<br />"; } else { echo ",<br >"; }
										echo $row_orders['postcode']."<br />";
										echo $row_orders['city']."<br />";
										echo "<b>".$row_orders['country']."</b></td>";
										echo "<td>";
										if (!empty($row_orders['company_name'])) { echo "Y"; }
										echo "</td>";
										echo "<td>";
											$sql_orders_products = "SELECT * FROM order_product WHERE order_id='$order_id'";
											$result_orders_products = $conn->query($sql_orders_products);			
										
											while($row_orders_products = $result_orders_products->fetch_assoc()) {
												$product_id = $row_orders_products['product_id'];
												$qty_product = $row_orders_products['qty'];
												
												$sql_products = "SELECT * FROM products WHERE productid='$product_id'";
												$result_products = $conn->query($sql_products);	
												$row_products = $result_products->fetch_assoc();
												
												echo $qty_product." x ".$row_products['product_name_en']."<br />";
												
											}
										
										
										
										"</td>";
										echo '<td><select name="order_status" class="order-status-select">' . order_status_options_html($row_orders['status']) . '</select> <br />';
										echo '<div class="shipping_info hide">';
										echo '<label>Shipping Courier:</label> <br /><select name="shipping_info[courier]" class="courier">' . order_courier_options_html($row_orders['shipping_courier']) . '</select><br />';
										echo '<label for="tracking_no">Tracking Number:</label> <br /><input type="text" class="trackingno" name="shipping_info[tracking_no]" id="tracking_no" value="' . $row_orders['tracking_no'] .'" />';
										echo '</div>';
										echo '<button type="button" class="update-order-status" data-orderid="' . $row_orders['id'] . '">Update</button>';
										echo '</td>';
									echo "</tr>";
									}
								}

							?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

require_once('./template/footer.php');

?>

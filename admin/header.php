<!DOCTYPE html>
<head>
<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300:700|Dosis:300,800' rel='stylesheet' type='text/css'>
  <title><?php echo $title; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php echo $meta_desc; ?>" />

	<meta name="language" content="English" />
	<meta name="copyright" content="link2light 2004-7" />
	<meta name="charset" content="utf-8" />

	<link href="<?php echo $rootURL; ?>css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/featherlight.min.css" />
	<link rel="stylesheet" href="<?php echo $rootURL; ?>css/featherlight.gallery.min.css" />
	<link href="<?php echo $rootURL; ?>css/style.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo $rootURL; ?>css/slick.css"/>
	<link rel="stylesheet" type="text/css" href="<?php echo $rootURL; ?>css/slick-theme.css"/>

</head>
<body>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-51828539-1', 'auto');
  ga('send', 'pageview');

</script>

<div id="page-wrapper">
<div id="header-wrapper">
	<div id="header">
		<div class="container">
			<div class="row">
				<div class="header-inner-wrapper">
					<div class="col-md-12">
						<div class="row">
							<div id="header_content">
								<div id="new-logo">
									<a href="index.php"><img src="<?php echo $rootURL; ?>images/site-images/logo-black-on-white.jpg" alt="logo-picture" /></a>
									
									<div class="menu-button-wrapper">
										<span><i id="navicon" class="fa fa-bars" aria-hidden="true"></i></span>
									</div>		
									
									<div id="nav">
										<ul>
											<li><a href="index.php"><?php echo $lang['home-page']; ?></a></li>
											<li><a href="about.php"><?php echo $lang['about-nav-menu']; ?></a></li>
											<li><a href="shop.php"><?php echo $lang['shop-bread']; ?></a></li>
										<!--	<li><a href="shop.php">Design</a></li>
											<li><a href="shop.php">News</a></li>
											<li><a href="shop.php">Press</a></li> -->
											<li><a href="contact.php"><?php echo $lang['contact']; ?></a></li>
											<li class="mobile-only"><a href="cart.php">Cart ( <?php isset($_SESSION['product_num']) ? print_r( $_SESSION['product_num']) : print_r(0); ?> )</a></li>
											<li class="mobile-only">
											<form action="<?php echo $form_action; ?>" method="post">
												<input type="hidden" name="lng" value="en"/>
												<input type="submit" name="submit" value="EN" />
											</form>
											
											<form action="<?php echo $form_action; ?>" method="post">
												<input type="hidden" name="lng" value="pl"/>
												<input type="submit" name="submit" value="PL" />
											</form>
											</li>

										</ul>

									</div>
									
						
								
								
								
								
								
								</div>
								



							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div id="contents-wrapper">
	<div id="contents-holder">
<?php

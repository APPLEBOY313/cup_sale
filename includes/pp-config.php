<?php

return array(
    // set your paypal credential
    'client_id' => 'AYEqcgswyyrmC63qzY5Lq4JwMsdn5cTZ7PcwXyWnbojvIaNPK2NQwUzhwSF5Lir4yQr92y7aYDo7mRCK',
    'secret' => 'EKTfYlfCEgTX06YE_iXj9YIEkXip_dgJLN7P9fNBpzl6L0z3mArKnGJuQLtrZ3SJar6L-TkNR2z_F0UJ',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => __DIR__. '\logs\paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);
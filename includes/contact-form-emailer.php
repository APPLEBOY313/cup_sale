<?php
require_once('../config.php');

if(isset($_POST['firstname']) && isset($_POST['lastname']) && isset($_POST['emailadress']) && isset($_POST['message']) ) {
	
		$firstname = mysqli_real_escape_string($conn,$_POST['firstname']);
		$lastname = mysqli_real_escape_string($conn,$_POST['lastname']);
		$emailadress = mysqli_real_escape_string($conn,$_POST['emailadress']);
		$phone = mysqli_real_escape_string($conn,$_POST['phone1'])." ".mysqli_real_escape_string($conn,$_POST['phone2'])." ".mysqli_real_escape_string($conn,$_POST['phone3'])." ".mysqli_real_escape_string($conn,$_POST['phone4']);
		if (mysqli_real_escape_string($conn,$_POST['subject']) != "") { $subject = ": ".mysqli_real_escape_string($conn,$_POST['subject']); }
		$message = mysqli_real_escape_string($conn,$_POST['message']);
		
		if (trim($firstname)==false || trim($lastname)==false || trim($emailadress)==false || trim($message)==false) {
		header('Location: ../contact.php');	
		exit;	
		}
		else
		{
			$message = "Agaf Design website enquiry from $firstname $lastname\r\n\r\n".$message."\r\n\r\nTelephone: ";
			if (strlen($phone<6)) { $message .="No telephone number given"; } else { $message .="$phone";  }
			
			$from="From: $firstname $lastname<$emailadress>\r\nReturn-path: $email";
			$subject="Agaf Design website enquiry $subject";
			mail("agafdesign@gmail.com", $subject, $message, $from);	
			
			header('Location: ../contact.php?status=message-posted');	
			exit;
			
		}
}
else
{
header('Location: ../contact.php');	
exit;
}
?>
<?php

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - ".$lang['contact'];
$meta_desc = "Contact Agaf Design Studio";

$banner_img="images/site-images/banner-contact-img.jpg";

$form_data = array(
	'firstname' => '',
	'lastname' => '',
	'emailadress' => ''
);

if (isset($_POST['signup_submit'])) {
	foreach ($form_data as $key => $value) {
		if (isset($_POST[$key])) {
			$form_data[$key] = $_POST[$key];
		}
	}
}

require_once('header.php');
?>
<div id="banner-line" class="banner-line-shop-all">
	<div id="banner-line-image" style="display: none; height: 375px; width: 100%;">
	</div>
</div>

<div class="contact-main-wrapper"> <!-- START OF FORM AND CONTACT -->
    <div class="contact-main-inner-wrapper">
        <div class="form-and-contact-main-wrapper">
            <div class="form-and-contact-inner-wrapper">
                <div class="container">
                    <div class="row">
						<h2>Register</h2>
                        <div class="form-main-wrapper clearfix"> <!-- START OF FORM MAIN WRAPPER -->
                            <div class="col-md-offset-1 col-md-6">
                                <div class="row">
                                    <div class="form-wrapper">
										<?php if (is_complete_state()): ?>
											<div class="alert alert-success"><?php echo get_state_message(); ?></div>
										<?php else: ?>
											<?php if (is_error_state()): ?>
											<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
											<?php endif; ?>
											<form action="register.php" method="post" id="submit-register" class="signup-form">
												<div class="form-inner-wrapper clearfix">
													<!-- Name -->
													<div class="col-md-12">
														<div class="row">
															<div class="input-field-inner-wrapper clearfix">

																<p class="contact-full-name"><?php echo $lang['name-main']; ?> <span>*</span></p>
																<div class="col-md-6 col-xs-12 no-padding-left">
																		<div class="first-name-custom">
																			<input name="firstname" type="text"
																				   placeholder="" id="fname" value="<?php echo $form_data['firstname']; ?>">
																			<label><?php echo $lang['first-name']; ?></label>
																		</div>
																</div>
																<div class="col-md-6 col-xs-12 no-padding">
																		<div class="last-name-custom">
																			<input name="lastname" type="text"
																				   placeholder="" id="lname" value="<?php echo $form_data['lastname']; ?>">
																		   <label><?php echo $lang['last-name']; ?></label>
																		</div>
																</div>
															</div>
														</div>
													</div>
													<!-- Email -->
													<div class="col-xs-12">
														<div class="row">
															<div class="input-field-inner-wrapper">
																<p><?php echo $lang['e-mail-main']; ?> <span>*</span></p>
																<input class="email-custom" type="text" name="emailadress" id="email" value="<?php echo $form_data['emailadress']; ?>">
															</div>
														</div>
													</div>
													<!-- Password -->
													<div class="col-xs-12">
														<div class="row">
															<div class="input-field-inner-wrapper">
																<p><?php echo $lang['password']; ?> <span>*</span></p>
																<input class="email-custom" type="password" name="password" id="password" value="">
															</div>
														</div>
													</div>
													<!-- Confirm Password -->
													<div class="col-xs-12">
														<div class="row">
															<div class="input-field-inner-wrapper">
																<p>Confirm Password: <span>*</span></p>
																<input class="email-custom" type="password" name="passwordconfirm" id="passwordconfirm" value="">
															</div>
														</div>
													</div>
													<div class="col-xs-12">
														<div class="row">
															<div class="submit-button-wrapper">
																<input type="submit" value="<?php echo $lang['submit-main']; ?>" id="signup-submit-<?php echo $lng; ?>" name="signup_submit">
															</div>
														</div>
													</div>
												</div>
											</form>
										<?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- END OF FORM MAIN WRAPPER -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- END OF FORM AND CONTACT -->

<img id="preload" src="<?php echo $banner_img; ?>" style="display:none;" />
<?php
require_once('footer.php');
?>

<?php

class DbException extends \Exception {};

class MaxLoginAttemptsReached extends \Exception {};
class CannotRemoveDefaultAddressException extends \Exception {};
class MailDeliveryFailedException extends \Exception {};
class AddressNotFoundException extends \Exception {};
class CustomerExistsException extends \Exception {};
class CustomerAccountNotActiveException extends \Exception {};
class InvalidLoginCredentialsException extends \Exception {};
class DataNotValidException extends \Exception {};
class DataNotFoundException extends \Exception {};

function pricedisplay($price,$lng) {  
	$price_display = ($lng=="en") ? "&euro;".$price : $price." zł"; 
	return $price_display;
}

function get_currency_by_lang($lng) {
	return (($lng == "en") ? "EUR" : "PLN");
}

function calculateshipping ($country, $conn, $europe) {
		$shipping_cost = 0;
		
	    if($country == 'Poland'){
            $shipping_qty_text = "poland";
        } else if(array_search($country, $europe)){
            $shipping_qty_text = "europe";
        } else {
            $shipping_qty_text = "worldwide";
        }
		
		$cart_content = $_SESSION['cart'];
		$lng = $_SESSION['lng'];
		$nr_items_in_cart = array_sum($cart_content['quantity']);
		
		$productid_arr = $cart_content['id'];
		$productqty_arr = $cart_content['quantity'];

		$i=0;
		foreach ($productid_arr as $productid) {
			$cart_content_arr[$productid] = $productqty_arr[$i];
			$i++;
		}
	
	if ($nr_items_in_cart==1) {
		$productid = $cart_content['id'][0];
			$shipping_query = "SELECT shipping_alone_".$shipping_qty_text."_".$lng." FROM products WHERE productid = $productid"; 
			$shipping_result=mysqli_query($conn, $shipping_query);
			$shipping_data = mysqli_fetch_assoc($shipping_result);
			$shipping_cost = $shipping_data['shipping_alone_'.$shipping_qty_text.'_'.$lng];
			$_SESSION['shipping_price_' . $lng] = $shipping_cost;
	}

	if ($nr_items_in_cart>1) { 

		
		foreach ($cart_content_arr as $productid => $productqty) {	
			$shipping_query = "SELECT shipping_with_other_product_".$shipping_qty_text."_".$lng." FROM products WHERE productid = $productid"; 
			$shipping_result=mysqli_query($conn, $shipping_query) or die("ouch");
			$shipping_data = mysqli_fetch_assoc($shipping_result);
			$shipping_cost = $shipping_cost + ($shipping_data['shipping_with_other_product_'.$shipping_qty_text.'_'.$lng]*$productqty); 			
		}
		$_SESSION['shipping_price_' . $lng] = $shipping_cost;

	}
	
	return $shipping_cost;
}

function full_url() {
	// $url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$url = "localhost:8080//cup";
	return $url;	
}

function is_dashboard() {
	$url = full_url();

	if (strpos($url, 'dashboard') !== false) {
		return true;
	}

	return false;
}

function is_dashboard_login() {
	$url = full_url();

	if (strpos($url, 'dashboard/login.php') !== false) {
		return true;
	}

	return false;
}

function root_url() {
	global $rootURL;

	return $rootURL;
}

function dashboard_index_url() {
	global $rootURL;

	return $rootURL . 'dashboard/index.php';
}

function dashboard_login_url() {
	global $rootURL;

	return $rootURL . 'dashboard/login.php';
}

function forgotten_password_url() {
	global $rootURL;

	return $rootURL . 'forgottenpass.php';
}

function account_validation_url($hash = '') {
	global $rootURL;

	return $rootURL . 'accountvalidate.php?hash=' . $hash;
}

function resent_validation_url($hash = '') {
	global $rootURL;

	return $rootURL . 'sendactivation.php?hash=' . $hash;	
}

function reset_password_url($hash = '') {
	global $rootURL;

	return $rootURL . 'resetpass.php?hash=' . $hash;
}

function dashboard_address_book_url() {
	global $rootURL;

	return $rootURL . 'dashboard/index.php';
}

function dashboard_addressbook_editaddress_url($address_id) {
	global $rootURL;

	return $rootURL . 'dashboard/addressbook-edit.php?id=' . $address_id;	
}

function dashboard_addressbook_removeaddress_url($address_id) {
	global $rootURL;

	return $rootURL . 'dashboard/addressbook-remove.php?id=' . $address_id;	
}

function dashboard_addressbook_makedefaultaddress_url($address_id) {
	global $rootURL;

	return $rootURL . 'dashboard/addressbook-makedefault.php?id=' . $address_id;
}

function dashboard_add_address_url() {
	global $rootURL;

	return $rootURL . 'dashboard/addressbook-add.php';
}

function dashboard_change_password_url() {
	global $rootURL;

	return $rootURL . 'dashboard/changepass.php';
}

function dashboard_order_history_url() {
	global $rootURL;

	return $rootURL . 'dashboard/orderhistory.php';
}

function is_authenticated() {
	return (isset($_SESSION['user']) && $_SESSION['user']);
}

function redirect_to_checkout() {
	global $rootURL;

	header("Location: " . $rootURL . 'checkout.php');
	exit;	
}

function redirect_to_dashboard_login() {
	global $rootURL;

	header("Location: " . $rootURL . 'dashboard/login.php');
	exit;
}

function redirect_to_dashboard_index() {
	global $rootURL;

	header("Location: " . $rootURL . 'dashboard/index.php');
	exit;
}

function redirect_to_dashboard_addressbook_index() {
	header("Location: " . dashboard_address_book_url() );
	exit;
}

function redirect_to_home() {
	global $rootURL;

	header("Location: " . $rootURL);
	exit;
	
}

function get_auth_user() {
	if (!is_authenticated()) {
		return null;
	}

	$userID = $_SESSION['user'];

	$user = db_get_user_by_id($userID);

	return $user;
}

function get_auth_user_id() {
	if (!is_authenticated()) {
		return null;
	}

	$userID = $_SESSION['user'];

	$user = db_get_user_by_id($userID);

	return $user['id'];
}

//------------ mail get -------------
function get_auth_user_mail() {
	if (!is_authenticated()) {
		return null;
	}

	$userID = $_SESSION['user'];

	$user = db_get_user_by_id($userID);

	return $user['email'];
}




function db_get_user_by_id($userID) {
	global $conn;

	$query = 'SELECT * FROM customers WHERE id = "' . $userID . '"';
	$result = $conn->query($query);

	$row_customer = $result->fetch_assoc();

	return $row_customer;
}

function dashboard_logout_url() {
	global $rootURL;

	return $rootURL . 'dashboard/logout.php';
}

function register_url() {
	global $rootURL;

	return $rootURL . 'register.php';
}

function logout() {
	unset($_SESSION['user']);
}

function redirect_to_index() {
	global $rootURL;

	header("Location: " . $rootURL);
	exit;
}

function validate_address_type($type) {
	$valid = true;

	if ($type != 'S' && $type != 'B') {
		$valid = false;
	}

	return $valid;
}

function validate_address($address_data) {
	$valid = true;

	if (!isset($address_data['type'])) {
		$valid = false;
	} else {
		if (!validate_address_type($address_data['type'])) {
			$valid = false;
		}
	}

	if (!isset($address_data['first_name'])) {
		$valid = false;
	} else {
		if ($address_data['first_name'] == '') {
			$valid = false;
		}
	}

	if (!isset($address_data['last_name'])) {
		$valid = false;
	} else {
		if ($address_data['last_name'] == '') {
			$valid = false;
		}
	}

	if (!isset($address_data['address_line_1'])) {
		$valid = false;
	} else {
		if ($address_data['address_line_1'] == '') {
			$valid = false;
		}
	}

	if (!isset($address_data['address_line_2'])) {
		$valid = false;
	}

	if (!isset($address_data['country'])) {
		$valid = false;
	} else {
		if (!validate_country($address_data['country'])) {
			$valid = false;
		}
	}

	if (!isset($address_data['postcode'])) {
		$valid = false;
	} else {
		if ($address_data['postcode'] == '') {
			$valid = false;
		}
	}

	if (!isset($address_data['city'])) {
		$valid = false;
	} else {
		if ($address_data['city'] == '') {
			$valid = false;
		}
	}

	return $valid;
}

function validate_country($country) {
	global $countries;

	$valid = true;

	if (!isset($countries[$country])) {
		$valid = false;
	}

	return $valid;
}

function db_update_customer_account_set_active($customer_email) {
	$sql = "UPDATE customers SET is_active = 1 WHERE email = '%s'";

	db_query(sprintf($sql, $customer_email));
}

function validate_customer_password($password) {
	$valid = true;

	if (strlen($password) < 8) {
		$valid = false;
	}

	if (!preg_match('/[a-z]/i', $password) || !preg_match('/[0-9]/i', $password) || !preg_match('/[\@\#\$\%\^\&\*\(\)\_\+\!]/i', $password)) {
		$valid = false;
	}

	return $valid;
}

function validate_customer_email($email) {
	$valid = true;

	$pattern = '/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/';

	if (!preg_match($pattern, $email)) {
		$valid = false;
	}

	return $valid;
}

function validate_customer($customer_data) {
	$valid = true;

	if (!isset($customer_data['email'])) {
		$valid = false;
	} else {
		if (!validate_customer_email($customer_data['email'])) {
			$valid = false;
		}
	}

	if (!isset($customer_data['password'])) {
		$valid = false;
	} else {
		if (!validate_customer_password($customer_data['password'])) {
			$valid = false;
		}
	}

	if (!isset($customer_data['first_name'])) {
		$valid = false;
	} else {
		if ($customer_data['first_name'] == '') {
			$valid = false;
		}
	}

	if (!isset($customer_data['last_name'])) {
		$valid = false;
	} else {
		if ($customer_data['last_name'] == '') {
			$valid = false;
		}
	}

	return $valid;
}

function db_query($prepared_sql) {
	global $conn;

	$result = $conn->query($prepared_sql);

	if (!$result) {
		throw new DbException('Query failed: ' . mysqli_error($conn));
	}

	return $result;
}

function db_get_last_insert_id() {
	global $conn;
	
	return mysqli_insert_id($conn);
}

function db_insert_lang($key) {
	global $conn, $env;

	$sql = "INSERT IGNORE INTO `languages` (`langid`, `lang_type`, `lang_code`, `en`, `pl`) VALUES (NULL, 'txt', '%s', '%s', '');";

	db_query(sprintf($sql, $key, $key));
}

function db_insert_account_validation_request($email, $hash) {
	global $conn, $env;

	$email = mysqli_real_escape_string($conn, $email);
	$hash = mysqli_real_escape_string($conn, $hash);

	$datetimeNow = date("Y-m-d H:i:s");
	
	$sql = "INSERT INTO account_validation_requests (email, hash, created_at) values('%s', '%s', '%s')";

	db_query(sprintf($sql, $email, $hash, $datetimeNow));
}

function db_insert_forgotten_password_request($email, $hash) {
	global $conn, $env;

	$email = mysqli_real_escape_string($conn, $email);
	$hash = mysqli_real_escape_string($conn, $hash);

	$datetimeNow = date("Y-m-d H:i:s");
	
	$sql = "INSERT INTO forgotten_password_requests (email, hash, created_at) values('%s', '%s', '%s')";

	db_query(sprintf($sql, $email, $hash, $datetimeNow));
}

function db_insert_order_products($orderid, $order_products) {
	global $conn;

	$datum = new DateTime();
	$startTime = $datum->format('Y-m-d-H:i:s');

	$insert_values_str = '';

	foreach ($order_products as $order_product) {
		$row = array(
			$order_product['product']['productid'],
			$orderid,
			$order_product['qty'],
			$order_product['price'],
			"'" . $order_product['currency'] . "'",
			"'" . $startTime . "'"
		);

		$insert_values_str .= '(' . implode(',', $row) . '),';
	}

	$insert_values_str = trim($insert_values_str, ',');

	$sql = "INSERT INTO order_product(
		product_id, order_id, qty, product_price, currency, created_at
		) VALUES 
			$insert_values_str
		";

	db_query($sql);
}

function db_is_default_address($customer_id, $address_id) {
	$address = db_find_customer_address_by_id($customer_id, $address_id);

	if (!$address) {
		throw new DataNotFoundException("");
	}

	$default_address = db_find_customer_default_address($customer_id, $address['type']);

	if ($default_address['id'] == $address['id']) {
		return true;
	} else {
		return false;
	}
}

function db_set_customer_default_address($customer_id, $address_id) {
	global $conn;

	$address = db_find_customer_address_by_id($customer_id, $address_id);

	if (!$address) {
		throw new Exception("Address not found");
	}

	$type = $address['type'];

	if ($type == 'S') {
		$column = "default_shipping_address_id";
	} else if ($type == 'B') {
		$column = "default_billing_address_id";
	}
	
	$sql = "UPDATE customers SET $column = %d WHERE id = %d";

	db_query(sprintf($sql, $conn->real_escape_string($address_id), $conn->real_escape_string($customer_id)));
}

function db_find_customer_by_email($customer_email) {
	global $conn, $env;

	$sql = "SELECT * FROM customers WHERE email = '%s'";

	$result = db_query(sprintf($sql, $customer_email));

	$row = $result->fetch_assoc();

	return $row;
}

function db_find_customer_address_by_id($customer_id, $address_id) {
	global $conn, $env;

	$sql = "SELECT * FROM customer_addresses WHERE customer_id = %d AND id = %d";

	$result = db_query(sprintf($sql, $customer_id, $address_id));

	$row = $result->fetch_assoc();

	return $row;
}

function db_find_customer_addresses($customer_id, $options = array()) {
	global $conn, $env;

	$type = (isset($options['type'])) ? $options['type'] : '';

	$base_sql = "SELECT * FROM customer_addresses WHERE customer_id = %d";

	if ($type) {
		$sql = $base_sql . " AND type = '%s'";

		$result = db_query(sprintf($sql, $customer_id, mysqli_real_escape_string($conn, $type)));
	} else {
		$result = db_query(sprintf($sql, $customer_id));
	}

	$rows = array();

	while ($row = $result->fetch_assoc()) {
		$rows[] = $row;
	}

	return $rows;
}

function db_find_orders_by_customer_id($customer_id) {
	global $conn, $env;

	$sql = "SELECT * FROM orders WHERE customer_id = %d";

	$result = db_query(sprintf($sql, mysqli_real_escape_string($conn, $customer_id)));

	$rows = array();

	while ($row = $result->fetch_assoc()) {
		$rows[] = $row;
	}

	return $rows;
}

function db_find_order_by_id_with_customer($order_id) {
	global $conn, $env;

	$sql = "SELECT o.*, c.email AS customer_email FROM orders AS o INNER JOIN customers AS c ON c.id = o.customer_id WHERE o.id = %d";

	$result = db_query(sprintf($sql, mysqli_real_escape_string($conn, $order_id)));

	$row = $result->fetch_assoc();

	return $row;	
}

function db_find_order_by_id($order_id) {
	global $conn, $env;

	$sql = "SELECT * FROM orders WHERE id = %d";

	$result = db_query(sprintf($sql, mysqli_real_escape_string($conn, $order_id)));

	$row = $result->fetch_assoc();

	return $row;
}

function db_find_products_by_order_id($order_id) {
	global $conn, $env;

	$sql = "
		SELECT 
			op.qty AS qty, 
			op.product_price AS product_price, 
			p.* FROM products AS p 
		INNER JOIN order_product AS op ON p.productid = op.product_id 
		WHERE op.order_id = %d";

	$result = db_query(sprintf($sql, mysqli_real_escape_string($conn, $order_id)));

	$rows = array();

	while ($row = $result->fetch_assoc()) {
		$rows[] = $row;
	}

	return $rows;
}

function db_find_product_by_id($product_id) {
	global $conn, $env;

	$sql = "
		SELECT 
			*
		FROM products
		WHERE productid = %d";

	$result = db_query(sprintf($sql, mysqli_real_escape_string($conn, $product_id)));

	$row = $result->fetch_assoc();

	return $row;
}

function db_mark_account_validation_request_as_used($hash) {
	$sql = "UPDATE account_validation_requests SET is_used = 1 WHERE hash = '%s'";

	$result = db_query(sprintf($sql, $hash));
}

function db_mark_account_validation_request_as_sent($hash) {
	$sql = "UPDATE account_validation_requests SET is_sent = 1 WHERE hash = '%s'";

	$result = db_query(sprintf($sql, $hash));
}

function db_find_account_validation_request_by_hash($hash) {
	global $conn, $env;

	$sql = "SELECT * FROM account_validation_requests WHERE hash = '%s' AND created_at > (NOW() - INTERVAL 24 HOUR) AND is_used = 0";

	$result = db_query(sprintf($sql, $hash));

	$row = $result->fetch_assoc();

	return $row;
}

function db_find_forgotten_password_request_by_hash($hash) {
	global $conn, $env;

	$sql = "SELECT * FROM forgotten_password_requests WHERE hash = '%s' AND created_at > (NOW() - INTERVAL 24 HOUR)";

	$result = db_query(sprintf($sql, $hash));

	$row = $result->fetch_assoc();

	return $row;
}

function db_remove_forgotten_password_request_by_hash($hash) {
	global $conn;

	$hash = mysqli_real_escape_string($conn, $hash);

	$sql = "DELETE FROM forgotten_password_requests WHERE hash = '%s'";

	db_query(sprintf($sql, $hash));
}

function db_change_customer_password($username, $password) {
	global $conn, $env;

	$username = mysqli_real_escape_string($conn, $username);
	$password = mysqli_real_escape_string($conn, $password);

	$sql = "UPDATE customers SET password = MD5('%s') WHERE email = '%s'";

	$result = db_query(sprintf($sql, $password, $username));
}

function db_find_customer_by_id($customer_id) {
	$customer = db_get_user_by_id($customer_id);

	return $customer;
}

function db_find_address_by_id($customer_id, $address_id) {
	global $conn, $env;

	$sql = "SELECT * FROM customer_addresses WHERE customer_id = %d AND id = %d";

	$result = db_query(sprintf($sql, $customer_id, $address_id));

	$row = $result->fetch_assoc();

	return $row;
}

function db_find_customer_default_address($customer_id, $type) {
	$customer = db_find_customer_by_id($customer_id);

	if ($type == 'B') {
		$default_address_id = $customer['default_billing_address_id'];
	} else if ($type == 'S') {
		$default_address_id = $customer['default_shipping_address_id'];
	}

	$default_address = db_find_address_by_id($customer_id, $default_address_id);

	return $default_address;
}

function db_set_default_customer_address($customer_id, $address_id, $type) {
	global $conn, $env;

	if ($type == 'B') {
		$col = 'default_billing_address_id';
	} else {
		$col = 'default_shipping_address_id';
	}

	$sql = "
		UPDATE customers SET
			%s = %d
		WHERE 
			id = %d
	";

	$result = db_query(sprintf($sql,
		$col,
		$conn->real_escape_string($address_id), 
		$conn->real_escape_string($customer_id)
	));
}

function db_insert_customer_address($customer_id, $address_data) {
	global $conn, $env;

	$sql = "INSERT INTO customer_addresses (customer_id, type, first_name, last_name, city, country, postal_code, address_line_1, address_line_2) values(%d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')";

	db_query(sprintf($sql, 
		$conn->real_escape_string($customer_id), 
		$conn->real_escape_string($address_data['type']),
		$conn->real_escape_string($address_data['first_name']),
		$conn->real_escape_string($address_data['last_name']),
		$conn->real_escape_string($address_data['city']), 
		$conn->real_escape_string($address_data['country']), 
		$conn->real_escape_string($address_data['postcode']), 
		$conn->real_escape_string($address_data['address_line_1']), 
		$conn->real_escape_string($address_data['address_line_2'])
	));
}

function db_insert_customer_shipping_address($customer_id, $address_data) {
	$address_data['type'] = 'S';
	db_insert_customer_address($customer_id, $address_data);
}

function db_insert_customer_billing_address($customer_id, $address_data) {
	$address_data['type'] = 'B';
	db_insert_customer_address($customer_id, $address_data);
}

function db_update_customer_address($customer_id, $address_id, $address_data) {
	global $conn, $env;

	$sql = "
		UPDATE customer_addresses SET
			type = '%s',
			city = '%s',
			country = '%s',
			postal_code = '%s',
			address_line_1 = '%s',
			address_line_2 = '%s'
		WHERE 
			customer_id = %d AND id = %d
	";

	$result = db_query(sprintf($sql, 
		$conn->real_escape_string($address_data['type']), 
		$conn->real_escape_string($address_data['city']), 
		$conn->real_escape_string($address_data['country']), 
		$conn->real_escape_string($address_data['postcode']), 
		$conn->real_escape_string($address_data['address_line_1']), 
		$conn->real_escape_string($address_data['address_line_2']),
		$conn->real_escape_string($customer_id),
		$conn->real_escape_string($address_id)
	));
}

function login_by_id($customer_id) {
	$_SESSION['user'] = $customer_id;
}

function register_customer_account($customer_data) {
	global $conn;

	if (!validate_customer($customer_data)) {
		throw new DataNotValidException("");
	}

	$customer_data['password'] = md5($customer_data['password']);

	db_insert_customer($customer_data);
	$new_customer_id = mysqli_insert_id($conn);

	$customer_email = $customer_data['email'];

	$hash = generate_account_validation_hash($customer_email);
	db_insert_account_validation_request($customer_email, $hash);

	send_account_validation_email($customer_email, $hash);

	return $new_customer_id;
}

function db_remove_customer_address($customer_id, $address_id) {
	global $conn;

	$sql = "DELETE FROM customer_addresses WHERE customer_id = %d AND id = %d";

	db_query(sprintf($sql, $customer_id, $address_id));
}

function db_insert_customer($customer_data) {
	global $conn, $env;

	$sql = "INSERT INTO customers (email, password, first_name, last_name) values('%s', '%s', '%s', '%s')";

	$result = db_query(sprintf($sql, 
		$conn->real_escape_string($customer_data['email']), 
		$conn->real_escape_string($customer_data['password']),
		$conn->real_escape_string($customer_data['first_name']),
		$conn->real_escape_string($customer_data['last_name'])
	));

}

function db_get_last_error() {
	global $conn;

	return mysqli_error($conn);
}

function db_insert_login_attempt($username) {
	global $conn, $env;

	$ip_address = $env['remote_ip'];
	$datetimeNow = date("Y-m-d H:i:s");
	$result = $conn->query(sprintf("INSERT INTO login_attempts (ip, attempts, lastlogin, username) values('%s', 1, '%s', '%s')", $ip_address, $datetimeNow, $username));

	if (!$result) {
		throw new DbException('Query failed');
	}
}

function db_get_login_attempts($username) {
	global $conn, $env;

	$ip_address = $env['remote_ip'];
	$result = $conn->query(sprintf("SELECT * FROM login_attempts WHERE username = '%s'", $username));

	$row = $result->fetch_assoc();

	return $row;
}

function db_update_login_attempt($username) {
	global $conn, $conf, $env;

	$row_attempts = db_get_login_attempts($username);

	if ($row_attempts) {
		$curr_attempts = $row_attempts['attempts'];
		$oldTime = strtotime($row_attempts['lastlogin']);
	} else {
		$curr_attempts = 0;
		$oldTime = 0;
	}

	$login_timeout = $conf['login_timeout'];
	$max_attempts = $conf['max_login_attempts'];
	$ip_address = $env['remote_ip'];
	$datetimeNow = date("Y-m-d H:i:s");
	$newTime = strtotime($datetimeNow);
	$timeDiff = $newTime - $oldTime;

	$sql = "UPDATE login_attempts SET attempts = %d, lastlogin = '%s' where ip = '%s' and username = '%s'";

	if ($timeDiff < $login_timeout) {
		$query = sprintf($sql, ++$curr_attempts, $datetimeNow, $ip_address, $username);
		$conn->query($query);
	} else {
		$query = sprintf($sql, 1, $datetimeNow, $ip_address, $username);
		$conn->query($query);
	}
}

function db_mark_order_as_complete($orderid) {
	$sql = "UPDATE orders SET status = 'p', status_change_date = NOW() WHERE id = %d";

	db_query(sprintf($sql, $orderid));
}

function db_mark_order_as_sent($orderid, $shipping_info) {
	$sql = "UPDATE orders SET status = 's', shipping_courier = '%s', tracking_no = '%s', status_change_date = NOW()  WHERE id = %d";

	db_query(sprintf($sql, $shipping_info['courier'], $shipping_info['tracking_no'], $orderid));
}

function db_mark_order_as_processed($orderid) {
	$sql = "UPDATE orders SET status = 'p', status_change_date = NOW() WHERE id = %d";

	db_query(sprintf($sql, $orderid));	
}

function db_mark_order_as_payment_received($orderid) {
	$sql = "UPDATE orders SET status = 'pr', status_change_date = NOW() WHERE id = %d";

	db_query(sprintf($sql, $orderid));		
}

function update_order_status($orderid, $newstatus, $shipping_info = array()) {
	if ($newstatus == 's') {
		db_mark_order_as_sent($orderid, $shipping_info);

		send_order_sent_email_notification_to_client($orderid);
	} else if ($newstatus == 'p') {
		db_mark_order_as_processed($orderid);

		send_order_processed_email_notification_to_client($orderid);
	} else if ($newstatus == 'pr') {
		db_mark_order_as_payment_received($orderid);

		send_order_payment_received_email_notification_to_client($orderid);
	}
}

function calculate_order_total_price($orderid) {
	global $env;

	$total_price = 0;
	$products = db_find_products_by_order_id($orderid);

	foreach ($products as $product) {
		$total_price += $product['product_price_' . $env['lang']];
	}

	return $total_price;
}

function cart_get_total_price() {
	global $env;

	$order_products = cart_get_order_products();
	$total_price = 0;

	foreach ($order_products as $order_product) {
		$total_price += $order_product['product']['product_price_' . $env['lang']];
	}

	return $total_price;
}

function cart_get_shipping_price($shipping_country) {
	global $env, $europe, $cache;

	if (isset($cache['cart_shipping_price'])) {
		return $cache['cart_shipping_price'];
	}

	$order_products = cart_get_order_products();
	$shipping_price = 0;

	if($shipping_country == 'Poland') {
		$shipping_qty_text = "poland";
	} else if(array_search($shipping_country, $europe)){
		$shipping_qty_text = "europe";
	} else {
		$shipping_qty_text = "worldwide";
	}

	if (count($order_products) == 1) {
		$key = 'shipping_alone_' . $shipping_qty_text . '_' . $env['lang'];
	} else {
		$key = 'shipping_with_other_product_' . $shipping_qty_text . '_' . $env['lang'];
	}

	foreach ($order_products as $order_product) {
		$shipping_price += $order_product['product'][$key];
	}

	$cache['cart_shipping_price'] = $shipping_price;

	return $shipping_price;
}

function cart_get_grandtotal_price($shipping_country) {
	return cart_get_total_price() + cart_get_shipping_price($shipping_country);
}

function cart_get_order_products() {
	global $env, $cache;

	if (isset($cache['cart_order_products'])){ 
		return $cache['cart_order_products'];
	}

	$order_products = array();

	for ($i = 0; $i < count(@$_SESSION['cart']['id']); $i++) {
		$product = db_find_product_by_id($_SESSION['cart']['id'][$i]);

		$order_products[] = array(
			'product' => $product,
			'qty' => $_POST['qtys'][$i],
			'price' => $product['product_price_' . $env['lang']],
			'currency' => get_currency_by_lang($env['lang']) 
		);
	}

	$cache['cart_order_products'] = $order_products;

	return $order_products;
}

function log_error($error_msg = '') {
	echo $error_msg;
}

function activate_error_state($state_msg = '') {
	global $env;

	$env['state'] = 'error';
	$env['state_msg'] = $state_msg;
}

function activate_complete_state($state_msg = '') {
	global $env;

	$env['state'] = 'complete';
	$env['state_msg'] = $state_msg;
}

function is_error_state() {
	global $env;

	return ($env['state'] == 'error' ? true : false);
}

function is_complete_state() {
	global $env;

	return ($env['state'] == 'complete' ? true : false);	
}

function get_state_message() {
	global $env;

	return (isset($env['state_msg'])? $env['state_msg'] : '');
}

function generate_unique_order_hash() {
	return substr(md5(openssl_random_pseudo_bytes(20)),-32);	
}

function generate_forgotten_password_hash() {
	return substr(md5(openssl_random_pseudo_bytes(20)),-32);
}

function generate_forgotten_password_link($email) {
	$hash = generate_forgotten_password_hash();
	db_insert_forgotten_password_request($email, $hash);

	$url = reset_password_url($hash);

	return $url;
}

function generate_account_validation_hash($email) {
	return substr(md5(openssl_random_pseudo_bytes(20)),-32);	
}

function generate_resend_validation_link($customer_email) {
	$hash = generate_account_validation_hash($customer_email);
	db_insert_account_validation_request($customer_email, $hash);

	$url = resent_validation_url($hash);

	return $url;
}

function send_account_validation_email($customer_email, $hash) {
	global $conf;

	$url = account_validation_url($hash);

	$subject = "Account Validation";
	$message = "Thank you for making an account at Agaf Design Studio! One more step - please validate your account.<br />
		Click on the link below:<br />
		<a href=\"$url\">$url</a> <br /><br />
	";

	require_once 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($customer_email);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	db_mark_account_validation_request_as_sent($hash);

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}
}

function send_order_complete_email_to_admin($orderid) {
	global $conf;

	$order = db_find_order_by_id($orderid);

	$subject = "Order Complete Notification";
	$message = "This email is sent to you as a notification that an order has been paid.<br />
		Here are the full order details:<br />
		<table>
			<tr>
				<td>ID</td>
				<td>{$order['id']}</td>
			</tr>
			<tr>
				<td>First Name</td>
				<td>{$order['first_name']}</td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td>{$order['last_name']}</td>
			</tr>
			<tr>
				<td>Address Primary</td>
				<td>{$order['address_primary']}</td>
			</tr>
			<tr>
				<td>Address Secondary</td>
				<td>{$order['address_secondary']}</td>
			</tr>
			<tr>
				<td>Country</td>
				<td>{$order['country']}</td>
			</tr>
			<tr>
				<td>City</td>
				<td>{$order['city']}</td>
			</tr>
			<tr>
				<td>City</td>
				<td>{$order['postcode']}</td>
			</tr>
			<tr>
				<td>Total Price</td>
				<td>{$order['total_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Shipping Price</td>
				<td>{$order['shipping_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Grand Total Price</td>
				<td>{$order['grand_total_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Lang</td>
				<td>{$order['lang']}</td>
			</tr>
		</table>

		<p>Invoice Details:</p>
		<p><strong>VAT #</strong>:{$order['vatnr']}</p>
		<p><strong>First Name</strong>:{$order['firstname_inv']}</p>
		<p><strong>Last Name</strong>:{$order['lastname_inv']}</p>
		<p><strong>Address 1</strong>:{$order['address1_inv']}</p>
		<p><strong>Address 2</strong>:{$order['address2_inv']}</p>
		<p><strong>Country</strong>:{$order['country_inv']}</p>
		<p><strong>Postcode</strong>:{$order['postcode_inv']}</p>
		<p><strong>City</strong>:{$order['city_inv']}</p>
	";

	require 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($conf['mail_from']);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}
}

function send_order_comlpete_email_to_client($orderid) {
	global $conf;

	$order = db_find_order_by_id_with_customer($orderid);

	$subject = "Order Complete Notification";
	$message = "This email is sent to you as a notification that your order with Agaf Design Studio has been paid.<br />
		Here are the full order details:<br />
		<table>
			<tr>
				<td>ID</td>
				<td>{$order['id']}</td>
			</tr>
			<tr>
				<td>First Name</td>
				<td>{$order['first_name']}</td>
			</tr>
			<tr>
				<td>Last Name</td>
				<td>{$order['last_name']}</td>
			</tr>
			<tr>
				<td>Address Primary</td>
				<td>{$order['address_primary']}</td>
			</tr>
			<tr>
				<td>Address Secondary</td>
				<td>{$order['address_secondary']}</td>
			</tr>
			<tr>
				<td>Country</td>
				<td>{$order['country']}</td>
			</tr>
			<tr>
				<td>City</td>
				<td>{$order['city']}</td>
			</tr>
			<tr>
				<td>City</td>
				<td>{$order['postcode']}</td>
			</tr>
			<tr>
				<td>Total Price</td>
				<td>{$order['total_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Shipping Price</td>
				<td>{$order['shipping_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Grand Total Price</td>
				<td>{$order['grand_total_price']} {$order['price_currency']}</td>
			</tr>
			<tr>
				<td>Lang</td>
				<td>{$order['lang']}</td>
			</tr>
		</table>

		<p>Invoice Details:</p>
		<p><strong>VAT #</strong>:{$order['vatnr']}</p>
		<p><strong>First Name</strong>:{$order['firstname_inv']}</p>
		<p><strong>Last Name</strong>:{$order['lastname_inv']}</p>
		<p><strong>Address 1</strong>:{$order['address1_inv']}</p>
		<p><strong>Address 2</strong>:{$order['address2_inv']}</p>
		<p><strong>Country</strong>:{$order['country_inv']}</p>
		<p><strong>Postcode</strong>:{$order['postcode_inv']}</p>
		<p><strong>City</strong>:{$order['city_inv']}</p>
	";

	require_once 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($order['customer_email']);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}
}

function render_order_products($orderid) {
	$html = '';
	$products = db_find_products_by_order_id($orderid);

	foreach ($products as $product) {
		$product_id = $product['productid'];
		$qty_product = $product['qty'];
		
		$html .= $qty_product." x ".$product['product_name_en']."<br />";
	}

	return $html;
}

function send_order_sent_email_notification_to_client($orderid) {
	global $conf;

	$order = db_find_order_by_id_with_customer($orderid);

	$subject = "Order Sent Notification";

	$message = "Hello,<br />
		This email is sent to you as a notification that your order with Agaf Design Studio has been sent.<br />

		<table>
			<tr>
				<th>Id</th>
				<th>Products</th>
			</tr>
			<tr>
				<td>" . $order['id'] . "</td>
				<td>" . render_order_products($orderid) . "</td>
			</tr>
		</table>

		<p>Shipping Info:</p>
		<p><strong>Courier</strong>:{$order['shipping_courier']}</p>
		<p><strong>Tracking No.</strong>:{$order['tracking_no']}</p>
	";

	require_once 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($order['customer_email']);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}
}


function send_order_payment_received_email_notification_to_client($orderid) {
	global $conf;

	$order = db_find_order_by_id_with_customer($orderid);

	$subject = "Order Payment Received Notification";

	$message = "Hello,<br />
		This email is sent to you as a notification that payment for your order with Agaf Design Studio has been received.<br />

		<table>
			<tr>
				<th>Id</th>
				<th>Products</th>
			</tr>
			<tr>
				<td>" . $order['id'] . "</td>
				<td>" . render_order_products($orderid) . "</td>
			</tr>
		</table>
	";

	require_once 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($order['customer_email']);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}	
}

function send_order_processed_email_notification_to_client($orderid) {
	global $conf;

	$order = db_find_order_by_id_with_customer($orderid);

	$subject = "Order Processed Notification";

	$message = "Hello,<br />
		This email is sent to you as a notification that your order with Agaf Design Studio has been processed and will be shipped shortly.<br />

		<table>
			<tr>
				<th>Id</th>
				<th>Products</th>
			</tr>
			<tr>
				<td>" . $order['id'] . "</td>
				<td>" . render_order_products($orderid) . "</td>
			</tr>
		</table>
	";

	require_once 'includes/PHPMailer-master/PHPMailerAutoload.php';

	$mail = new PHPMailer();
	$mail->setFrom($conf['mail_from']);
	$mail->addAddress($order['customer_email']);
	$mail->addReplyTo($conf['mail_from']);
	$mail->isHTML(true);
	$mail->Subject = $subject;
	$mail->Body = $message;

	$result = $mail->send();

	if (!$result) {
		throw new MailDeliveryFailedException("");
	}	
}

function dashboard_menu_html() {
	$html = '';

	$html .= '<ul class="dashboard-menu">
		<li><a href="' . dashboard_address_book_url() . '">Address Book</a></li>
		<li><a href="' . dashboard_order_history_url() . '">Order History</a></li>
		<li><a href="' . dashboard_change_password_url() . '">Change Password</a></li>
	</ul>';

	return $html;
}

function render_address_type($type) {
	if ($type == 'S') {
		return "shipping";
	} else if ($type == 'B') {
		return "billing";
	}
}

function handle_order_completed_event($orderid) {
	db_mark_order_as_complete($orderid);

	send_order_complete_email_to_admin($orderid);

	send_order_comlpete_email_to_client($orderid);
}

function render_address($address) {
	global $countries;

	return $address['first_name'] . ', ' . $address['last_name'] . ', ' . $address['address_line_1'] . ', ' . $address['postal_code'] . ', ' . $address['city'] . ', ' . $countries[$address['country']];
}

function render_countries_options($selected_country = '') {
	global $countries;

	$html = '';

	$html .= '<option value="">Country</option>';

	foreach ($countries as $key => $country) {
		$selected = '';

		if ($key == $selected_country) {
			$selected = "selected";
		}

		$html .= '<option value="' . $key . '" . ' . $selected . '>' . $country . '</option>';
	}

	return $html;
}

function lang_get_validation_error_message() {
	return "Invalid data. Please ensure the data you submit is correct and try again.";
}

function lang_get_cannotremovedefaultaddress_message() {
	return "This address cannot be removed. Please make another address default and then you can remove this one.";
}

function lang_get_unknownerror_message() {
	return "Unknown error occurred.";
}

function do_remove_customer_address($customer_id, $address_id) {
	if (db_is_default_address($customer_id, $address_id)) {
		throw new CannotRemoveDefaultAddressException();
	}

	db_remove_customer_address($customer_id, $address_id);
}

function header_code_conflict() {
	http_response_code(409);
}

function customer_is_default_billing_address($addressid) {
	$customer = get_auth_user();

	return ($addressid == $customer['default_billing_address_id']) ? true : false;
}

function customer_is_default_shipping_address($addressid) {
	$customer = get_auth_user();

	return ($addressid == $customer['default_shipping_address_id']) ? true : false;
}

function translate($key) {
	return lang_get_one($key);
}

function lang_get_one($key) {
	global $lang;

	if (isset($lang[$key])) {
		return $lang[$key];
	} else {
		db_insert_lang($key);
	}

	return $key;
}

function lang_get_all() {
	global $lang;

	return $lang;
}

function render_header_json() {
	$r = array();

	$r['lang'] = lang_get_all();

	return json_encode($r);
}

function create_address_data() {
	return array(
		'type' => '',
		'first_name' => '',
		'last_name' => '',
		'address_line_1' => '',
		'address_line_2' => '',
		'country' => '',
		'postcode' => '',
		'city' => ''
	);
}


function set_view_data($key, $value) {
	global $view;

	$view[$key] = $value;
}

function get_view_data($key, $default = null) {
	global $view;

	if (isset($view[$key])) {
		$value = $view[$key];
	} else {
		$value = $default;
	}

	return $value;
}

?>
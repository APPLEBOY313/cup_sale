<?php
require_once('config.php');

$title = "Agaf Design - Cart";
$meta_desc = "Agaf Design - Cart";

require_once('header.php');
?>
    <div class="container">
        <div class="row">
            <div class="cart-main-wrapper clearfix">
                <div id="cart-listings">
                    <div class="col-xs-12">
                        <div class="row">
                            <div class="cart-heading-wrapper clearfix">
                                <h1><?php echo $lang['shopping-cart']; ?></h1>
                            </div>
                        </div>
                    </div>
                    <?php

                    if (!empty($_SESSION['cart']))
                    { 
					?>
									
					<div id="cart-contents"  class="cart-listings-main-wrapper">
						<div class="cart-title-bar" style="float:left; width: 100%; border-bottom: 1px solid #ccc;">
							<div class="cart-img-col">&nbsp;</div>
							<div class="cart-item-col textcenter"><?php echo $lang['item-main']; ?></div>
							<div class="cart-qty-col textcenter"><?php echo $lang['quantity-main']; ?></div>
							<div class="cart-price-col textcenter"><?php echo $lang['price-main']; ?></div>
							<div class="cart-delete-col">&nbsp;</div>
						</div>
					<?php

                                        $cart_subtotal = 0;

                                        for ($i = 0; $i < count($_SESSION['cart']['id']); $i++) {

                                            $sql_product = "SELECT * FROM products WHERE productid={$_SESSION['cart']['id'][$i]}";
                                            $result_product = $conn->query($sql_product);
                                            $row_product = $result_product->fetch_assoc();
                                            $product_title = $row_product['product_name_' . $lng];
                                            $product_imgs_array = explode(",",
                                                $row_product['product_images_' . $lng]);
                                            $price_display = pricedisplay(($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]),
                                                $lng);

                                            $cart_subtotal = $cart_subtotal + ($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]);

                                            ?>

				
				
						<div class="single-product-info-wrapper" style="float: left; width: 100%;">
							<div class="cart-img-col">
								<div class="cart-img">
									<img
										src="images/product-images/<?php echo $product_imgs_array[0]; ?>" style="width: 100%; height: auto;" />
								</div>
							</div>
							
							<div class="cart-item-col">
								<div class="cart-item">
									<input type="hidden"
										   name="<?php echo $row_product['productid']; ?>pl_price"
										   value="<?php echo $row_product['product_price_pl']; ?>">
									<input type="hidden"
										   name="<?php echo $row_product['productid']; ?>_en_price"
										   value="<?php echo $row_product['product_price_en']; ?>">
									<a href="product.php?product=<?php echo $row_product['product_url_' . $lng]; ?>">
										<p><?php echo $product_title; ?></p>
									</a>
									<input type="hidden"
										   name="products[]"
										   value="<?php echo $row_product['productid']; ?>">
								</div>
							</div>
							
							<div class="cart-qty-col">
								<div class="cart-qty-price qty-small">
									<input
										class="width35 input-qty-val"
										name="qtys[]"
										type="number" min="1"
										id="product-<?php echo $row_product['productid']; ?>-qty"
										value="<?php echo $_SESSION['cart']['quantity'][$i]; ?>">
								</div>
							</div>
							
							<div class="cart-price-col">
								<div class="cart-qty-price">
									<span id="product-<?php echo $row_product['productid']; ?>-price"><?php echo $price_display; ?></span>
								</div>
							</div>
							
							<div class="cart-delete-col">
								<div class="cart-remove">
									<div class="delete-product-cart"
										id="<?php echo $row_product['productid']; ?>">
										x
									</div>
								</div>
							</div>
						</div>
										<?php } 
										
										$cart_subtotal_display = pricedisplay($cart_subtotal, $lng);
										
										
										
										?>
				
				
					</div>
				
					<div>
						<div class="subtotal-section-wrapper">
							<p> <?php echo $lang['subtotal-main']; ?> : <span
									id="subtotal-price"> <?php echo $cart_subtotal_display; ?></span>
							</p>
						</div>			
					
					</div>
				


                    <div class="clearing"></div>

                            <div class="checkout-line">
                                <a href="checkout.php">
                                    <div class="checkout-button">
                                        <?php echo $lang['continue']; ?>
                                    </div>
                                </a>

                                <div class="clearing"></div>
                            </div>

                </div>
            </div>
            <?php } else {

                if ($lng == 'pl') {
                    ?>
                    <div class="cart-no-item-wrapper clearfix"> Nie masz nic w koszyku. <a href="shop.php">Kontynuować zakupy</a>
                    </div>
                     <?php
                } else {
                    ?>
                    <div class="cart-no-item-wrapper clearfix"> You have nothing in your shopping cart. <a href="shop.php">Continue
                            Shopping</a></div>
                    <?php
                }

            } ?>
        </div>


    </div>


<?php
require_once('footer.php');
?>
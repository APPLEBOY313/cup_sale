<?php
require_once('config.php');


$title = "Agnieszka Fornalewska - Agaf Design";
$meta_desc = "Agnieszka Fornalewska - Agaf Design";

$banner_img="images/site-images/banner-about-img.jpg";

require_once('header.php');
?>
<div id="banner-line" class="banner-line-shop-all">
	<div id="banner-line-image" style="display: none; height: 375px; width: 100%;">
	</div>
</div>

<div class="contact-main-wrapper"> <!-- START OF FORM AND CONTACT -->
    <div class="contact-main-inner-wrapper">
        <div class="form-and-contact-main-wrapper">
            <div class="form-and-contact-inner-wrapper">
                <div class="container">
				
				<img src="images/site-images/agnieszka-fornalewska.jpg" alt="Agnieszka Fornalewska" style="float: left; width: 30%; margin: 120px 0px 60px 0;" />
				<div class="text-container" style="float: right; width: 65%; margin-top: 20px;">
				<h1 style="text-align: left;">Agnieszka Fornalewska - Agaf Design</h1>
				<?php if ($lng=="pl") { ?>
				<p style="margin-top: 40px">Z zawodu i zamiłowania jestem projektantką, a od 2013 roku właścicielką marki projektowej Agaf Design - Fun Loving Function.</p>
				
				<p>London Design School ukończyłam w 2000 roku. Nie przerywając kształcenia w tym kierunku, tym razem w Central Saint Martins College, rozpoczęłam równocześnie praktykę, w jednej z najbardziej prestiżowych firm w tej branży, Design House London.</p> 
				  
				<p>Przez kolejne 5 lat byłam częścią multidyscyplinarnego zespołu, i brałam udział <span class="narrow"></span>w kompleksowym projektowaniu usług, wnętrz i produktów kilku ogólnoświatowych marek. </p>

				<p>Pod koniec 2005 powróciłam do Polski. Tu kontynuowałam pracę jako projektantka wnętrz i obiektów wielofunkcyjnych, które je dopełniają. Nowatorskie, dobrze zaprojektowane przedmioty użytkowe, były zawsze ważnym elementem moich wnętrz. Nie było ich na rynku zbyt wiele, więc  postanowiłam to zmienić.</p>

				<p>Oficjalne otwarcie Studio Projektowe Ceramiki Użytkowej Agaf Design – Fun Loving Function datuję na 2013, chociaż pracę nad niektórymi formami rozpoczęłam jeszcze aranżując wnętrza.</p>

				<p>Resztę opowiedzą moje formy...</p>
				
				<?php } else { ?>
				
				<p style="margin-top: 40px">By profession and passion I am a designer, and from 2013 owner of the brand Agaf Design.</p> 
				
				<p>I studied in Britain at the London Design School and Central Saint Martins College before starting work in one of the most prestigious companies in the industry, Design House London.</p> 

				<p>Here I was part of a multidisciplinary team delivering comprehensive interior and product design solutions to national and global brands.</p> 
				
				<p>In 2005 I returned to Poland and established my own interior design practice working primarily with international clients. It was in this role that I discovered the challenges of sourcing certain functional, innovative, modern and contemporary products to complement my projects.</p> 
				
				<p>Determined not to compromise I resolved that if these pieces did not exist I would make them myself and so in 2013 I established Agaf Design Studio.</p> 
				
				<p>The rest of the story is told by my forms ...</p> 

				<?php } ?>
				</div>
                </div>
            </div>
        </div>
    </div>
</div> <!-- END OF FORM AND CONTACT -->

<img id="preload" src="<?php echo $banner_img; ?>" style="display:none;" />
<?php
require_once('footer.php');
?>

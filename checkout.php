<?php
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Content-Type: application/xml; charset=utf-8");


define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/functions.php');

global $conf, $lang, $env;

$title = "Area 16 Cart";
$meta_desc = "Area 16 - Cart Contents";
error_reporting(0);
$page_type = "noheaderimg";
$logo_page = 'none';

if(!empty($_SESSION['user_order_id'])){
	$order_id = $_SESSION['user_order_id'];

	/* $con=mysqli_connect("localhost","root", "" ,"timpurephp") or die("Connecting to MySQL failed"); */

	$query = "SELECT products.productid AS 'product_id', products.product_name_en AS 'product_name_en',
			  products.product_name_pl AS 'product_name_pl', products.product_url_en AS 'product_url_en',
			  products.product_url_pl AS 'product_url_pl', products.product_description_en AS 'product_description_en',
			  products.product_description_pl AS 'product_description_pl', products.product_price_en AS 'product_price_en',
			  products.product_price_pl AS 'product_price_pl', products.product_images_en AS 'product_images_en',
			  products.product_images_pl AS 'product_images_pl',  order_product.qty as 'qty' FROM products
			  INNER JOIN order_product ON products.productid = order_product.product_id
			  INNER JOIN orders ON order_product.order_id = orders.id
			  WHERE orders.id = $order_id";

	$data=mysqli_query($conn, $query);

	$order_query = "SELECT id, first_name, last_name, address_primary, address_secondary, country, postcode, city, customer_email, vatnr, company_name, firstname_inv, lastname_inv, address1_inv, address2_inv, country_inv, postcode_inv, city_inv FROM orders WHERE id = $order_id";

	$order_data=mysqli_query($conn, $order_query);
	
	$order = mysqli_fetch_assoc($order_data);
//    echo "<pre>"; print_r( $order  ); echo "</pre>" ; exit();
}

// Calculate shipping
$cart_content = $_SESSION['cart'];
if (!empty($order['country'])) {
	$country = $order['country'];
	}
	else
	{
	if ($lng=="en") {$country = "United Kingdom"; } else { $country = "Poland"; }		
	}
// print_r ($cart_content);

$shipping_cost = calculateshipping($country, $conn, $europe); 

if (is_authenticated()) {
	$auth = get_auth_user();

	if (!isset($order['first_name'])) {
		$order['first_name'] = $auth['first_name'];
	}

	if (!isset($order['last_name'])) {
		$order['last_name'] = $auth['last_name'];
	}
}

require_once('header.php');

?>

<script type="text/javascript">

	var login_state = "";
	 <?php 
		if(is_authenticated())
		{
			$login_state_php = "true";
		}
		else
		{
			$login_state_php = "false";
		}
 	?>
	login_state = <?php echo $login_state_php; ?>;

	var back = '';
	back = window.location.hash.substring(1);
	var checkout_page_flag = "true";
</script>

<div class="checkout-main-wrapper clearfix">
	<div class="container">
		<div class="row">
			<div id="checkout">
				<div class="checkout-heading-wrapper ">
					<div class="col-xs-12">
						<div class="row">
							<h1><?php echo $lang['checkout-page']; ?></h1>
						</div>
						<div class="row">
							<?php if (is_error_state()): ?>
								<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="customer-info-wrapper">
							<div class="col-xs-12">
								<div class="row">
									<div class="customer-info">
										<div class="col-xs-12">
											<div class="row">
												<div class="shipping-billing-heading-wrapper">
													<h2><?php echo $lang['shipping-billing']; ?></h2>
												</div>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="row">
												<div class="customer-info-form-wrapper">
													<form action="" method="post" id="review-form" name="checkout-submit-form" class="checkout-form">
														<input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
														<input type="hidden" name="lang" value="<?php echo $lng; ?>">
														<input type="hidden" name="checkout-submit" value="checkout-submit">

														<?php if (is_authenticated()): ?>
															<?php
																$shipping_addresses = db_find_customer_addresses(get_auth_user_id(), array('type' => 'S'));
															?>
														<?php if (count($shipping_addresses)): ?>
														<div class="form-field-holder-full-width">
															<label><input type="radio" name="custom_address" class="usecustom" value="0" <?php if (($_SESSION['custom_address_val'] == 0 ) || (!isset($_SESSION['custom_address_val']))){ echo "checked"; } ?> />Use an address from your address book</label>

															
															<div class="input-field-inner-wrapper clearfix">
																<div class="form-field form-field-holder-full-width">
																	<select name="shipping_address_id" class="text-input addresses-select shipping-addresses-select">
																		<?php foreach ($shipping_addresses as $address): ?>
																			<?php $is_default = ((db_is_default_address(get_auth_user_id(), $address['id'])) ? true : false); ?>
																			<?php $address_info = render_address($address); ?>
																			<?php if ($is_default) $address_info = '<strong>' . $address_info . '</strong>'; ?>
																		<option value="<?php echo $address['id']; ?>" data-firstname="<?php echo $address['first_name']; ?>" data-lastname="<?php echo $address['last_name']; ?>" data-city="<?php echo $address['city']; ?>" data-country="<?php echo $address['country']; ?>" data-addressline1="<?php echo $address['address_line_1']; ?>" data-addressline2="<?php echo $address['address_line_2']; ?>" data-postalcode="<?php echo $address['postal_code']; ?>" <?php if ($is_default): ?>select<?php endif; ?>><?php echo $address_info; ?></option>
																		<?php endforeach; ?>
																	</select>
																</div>
															</div>
															
															<br />
															<label><input type="radio" name="custom_address"  class="usecustom" value="1" <?php if ($_SESSION['custom_address_val'] == 1 ){ echo 'checked'; } ?> />or click here to enter a custom address</label>
															
															<div class="clearing"></div>
														</div>
														<?php endif; ?>

														<div class="form-field form-field-holder-full-width">
															<input name="save_custom_address" type="checkbox" class="checkbox save-custom-address-checkbox" value="1" /><span>Save this address in your address book</span>
														</div>
														<?php endif; ?>
	
														<div class="input-field-inner-wrapper clearfix">
															<input name="firstname" type="text" class="text-input disabled-on-check shipping_field shipping_firstname" id="checkout-fname" placeholder="<?php echo $lang['first-name']; ?>" value="<?php !empty($order['first_name']) ? print_r($order['first_name']) : '' ?>"/>
														</div>

														<div class="input-field-inner-wrapper clearfix">
															<input name="lastname" type="text" class="text-input disabled-on-check shipping_field shipping_lastname" id="checkout-lname" placeholder="<?php echo $lang['last-name']; ?>" value="<?php !empty($order['last_name']) ? print_r($order['last_name']) : '' ?>"/>
														</div>
														
														<div class="input-field-inner-wrapper clearfix">
															<input name="address1" type="text" class="text-input disabled-on-check shipping_field shipping_addressline1" id="checkout-addressone" placeholder="<?php echo $lang['adress-main']; ?>" value="<?php !empty($order['address_primary']) ? print_r($order['address_primary']) : '' ?>"/>
														</div>
														
														<div class="input-field-inner-wrapper clearfix">
															<input name="address2" type="text" class="text-input disabled-on-check shipping_field shipping_addressline2" id="checkout-addresstwo"
																   placeholder="<?php echo $lang['adress-main']; ?><?php if($lng!="pl") { ?>2<?php } ?>" value="<?php !empty($order['address_secondary']) ? print_r($order['address_secondary']) : '' ?>"/>
														</div>									

														<div class="input-field-inner-wrapper clearfix">
															<div class="form-field form-field-holder-full-width">
																<select name="country" class="text-input disabled-on-check shipping_country shipping_field" id="select-country"/>
																	<?php
																		echo render_countries_options($order['country']);
																	?>
																</select>
															</div>
														</div>

														<div class="input-field-inner-wrapper clearfix">
															<input name="postcode" type="text" class="text-input disabled-on-check shipping_field shipping_postcode" id="checkout-postcode"
																   placeholder="<?php echo $lang['postcode-main']; ?>" value="<?php !empty($order['postcode']) ? print_r($order['postcode']) : '' ?>"/>
														</div>

														<div class="input-field-inner-wrapper clearfix">
															<input name="city" type="text" class="text-input disabled-on-check shipping_field shipping_city" id="checkout-city"
																   placeholder="<?php echo $lang['town-main']; ?>" value="<?php !empty($order['city']) ? print_r($order['city']) : '' ?>"/>
														</div>

														<?php if (!is_authenticated()): ?>

														<div class="input-field-inner-wrapper clearfix">
															<input name="email" type="text" class="text-input" id="checkout-email" placeholder="E-Mail" value="<?php !empty($order['customer_email']) ? print_r($order['customer_email']) : '' ?>"/>
														</div>

														<div class="create-account-wrapper">
															<div class="col-xs-12">
																<div class="row">
																	<div class="form-field form-field-holder-full-width">
																		<label><input name="create_account" id="createAcccountCheckbox" type="checkbox" class="checkbox create-account-checkbox" value="1"><span>Create an account for this email</span></label>
																	</div>

																	<div class="account-info-wrapper">
																		<div class="col-xs-12">
																			<div class="row">
																				<div class="input-field-inner-wrapper clearfix">
																					<input name="pass" id="password" type="password" class="text-input" placeholder="Password" value="">
																				</div>
																				<div class="input-field-inner-wrapper clearfix">
																					<input name="passagain" type="password" class="text-input" placeholder="Password Again" value="">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>

															<div class="clearing"></div>
														</div>
														<?php endif; ?>

														<div class="invoice-outer-wrapper">
															<div class="form-field form-field-holder-full-width">
																<label><input name="inv_required" type="checkbox" value="inv_required" class="checkbox invoice-required-checkbox" <?php if (!empty($order['address1_inv'])) { echo ""; } ?>><span><?php echo $lang['invoice-req-main']; ?></span></label>
															</div>
															<!-- <div class="invoice-main-wrapper" < if (empty($order['address1_inv'])) { echo " display-on-off"; } ?>"> -->
															<div class="invoice-main-wrapper display-on-off">
																<div class="row">
																	<div class="col-xs-12">
																		<div class="invoice-inner-wrapper">
																			<!-- only display from here if invoice required -->
																			<div class="input-field-inner-wrapper clearfix">
																				<input name="vatnr" type="text"
																					   class="text-input"
																					   placeholder="<?php echo $lang['vat-num-main']; ?>" value="<?php !empty($order['vatnr']) ? print_r($order['vatnr']) : '' ?>"/>
																			</div>

																			<div class="input-field-inner-wrapper clearfix">
																				<input name="company_name" type="text"
																					   class="text-input"
																					   placeholder="<?php echo $lang['comp-name-main']; ?>" value="<?php !empty($order['company_name']) ? print_r($order['company_name']) : '' ?>"/>
																			</div>

																			<?php
																				if (is_authenticated()): ?>
																				<?php
																					$billing_addresses = db_find_customer_addresses(get_auth_user_id(), array('type' => 'B'));
																				?>
																				<?php if (count($billing_addresses)): ?>
																				<div class="form-field-holder-full-width">
																					<label><input type="radio" name="custom_billing_address" class="usecustombilling" value="0" checked />Select an address from your address book</label>
																					
																					<div class="form-field form-field-holder-full-width">
																						<select name="billing_address_id" class="text-input billing-addresses-select">
																							<?php foreach ($billing_addresses as $address): ?>
																								<?php $is_default = ((db_is_default_address(get_auth_user_id(), $address['id'])) ? true : false); ?>
																								<?php $address_info = render_address($address); ?>
																								<?php if ($is_default) $address_info = '<strong>' . $address_info . '</strong>'; ?>
																							<option value="<?php echo $address['id']; ?>" data-firstname="<?php echo $address['first_name']; ?>" data-lastname="<?php echo $address['last_name']; ?>" data-city="<?php echo $address['city']; ?>" data-country="<?php echo $address['country']; ?>" data-addressline1="<?php echo $address['address_line_1']; ?>" data-addressline2="<?php echo $address['address_line_2']; ?>" data-postalcode="<?php echo $address['postal_code']; ?>" <?php if ($is_default): ?>select<?php endif; ?>><?php echo $address_info; ?></option>
																							<?php endforeach; ?>
																						</select>
																					</div>
																					<br />
																					<label><input type="radio" name="custom_billing_address"  class="usecustombilling" value="1" />or click here to enter a custom address</label>
																					
																					<div class="clearing"></div>
																				</div>
																				<?php endif; ?>

																				<div class="form-field form-field-holder-full-width save-custom-billing-address-wrapper">
																					<label><input name="save_custom_billing_address" type="checkbox" class="checkbox save-custom-billing-address-checkbox" value="1" /><span>Save this address in your address book</span></label>
																				</div>
																			<?php endif; ?>
						
																			<div class="input-field-inner-wrapper clearfix">
																				<input name="firstname_inv" type="text"
																					   class="text-input billing_field billing_firstname"
																					   placeholder="<?php echo $lang['first-name']; ?>" value="<?php !empty($order['firstname_inv']) ? print_r($order['firstname_inv']) : '' ?>"/>
																			</div>

																			<div class="input-field-inner-wrapper clearfix">
																				<input name="lastname_inv" type="text"
																					   class="text-input billing_field billing_lastname"
																					   placeholder="<?php echo $lang['last-name']; ?>" value="<?php !empty($order['lastname_inv']) ? print_r($order['lastname_inv']) : '' ?>"/>
																			</div>
		
																			<div class="input-field-inner-wrapper clearfix">
																				<input name="address1_inv" type="text"
																					   class="text-input billing_field"
																					   placeholder="<?php echo $lang['adress-main']; ?>" value="<?php !empty($order['address1_inv']) ? print_r($order['address1_inv']) : '' ?>"/>
																			</div>

																			<div class="input-field-inner-wrapper clearfix">
																				<input name="address2_inv" type="text"
																					   class="text-input billing_field"
																					   placeholder="<?php echo $lang['adress-main']; ?>" value="<?php !empty($order['address2_inv']) ? print_r($order['address2_inv']) : '' ?>"/>
																			</div>
																			

																			<div
																				class="form-field form-field-holder-full-width">
																				<select name="country_inv" class="text-input billing_field" id="country_inv"/>
																					<?php
																						echo render_countries_options($order['country_inv']);
																					?>
																				</select>
																			</div>
												
																			<div class="input-field-inner-wrapper clearfix">
																				<input name="postcode_inv" type="text"
																					   class="text-input billing_field"
																					   placeholder="<?php echo $lang['postcode-main']; ?>" value="<?php !empty($order['postcode_inv']) ? print_r($order['postcode_inv']) : '' ?>"/>
																			</div>

																			<div class="input-field-inner-wrapper clearfix">
																				<input name="city_inv" type="text"
																					   class="text-input billing_field"
																					   placeholder="<?php echo $lang['town-main']; ?>" value="<?php !empty($order['city_inv']) ? print_r($order['city_inv']) : '' ?>"/>
																			</div>
																			
																			<div class="clearing"></div>
																		</div>
																	</div>

																</div>
															</div>
														</div>														



												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							 <div class="clearing"></div>
						</div>
					</div>
				</div> <!--/ .col-md-6 -->
				<div class="col-md-6"> <!-- .col-md-6 -->
					<div class="row">
						<div class="order-summary-wrapper">
							<div class="order-summary">
								<div class="col-xs-12">
									<div class="row">
										<div class="order-summary-heading-wrapper">
											<h2><?php echo $lang['order-summary']; ?></h2>
										</div>
									</div>
								</div>
								<div class="cart-listings-main-wrapper clearfix">
									<div class="col-xs-12">
										<div class="row">
											<div class="cart-listings-inner-wrapper">
												<div id="cart-listings">
													<div class="col-xs-12 order-listings-header"> <!-- .product-info-wrapper -->
														<div class="row">
															<div class="product-info-wrapper clearfix">
																<div class="col-xs-6">
																	<div class="row">
																		<div class="cart-img-header">
																			<p><?php echo $lang['item-main']; ?></p>
																		</div>
																	</div>
																</div>
																<div class="col-xs-2">
																	<div class="row">
																		<div class="cart-qty-price-header checkout-quantity">
																			<p><?php echo $lang['quantity-main']; ?></p>
																		</div>
																	</div>
																</div>
																<div class="col-xs-3">
																	<div class="row">
																		<div class="cart-qty-price-header checkout-price">
																			<p><?php echo $lang['price-main']; ?></p>
																		</div>
																	</div>
																</div>
																<div class="col-xs-1">
																	<div class="row">

																	</div>
																</div>
															</div>
														</div>
													</div> <!-- /.product-info-wrapper -->

													<?php
													$cart_subtotal = 0;
													$total_items_in_cart = 0;

														for ($i = 0; $i < count(@$_SESSION['cart']['id']); $i++) {

															$sql_product = "SELECT * FROM products WHERE productid={$_SESSION['cart']['id'][$i]}";
															$result_product = $conn->query($sql_product);
															$row_product = $result_product->fetch_assoc();
//                                                        echo "<pre>"; print_r(  $row_product['productid'] ); echo "</pre>" ; exit();
															$product_title = $row_product['product_name_' . $lng];
															$product_imgs_array = explode(",",
																$row_product['product_images_' . $lng]);
															$price_display = pricedisplay(($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]),
																$lng);

															$cart_subtotal = $cart_subtotal + ($row_product['product_price_' . $lng] * $_SESSION['cart']['quantity'][$i]);
															
															$total_items_in_cart = $total_items_in_cart + $_SESSION['cart']['quantity'][$i];

															?>
															<div class="col-xs-12">
																<div class="row">
																	<div class="single-product-info-wrapper clearfix">

																		<div> <!-- glavni div wrapper produkta -->
																			<div class="col-xs-6">
																				<!-- .product-img-name-wrapper -->
																				<div class="row">
																					<div
																						class="product-img-name-wrapper clearfix">
																						<div class="col-xs-3">

																									<img
																										class="width100"
																										src="images/product-images/<?php echo $product_imgs_array[0]; ?>"/>

																						</div>
																						<div class="col-xs-9">
																							<div class="row">
																								<div class="cart-item">
																									<input type="hidden"
																										   name="<?php echo $row_product['productid']; ?>_pl_price"
																										   value="<?php echo $row_product['product_price_pl']; ?>">
																									<input type="hidden"
																										   name="<?php echo $row_product['productid']; ?>_en_price"
																										   value="<?php echo $row_product['product_price_en']; ?>">
																									<a href="product.php?product=<?php echo $row_product['product_url_' . $lng]; ?>">
																										<p><?php echo $product_title; ?></p>
																									</a>
																									<input type="hidden"
																										   name="products[]"
																										   value="<?php echo $row_product['productid']; ?>">
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div> <!-- /.product-img-name-wrapper -->
																			<div class="col-xs-6">
																				<div class="row">
																					<div
																						class="product-qnty-prc-wrapper clearfix">
																						<div class="col-sm-5">
																							<div class="row">
																								<div
																									class="cart-qty-price qty-small">
																									<input
																										class="width35 input-qty-val"
																										name="qtys[]"
																										type="number" min="1"
																										id="product-<?php echo $row_product['productid']; ?>-qty"
																										value="<?php echo $_SESSION['cart']['quantity'][$i]; ?>">
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-5">
																							<div class="row">
																								<div
																									class="cart-qty-price">
																									<p id="product-<?php echo $row_product['productid']; ?>-price"><?php echo $price_display; ?></p>
																								</div>
																							</div>
																						</div>
																						<div class="col-sm-2">
																							<div class="row">
																								<div
																									class="cart-remove">
																									<div
																										class="delete-product-cart delete-mobile"
																										id="<?php echo $row_product['productid']; ?>">
																										x
																									</div>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>

																		</div>

																	</div>
																</div>
															</div>


															<?php
														}

													$cart_subtotal_display = pricedisplay($cart_subtotal, $lng);

													if(!empty($_SESSION['shipping_price_en']) || !empty($_SESSION['shipping_price_pl'])){
														$shipping_display = pricedisplay($_SESSION['shipping_price_' . $lng] ,
														$lng);
														$cart_total_display = pricedisplay($cart_subtotal + $_SESSION['shipping_price_' . $lng], $lng);

													} else {
														$shipping_display = pricedisplay($shipping_cost , $lng);
														$cart_total_display = $cart_subtotal_display;
													}

													?>
													<div class="col-xs-12">
														<input type="hidden" name="lng" value="<?php echo $lng; ?>">
														<div class="row">
															<div class="subtotal-section-wrapper">
																<p> <?php echo $lang['subtotal-main']; ?> : <span id="subtotal-price"> <?php echo $cart_subtotal_display; ?></span></p>
															</div>
															<div class="subtotal-section-wrapper">
																<p> <?php echo $lang['shipping']; ?>: <span id="shipping-price"> <?php echo $shipping_display; ?> </span></p>
															</div>
															<input type="hidden" name="shipping_price">
															<div class="subtotal-section-wrapper">
																<p> <?php echo $lang['total']; ?>: <span id="total-price"><?php echo $cart_total_display; ?></span></p>
															</div>
															<input type="hidden" name="total_price">
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="clearing"></div>
						</div>
					</div>
				</div> <!--/ .col-md-6 -->
					<div class="checkout-line">
						<!-- <a href="reviewandpay.php"> -->
							<button action="" class="checkout-button" id="review-submit" type="submit" name="checkout-submit">
								<?php echo $lang['review-pay-but-main']; ?>
							</button>
							<!-- </a> -->

						<div class="clearing"></div>
					</div>
				</form>

				<div class="clearing"></div>
			</div>
		</div>
	</div>
</div>


<?php
require_once('footer.php');
?>


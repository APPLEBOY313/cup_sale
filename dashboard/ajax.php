<?php 

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/../functions.php');

global $env;

$action = (isset($_REQUEST['action'])) ? $_REQUEST['action'] : 0;
$output = array();

if ($action == 'remove_address') {
	try {
		$customerid = get_auth_user_id();
		$addressid = (int) ((isset($_POST['addressid'])) ? $_POST['addressid'] : 0);

		if (!$addressid) {
			throw new DataNotValidException("");
		}

		do_remove_customer_address($customerid, $addressid);

	} catch (CannotRemoveDefaultAddressException $ex) {
		header_code_conflict();

		activate_error_state(lang_get_cannotremovedefaultaddress_message());
	} catch (DataNotValidException $ex) {
		activate_error_state("addressid not valid");
	} catch (Exception $ex) {
		activate_error_state(lang_get_unknownerror_message());
	}
}

if ($action == 'makedefault_address') {
	try {
		$customerid = get_auth_user_id();
		$addressid = (int) ((isset($_POST['address_id'])) ? $_POST['address_id'] : 0);

		db_set_customer_default_address($customer_id, $address_id);

	} catch (Exception $ex) {
		activate_error_state(lang_get_unknownerror_message());
	}
}

$ajax_output = array(
	'env' => array(
		'state' => $env['state'],
		'state_msg' => $env['state_msg'] 
	),
	'output' => $output
);

header("Content-Type: application/json");

echo json_encode($ajax_output);

exit;

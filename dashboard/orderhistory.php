<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

$auth_user = get_auth_user();

$orders = db_find_orders_by_customer_id($auth_user['id']);

require_once(PROJECT_ROOT . '/header.php');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="sidebar col-md-3">
						<?php echo dashboard_menu_html(); ?>
					</div>
					<div class="dashboard-inner col-md-9">
						<h3>Order History</h3>

						<table class="table">
							<tr>
								<th>Order ID</th>
								<th>Products</th>
								<th>Status</th>
							</tr>

							<?php foreach ($orders as $order): ?>
							<tr>
								<td><?php echo $order['id']; ?></td>
								<td><?php echo $order['id']; ?></td>
								<td><?php echo $order['status']; ?></td>
							</tr>
							<?php endforeach; ?>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$customer = get_auth_user();

$billing_addresses = db_find_customer_addresses(get_auth_user_id(), array('type' => 'B'));
$shipping_addresses = db_find_customer_addresses(get_auth_user_id(), array('type' => 'S'));

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

require_once(PROJECT_ROOT . '/header.php');
?>
<div class="modal fade remove-address-confirmation-modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                Remove Address
            </div>
            <div class="modal-body">
                Are you sure you want to remove this address?
				
				<div class="alert alert-info"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-cancel" data-dismiss="modal">Cancel</button>
                <a class="btn btn-danger btn-ok btn-submit">Remove</a>
            </div>
        </div>
    </div>
</div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="sidebar col-md-3">
						<?php echo dashboard_menu_html(); ?>
					</div>
					<div class="dashboard-inner col-md-9">
						<h3>Address Book</h3>
						<a href="<?php echo dashboard_add_address_url(); ?>" class="add-address-button">Add Address</a>

						<h4>Billing Addresses</h4>
						<ul class="addresses">
							<?php if (count($billing_addresses)): ?>
								<?php foreach ($billing_addresses as $address): ?>
								<li>
									<div class="address-entry">
										<span class="info">
											<span class="address-bit"><span class="first_name"><?php echo $address['first_name']; ?></span> <span class="last_name"><?php echo $address['last_name']; ?></span></span>
											<span class="address-bit"><span class="city"><?php echo $address['city']; ?></span></span>
											<span class="address-bit"><span class="address-line1"><?php echo $address['address_line_1']; ?></span></span>
											<span class="address-bit"><span class="address-line2"><?php echo $address['address_line_2']; ?></span></span>
											<span class="address-bit"><span class="pcode"><?php echo $address['postal_code']; ?></span></span>
											<span class="address-bit"><span class="country"><?php echo $address['country']; ?></span></span>
										</span>
										<span class="actions">
											<a href="<?php echo dashboard_addressbook_editaddress_url($address['id']); ?>">Edit Address</a>
											<?php if (!customer_is_default_billing_address($address['id'])): ?>	| 
												<a href="<?php echo dashboard_addressbook_removeaddress_url($address['id']); ?>" class="remove-address-link" data-addressid="<?php echo $address['id']; ?>">Remove Address</a>
											<?php endif; ?>
										</span>

										<?php if (!customer_is_default_billing_address($address['id'])): ?>
										<a href="<?php echo dashboard_addressbook_makedefaultaddress_url($address['id']); ?>" class="make-default" data-addressid="<?php echo $address['id']; ?>">Make Default</a>
										<?php else: ?>
										<span class="default-address">Default Address</span>
										<?php endif; ?>
									</div>
								</li>
								<?php endforeach; ?>
							<?php else: ?>
								<div class="alert alert-info">There are no billing addresses yet! Click on the button above to add one.</div>
							<?php endif; ?>
						</ul>
						<h4>Shipping Addresses</h4>
						<ul class="addresses">
							<?php if (count($shipping_addresses)): ?>
								<?php foreach ($shipping_addresses as $address): ?>
								<li>
									<div class="address-entry">
										<span class="info">
											<span class="address-bit"><span class="first_name"><?php echo $address['first_name']; ?></span> <span class="last_name"><?php echo $address['last_name']; ?></span></span>
											<span class="address-bit"><span class="city"><?php echo $address['city']; ?></span></span>
											<span class="address-bit"><span class="address-line1"><?php echo $address['address_line_1']; ?></span></span>
											<span class="address-bit"><span class="address-line2"><?php echo $address['address_line_2']; ?></span></span>
											<span class="address-bit"><span class="pcode"><?php echo $address['postal_code']; ?></span></span>
											<span class="address-bit"><span class="country"><?php echo $address['country']; ?></span></span>
										</span>
										<span class="actions">
											<a href="<?php echo dashboard_addressbook_editaddress_url($address['id']); ?>">Edit Address</a> 
											<?php if (!customer_is_default_shipping_address($address['id'])): ?> |
												<a href="<?php echo dashboard_addressbook_removeaddress_url($address['id']); ?>" class="remove-address-link" data-addressid="<?php echo $address['id']; ?>">Remove Address</a>
											<?php endif; ?>
										</span>

										<?php if (!customer_is_default_shipping_address($address['id'])): ?>
										<a href="<?php echo dashboard_addressbook_makedefaultaddress_url($address['id']); ?>" class="make-default" data-addressid="<?php echo $address['id']; ?>">Make Default</a>
										<?php else: ?>
										<span class="default-address">Default Address</span>
										<?php endif; ?>
									</div>
								</li>
								<?php endforeach; ?>
							<?php else: ?>
								<div class="alert alert-info">There are no shipping addresses yet! Click on the button above to add one.</div>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
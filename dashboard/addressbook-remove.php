<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

$address_id = (isset($_GET['id'])) ? $_GET['id'] : 0;

if (!$address_id) {
	redirect_to_dashboard_index();
}

require_once(PROJECT_ROOT . '/header.php');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="sidebar col-md-3">
						<?php echo dashboard_menu_html(); ?>
					</div>
					<div class="dashboard-inner col-md-9">
						<h3>Address Book</h3>
						<h4>Remove Address</h4>

						<?php if (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php else: ?>
							<?php if (is_error_state()): ?>
								<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
							<?php endif; ?>
							<div class="alert">Are you sure you want to remove this address?</div>
							<form action="" method="post" name="removeaddress-form">
								<input type="hidden" name="address_id" value="<?php echo $address_id; ?>" />
								<button class="checkout-button" id="removeaddress-submit" type="submit" name="removeaddress_submit">Remove Address</button>
							</form>
						<?php endif; ?>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
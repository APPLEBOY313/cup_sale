<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

require_once(PROJECT_ROOT . '/header.php');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="sidebar col-md-3">
						<?php echo dashboard_menu_html(); ?>
					</div>
					<div class="dashboard-inner col-md-9">
						<h3>Change Password</h3>

						<?php if (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php else: ?>
							<?php if (is_error_state()): ?>
								<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
							<?php endif; ?>
							<form action="" method="post" name="changepass-form" class="changepass-form">
								<div class="form-field form-field-holder-full-width">
									<input name="oldpass" id="oldpass" type="password" class="text-input disabled-on-check" placeholder="Old Password" value="">
								</div>

								<div class="form-field form-field-holder-full-width">
									<input name="pass" type="password" class="text-input disabled-on-check" placeholder="Password" value="">
								</div>

								<button class="submit-button changepass-button" id="changepass-submit" type="submit" name="changepass_submit" value="submit">Change Password</button>
							</form>
						<?php endif; ?>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
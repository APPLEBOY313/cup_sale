<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

$address = get_view_data('address', create_address_data());

require_once(PROJECT_ROOT . '/header.php');
?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="sidebar col-md-3">
						<?php echo dashboard_menu_html(); ?>
					</div>
					<div class="dashboard-inner col-md-9">
						<h3>Address Book</h3>
						<h4>Add Address</h4>
	
						<?php if (is_complete_state()): ?>
							<div class="alert alert-success"><?php echo get_state_message(); ?></div>
						<?php else: ?>
							<?php if (is_error_state()): ?>
								<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
							<?php endif; ?>
							<form action="" method="post" name="addaddress-form" class="dashboard-form addaddress-form">
								<div class="form-inner-wrapper clearfix">
									<!-- Name -->
									<div class="row">
										<div class="col-md-12">
											<div class="input-field-inner-wrapper clearfix">
												<select name="type" type="text" id="addaddress-type">
													<option value="B">Billing</option>
													<option value="S">Shipping</option>
												</select>
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('name-main'); ?> <span>*</span></p>
												<div class="row">
													<div class="col-md-6">
														<input name="first_name" type="text" class="text-input" id="addaddress-firstname" placeholder="<?php echo translate('first-name'); ?>" value="<?php echo $address['first_name']; ?>">
														<label class=""><?php echo translate('first-name'); ?></label>
													</div>
													<div class="col-md-6">
														<input name="last_name" type="text" class="text-input" id="addaddress-lastname" placeholder="<?php echo translate('last-name'); ?>" value="<?php echo $address['last_name']; ?>">
														<label class=""><?php echo translate('last-name'); ?></label>
													</div>
												</div>
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('address-line1'); ?> <span>*</span></p>
												<input name="address_line_1" type="text" class="text-input" id="addaddress-addressline1" placeholder="<?php echo translate('address-line1'); ?>" value="<?php echo $address['address_line_1']; ?>">
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('address-line2'); ?> <span>*</span></p>
												<input name="address_line_2" type="text" class="text-input" placeholder="<?php echo translate('address-line2'); ?>" value="<?php echo $address['address_line_2']; ?>">
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('country'); ?> <span>*</span></p>

												<select name="country" class="text-input">
													<?php
														echo render_countries_options($address['country']);
													?>
												</select>
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('postcode'); ?> <span>*</span></p>

												<input name="postcode" type="text" class="text-input disabled-on-check" id="addaddress-postcode" placeholder="<?php echo translate('postcode'); ?>" value="<?php echo $address['postcode']; ?>">
											</div>

											<div class="input-field-inner-wrapper clearfix">
												<p class="field-title"><?php echo translate('town_city'); ?> <span>*</span></p>

												<input name="city" type="text" class="text-input disabled-on-check" id="addaddress-city" placeholder="<?php echo translate('town_city'); ?>" value="<?php echo $address['city']; ?>">
											</div>

											<button class="checkout-button" id="addaddress-submit" type="submit" name="addaddress_submit"><?php echo translate('add_address'); ?></button>
										</div>
									</div>
								</div>
							</form>
						<?php endif; ?>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
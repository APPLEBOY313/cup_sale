<?php

define("PROJECT_ROOT", realpath("../"));

require_once(PROJECT_ROOT . '/functions.php');

$title = "Agaf Design - Dashboard";
$meta_desc = "Agaf Design - Dashboard";

require_once(PROJECT_ROOT . '/header.php');

?>
    <div class="container">
        <div class="row">
            <div class="col-md-12 dashboard">
				<div class="title-wrapper">
					<h2>Dashboard</h2>
				</div>
				<div class="row dashboard-wrapper">
					<div class="dashboard-inner col-md-4 col-md-offset-4">
						<h3>Login</h3>

						<?php if (is_error_state()): ?>
							<div class="alert alert-danger"><?php echo get_state_message(); ?></div>
						<?php endif; ?>
						<form action="" method="post" name="login-form" class="login-form">
							<div class="form-field form-field-holder-full-width">
								<input name="username" type="text" class="text-input disabled-on-check" placeholder="E-mail" value="">
							</div>

							<div class="form-field form-field-holder-full-width">
								<input name="password" type="password" class="text-input disabled-on-check" placeholder="Password" value="">
							</div>

							<button class="login-button" id="login_submit" type="submit" name="login_submit" value="submit">Login</button>
						</form>
						
						<a href="<?php echo forgotten_password_url(); ?>">Forgotten Password</a>
					</div>
				</div>
			</div>
        </div>
    </div>


<?php
require_once(PROJECT_ROOT . '/footer.php');
?>
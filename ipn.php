<?php

define("PROJECT_ROOT", realpath("./"));

require_once(PROJECT_ROOT . '/includes/paypal-ipn.php');

require_once(PROJECT_ROOT . '/functions.php');

file_put_contents("test.txt", print_r($_POST));

try {
	$ipn = new PaypalIPN();

	if ($conf['paypal_is_sandbox']) {
		// Use the sandbox endpoint during testing.
		$ipn->useSandbox();
	}

	$verified = $ipn->verifyIPN();

	if ($verified) {
		var_dump('test');
		exit;
		
		$order_hash = $_POST['invoice'];

		$order = db_find_order_by_hash($order_hash);

		if ($order) {
			if ($order['grand_total_price'] == $_POST['mc_gross']) {
				handle_order_completed_event($order['id']);
			}
		}

		/*
		 * Process IPN
		 * A list of variables is available here:
		 * https://developer.paypal.com/webapps/developer/docs/classic/ipn/integration-guide/IPNandPDTVariables/
		 */
	}
	// Reply with an empty 200 response to indicate to paypal the IPN was received correctly.
	header("HTTP/1.1 200 OK");
} catch (Exception $ex) {
	header("HTTP/1.1 400 Bad Params");
}
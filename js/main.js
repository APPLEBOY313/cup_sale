$(document).ready(function ($) {

    if(window.location.href.indexOf("reviewandpay.php") > -1)
    {
        $('#pay_paypal :input').prop("disabled", true);
        $('.delete-product-cart').removeClass('delete-product-cart');
        $('.input-qty-val').attr("disabled", true);
    }
    initalize();
	$('.remove-address-link').click(function(ev) {
		ev.preventDefault();

		var $link = $(this);

		$('.remove-address-confirmation-modal').find('.btn-submit').on('click', function(ev) {
			ev.preventDefault();

			var addressid = $link.attr('data-addressid');

			$.ajax({
				url: '../ajax.php',
				data: {
					format: 'json',
					'action': 'remove_address',
					'addressid': addressid
				},
				fail: function() {
					$('.remove-address-confirmation-modal').find('.alert').html('Cannot remove a default address.');
				},
				success: function(data) {
					$('.remove-address-confirmation-modal').modal('hide');

					$link.parents('.address-entry').remove();
				},
				type: 'POST'
			});
		});

		$('.remove-address-confirmation-modal').modal('show');
	});

	$('.remove-address-confirmation-modal').on('hidden.bs.modal', function() {		
		$('.remove-address-confirmation-modal').find('.btn-submit').off('click');
	});

    $('#contact-submit-pl').on('click', function(e){
        // e.preventDefault();
        $('div .field-error').remove();
        var error = false;

        var fname = $('#contact-fname');
        var lname = $('#contact-lname');
        var email = $('#contact-email');
        var message = $('#contact-message');
        if(fname.val().length == 0 && lname.val().length == 0 ){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Pole 'Nazwa' nie zostało wypełnione.</div>");
            fname.addClass('has-error-border');
            lname.addClass('has-error-border');
            $('.contact-full-name').addClass('has-error-validation');
            $('.first-name-custom').find('label').addClass('has-error-validation');
            $('.last-name-custom').find('label').addClass('has-error-validation');

        } else if(fname.val().length == 0 && lname.val().length > 0){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Wpisz Imię.</div>");
            error = true;
            fname.addClass('has-error-border');
            $('.first-name-custom').find('label').addClass('has-error-validation');
            $('.contact-full-name').addClass('has-error-validation');

            lname.removeClass('has-error-border');
            $('.last-name-custom').find('label').removeClass('has-error-validation');
        }  else if( lname.val().length == 0 && fname.val().length > 0 ){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Wpisz Nazwisko.</div>");
            error = true;

            lname.addClass('has-error-border');
            $('.contact-full-name').addClass('has-error-validation');
            $('.last-name-custom').find('label').addClass('has-error-validation');

            fname.removeClass('has-error-border');
            $('.first-name-custom').find('label').removeClass('has-error-validation');
        } else {
            $('.contact-full-name').removeClass('has-error-validation');

            fname.removeClass('has-error-border');
            $('.first-name-custom').find('label').removeClass('has-error-validation');

            lname.removeClass('has-error-border');
            $('.last-name-custom').find('label').removeClass('has-error-validation');
        }

        if(email.val().length == 0){
            email.addClass('has-error-border');
            error = true;
            $(email.parent()).prepend("<div class='field-error'>Wpisz swój adres mailowy.</div>");
            $(email.parent()).find('p').addClass('has-error-validation');
        } else if (!validateEmail(email.val())) {
            email.addClass('has-error-border');
            error = true;
            $(email.parent()).prepend("<div class='field-error'>Wpisany adres mailowy jest niepoprawny. Sprawdż i wpisz poprawny adres mailowy.</div>");
            $(email.parent()).find('p').addClass('has-error-validation');
        } else {
            email.removeClass('has-error-border');
            $(email.parent()).find('p').removeClass('has-error-validation');
        }

        if(message.val().length == 0 ){
            error = true;
            message.addClass('has-error-border');
            $(message.parent()).prepend("<div class='field-error'>Wpisz wiadomość.</div>");
            $(message.parent()).find('p').addClass('has-error-validation');
        } else {
            message.removeClass('has-error-border');
            $(message.parent()).find('p').removeClass('has-error-validation');
        }

        if(!error){
            $('#submit-contact').submit();
        } else {
            $(".form-wrapper").append("<div class='field-error'>Twoja wiadomość nie została wysłana. Sprawdż proszę czy wszystkie wymagane pola zostały wypełnione.</div>").prepend("<div class='field-error'>Twoja wiadomość nie została wysłana. Sprawdż proszę czy wszystkie wymagane pola zostały wypełnione.</div>");
			return false;
        }


    });
	
	// PL contact form

   $('#contact-submit-en').on('click', function(e){
        // e.preventDefault();
        $('div .field-error').remove();
        var error = false;

        var fname = $('#contact-fname');
        var lname = $('#contact-lname');
        var email = $('#contact-email');
        var message = $('#contact-message');
        if(fname.val().length == 0 && lname.val().length == 0 ){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Your name is required.</div>");
            fname.addClass('has-error-border');
            lname.addClass('has-error-border');
            $('.contact-full-name').addClass('has-error-validation');
            $('.first-name-custom').find('label').addClass('has-error-validation');
            $('.last-name-custom').find('label').addClass('has-error-validation');

        } else if(fname.val().length == 0 && lname.val().length > 0){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Your first name is required</div>");
            error = true;
            fname.addClass('has-error-border');
            $('.first-name-custom').find('label').addClass('has-error-validation');
            $('.contact-full-name').addClass('has-error-validation');

            lname.removeClass('has-error-border');
            $('.last-name-custom').find('label').removeClass('has-error-validation');
        }  else if( lname.val().length == 0 && fname.val().length > 0 ){
            $(fname.parents().eq(2)).prepend("<div class='field-error'>Last name is required.</div>");
            error = true;

            lname.addClass('has-error-border');
            $('.contact-full-name').addClass('has-error-validation');
            $('.last-name-custom').find('label').addClass('has-error-validation');

            fname.removeClass('has-error-border');
            $('.first-name-custom').find('label').removeClass('has-error-validation');
        } else {
            $('.contact-full-name').removeClass('has-error-validation');

            fname.removeClass('has-error-border');
            $('.first-name-custom').find('label').removeClass('has-error-validation');

            lname.removeClass('has-error-border');
            $('.last-name-custom').find('label').removeClass('has-error-validation');
        }

        if(email.val().length == 0){
            email.addClass('has-error-border');
            error = true;
            $(email.parent()).prepend("<div class='field-error'>Email address is required.</div>");
            $(email.parent()).find('p').addClass('has-error-validation');
        } else if (!validateEmail(email.val())) {
            email.addClass('has-error-border');
            error = true;
            $(email.parent()).prepend("<div class='field-error'>Email address is not valid. Email addresses should follow the format user@domain.com.</div>");
            $(email.parent()).find('p').addClass('has-error-validation');
        } else {
            email.removeClass('has-error-border');
            $(email.parent()).find('p').removeClass('has-error-validation');
        }

        if(message.val().length == 0 ){
            error = true;
            message.addClass('has-error-border');
            $(message.parent()).prepend("<div class='field-error'>Message is required.</div>");
            $(message.parent()).find('p').addClass('has-error-validation');
        } else {
            message.removeClass('has-error-border');
            $(message.parent()).find('p').removeClass('has-error-validation');
        }

        if(!error){
            $('#submit-contact').submit();
        } else {
            $(".form-wrapper").append("<div class='field-error'>Your form has encountered a problem. Please scroll up to review.</div>").prepend("<div class='field-error'>Your form has encountered a problem. Please scroll down to review.</div>");
			return false;
        }

    });

	$.validator.addMethod("pwcheck", function (value) {
		var valid = true;

		if (value.length < 8) {
			valid = false;
		} else if (!/[\@\#\$\%\^\&\*\(\)\_\+\!]/.test(value) || !/[a-zA-Z]/.test(value) || !/[0-9]/.test(value)) {
			valid = false;
		}

		return valid;
	});

	function translate(key) {
		if (typeof conf.lang[key] == 'undefined') {
			return key;
		}

		return conf.lang[key];
	}

	function is_valid_email(email) {
		return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
		
	}
email_enabel = 'false';
	$.validator.addMethod('agfemail', function (value) {
		if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value))
		{
			email_enabel = 'true';
			return true;
		}
		else
		{
			email_enabel = 'false';			
			return false;
		}
	}, 'Please enter valid email address.');

	$.validator.addMethod("uniqueemail", function(value, element) {
			// if (!is_valid_email(value)) {
			// 	return false;
			// }
			res='';
			$.ajax({
				type: "POST",
				url: "ajax.php",
				format: "json",
				async: false,
				data: {
					action: 'check_email_unique',
					email: value
				},
				dataType:"json",
				success: function(resp)
				{
					if (resp.output.unique) {
						res = 'false';
					} else {
						res = 'true';
					}
				}
			});
			if(res === 'true')
			{
				email_enabel = 'true'
				$(".create-account-wrapper").show();
				return true;
			}
			else
			{
				email_enabel = 'false'
				$(".create-account-wrapper").hide();
				return false;			
			}
		}, 
		"Email has already been taken. Please <a href=\"" + $('.login-link').attr('href') + "\">login</a>."
	);


	jQuery.extend(jQuery.validator.messages, {
		pwcheck: translate("validation_password_pwcheck")
	});

	var baseValidateConfig = {
		ignore: '.ignore',
		errorClass: "field-error",
		errorPlacement: function(error, element) {
			$(element).parents('.input-field-inner-wrapper').prepend(error);
		},
		highlight: function(element, errorClass) {
			$(element).removeClass(errorClass).addClass('has-error-border');
		},
		unhighlight: function(element, errorClass) {
			$(element).removeClass('has-error-border');
		},
		messages: {
			
		}
	};

	$('.signup-form').validate($.extend(baseValidateConfig, {
		groups: {
			nameGroup: "firstname lastname"
		},
		rules: {
			 // no quoting necessary
			 firstname: "required",
			 lastname: "required",
			 emailadress: {
				"required": true,
				"agfemail":true 
			 },
			 password: {
				pwcheck: true
			 },
			 passwordconfirm: {
				 equalTo: "#password"
			 }
		}
	}));

	$('.checkout-form').validate($.extend(baseValidateConfig, {
		rules: {
			 email: {
				"required": true,
				"agfemail":true,
				"uniqueemail":true
			 },
			 pass: {
				pwcheck: true
			 },
			 passagain: {
				equalTo: "#password"
			 }
		}
	}));

	$('#createAcccountCheckbox').on('change', function(event) {
		if ($(this).is(':checked')) {
			$('.account-info-wrapper').find('input, select').removeClass('ignore');
		} else {
			$('.account-info-wrapper').find('input, select').addClass('ignore');
		}
	});

	$('.login-form').validate($.extend(baseValidateConfig, {
		rules: {
			username: {
				"required": true,
				"agfemail":true 
			},
			password: {
				"required": true,
			}
		}
	}));

	$('.forgottenpass-form').validate($.extend(baseValidateConfig, {
		rules: {
			email: {
				"required": true,
				"agfemail":true 
			}
		}
	}));

	$('.changepass-form').validate($.extend(baseValidateConfig, {
		rules: {
			 pass: {
				"required": true,
				"minlength": 8,
				pwcheck: true
			 }
		}
	}));

	$('.addaddress-form').validate($.extend(baseValidateConfig, {
		groups: {
			nameGroup: "first_name last_name"
		},
		rules: {
			first_name: {
				"required": true
			},
			last_name: {
				"required": true
			},
			address_line_1: {
				"required": true
			},
			postcode: {
				"required": true
			},
			city: {
				"required": true
			}
		}
	}));

	$('.editaddress-form').validate($.extend(baseValidateConfig, {
		groups: {
			nameGroup: "first_name last_name"
		},
		rules: {
			first_name: {
				"required": true
			},
			last_name: {
				"required": true
			},
			address_line_1: {
				"required": true
			},
			postcode: {
				"required": true
			},
			city: {
				"required": true
			}
		}
	}));

    //products
    $("#product_to_cart").submit( function(e) {
		e.preventDefault();
        var formdata = $(this).serialize();
        var button_content = $(this).find('button[type=submit]'); //get clicked button info
        button_content.html('Adding...');
        var qty = $('input[name = "quantity"]').val();
       $.ajax({
                method: "POST",
                url: "functions.php",
                data: formdata
             })
            .done(function () {
                console.log('aaa');
                button_content.html('Add to cart');
                jQuery('#triger-featherlight').click();
                $('.number-holder').text( +parseInt($('.number-holder').text()) + +qty);
            })
            .fail(function () {
                console.log("error");
            })
            .always(function () {
                console.log("complete");
            });
        
    });

    //cart
    $('.delete-product-cart').on('click', function(e){
        e.preventDefault();

        var _this = this;
        var check_lang = $("input[name='lng']").val();
		var country = $('#select-country').find(":selected").text();
		
        console.log( );
        if(check_lang == "en") {
            var deleted_price = $('#product-'+_this.id+'-price').html().replace("z"+"\u0142", '');
            var subtotal_price = $('#subtotal-price').html().replace("z"+"\u0142", '');
        } else {
            var deleted_price = $('#product-'+_this.id+'-price').html().replace("\u20ac", '');
            var subtotal_price = $('#subtotal-price').html().replace("\u20ac", '');
        }
        var new_subtotal_price = parseInt(subtotal_price) - parseInt(deleted_price);

        $.ajax( {
          method: "get",
          url: "functions.php",
          data: {
                    'id' : this.id,
                    'delete-product' : $(this).attr("class"),
                    'qty' : $('#product-'+_this.id+'-qty').val(),
					'country' : country
                }
        } )
          .done(function(res) {
			  console.log(res);
              var ress = jQuery.parseJSON(res);

              var response = ress.num_of_products;
              var shippingcost = ress.new_shipping_cost;
			  var new_total_price = parseInt(new_subtotal_price) + parseInt(shippingcost);

              $('.number-holder').text(response);
              if(check_lang == "en") {
                  $('#subtotal-price').text(parseInt(new_subtotal_price)+" z"+"\u0142");
				  $('#shipping-price').text(parseInt(shippingcost)+" z"+"\u0142");
				  $('#total-price').text(parseInt(new_total_price)+" z"+"\u0142");
              } else{

                  $('#subtotal-price').text("\u20ac"+parseInt(new_subtotal_price));
                  $('#shipping-price').text("\u20ac"+parseInt(shippingcost));
				  $('#total-price').text("\u20ac"+parseInt(new_total_price));
              }

              if(response == 0){
                  location.reload();
              }
              $(_this).parents('div.single-product-info-wrapper').remove();


          })
          .fail(function(xhr, status, error) {
            console.log( "error" );
          })
          .always(function() {
            console.log( "complete" );
          });
    });

    $('.input-qty-val').on('change', function (e) {
        var _this = this;
        var qty = $(_this).val();
        var id = _this.id.replace ( /[^\d.]/g, '' ); 
        var product_price_pl = $("input[name = '" + id + "_pl_price']").val();
        var product_price_en = $("input[name = '" + id + "_en_price']").val(); 
        var product_old_qty = $("input[name = '" + id + "_old_qty']").val(); 
        var check_lang = $("input[name='lng']").val();
		var country = $('#select-country').find(":selected").text();
		var TotalItemsInCart = $('#total-items-in-cart').text();
		

        if(check_lang == "en") {
            var current_price = $('#product-'+id+'-price').html().replace("z"+"\u0142", '');
            var subtotal_price = $('#subtotal-price').html().replace("z"+"\u0142", '');

        } else {
            var current_price = $('#product-'+id+'-price').html().replace("\u20ac", '');
            var subtotal_price = $('#subtotal-price').html().replace("\u20ac", '');
        }

        subtotal_price = subtotal_price - current_price;

        if(qty == 1 || qty > 1){
        console.log('tu');
            var ajax = $.ajax( {
                    method: "get",
                    url: "functions.php",
                    data: {
                        "update-quantity" : "aaa",
                        "id" : id,
                        "qty" : qty,
						"oldqty": product_old_qty,
						"country" : country,
						"totalitemsincart" : TotalItemsInCart
                    }
                } )
                .done(function(res) {
					
					var ChangeQtyarr = res.split('|');
					
                    //if is exactly en_lang
                    $('.number-holder').text( parseInt(ChangeQtyarr[0]));
					$('#total-items-in-cart').text( parseInt(ChangeQtyarr[0]));
                    if(window.location.href.indexOf("checkout.php") > -1){
                        if(check_lang == "pl") { // English and shipping country set
                            var price = parseInt(product_price_en) * parseInt(qty);
                            subtotal_price = price + subtotal_price;

                            // var shipping_price = $('#shipping-price').html().replace("\u20ac", '');
                            var shipping_price = parseInt(ChangeQtyarr[1]); // alert(shipping_price);
                            var total =  +subtotal_price + +shipping_price;
                            console.log(total);
                            $('#total-price').text("\u20ac" + total);

							$('#shipping-price').text("\u20ac" + shipping_price);
                            $('#product-'+id+'-price').html("\u20ac"+ price);
                            $('#subtotal-price').html("\u20ac" +  subtotal_price  );
                        } else {
                            var price = parseInt(product_price_pl) * parseInt(qty);
                            subtotal_price = price + subtotal_price;
                            var shipping_price = parseInt(ChangeQtyarr[1]);
                            var total =  +subtotal_price + +shipping_price;

                            $('#total-price').html(total + " z"+"\u0142");
                            $('#shipping-price').html(shipping_price + " z"+"\u0142");
                            $('#product-'+id+'-price').html(price +" z"+"\u0142");
                            $('#subtotal-price').html( subtotal_price+ " z"+"\u0142");
                        }
                    } else {
                        if(check_lang == "pl") {
                            var price = parseInt(product_price_en) * parseInt(qty);
                            subtotal_price = price + subtotal_price;

                            $('#product-'+id+'-price').html("\u20ac"+ price);
                            $('#subtotal-price').html("\u20ac" +  subtotal_price  );
                        } else {
                            var price = parseInt(product_price_pl) * parseInt(qty);
                            subtotal_price = price + subtotal_price;
                            $('#product-'+id+'-price').html(price +" z"+"\u0142");
                            $('#subtotal-price').html( subtotal_price+ " z"+"\u0142");
                        }
                    }

                })
                .fail(function() {
                    console.log( "error" );
                })
                .always(function() {
                    console.log( "complete" );
                });
        }

    });




    /**
     * Checkout
     */
    $('#review-form').on('submit', function (e) {
    	// debugger;
		if(login_state)
		{
	    	var address_book_flag = $('.usecustom');
	    	if (address_book_flag[1].checked) 
	    	{
	    		submit_check(this,e);
	    	}
		}
		else
		{
			submit_check(this,e);
		}
    });

	function submit_check(self_this,form_this)
	{
	        var error = false;
	        var fname = $('#checkout-fname');
	        var lname = $('#checkout-lname');
	        var address_one = $('#checkout-addressone');
	        var post_code = $('#checkout-postcode');
	        var city = $('#checkout-city');
	        var country = $('#select-country');
	        var mail = $('#checkout-email').val();
	        var lng = $('input[name=lng]').val();
	        if(lng != 'en'){
	            $('input[name=shipping_price]').val($('#shipping-price').html().replace("\u20ac", ''));
	            $('input[name=total_price]').val($('#total-price').html().replace("\u20ac", ''));
	        } else {
	            $('input[name=shipping_price]').val($('#shipping-price').html().replace("z"+"\u0142", ''));
	            $('input[name=total_price]').val($('#total-price').html().replace("z"+"\u0142", ''));
	        }
	        var fname_str = fname.val();
	        if((fname.val() == "") || (fname_str.search(",") > 0)){
	            fname.addClass('has-error-border');
	            if(fname.val() == "")
	            {
		            $('#checkout-fname').parent().append("<p style='color:red;'>*This field is required.</p>");	            	
	            }
	            else
	            {
		            $('#checkout-fname').parent().append("<p style='color:red;'>*This field can't include comma.</p>");          	
	            }
	            error = true;
	        } else {
	            fname.removeClass('has-error-border');
	        }
	        if(lname.val() == "" ){
	            lname.addClass('has-error-border');
	            $('#checkout-lname').parent().append("<p style='color:red;'>*This field is required.</p>");
	            error = true;
	        } else {
	            lname.removeClass('has-error-border');
	        }
	  

	        var cnt_address = address_one.val();
			var words = cnt_address.split(" ");

	        if(words.length < 2 ){
	            address_one.addClass('has-error-border');
	            $('#checkout-addressone').parent().append("<p style='color:red;'>*The address should be more than 1 word.</p>");
	            error = true;
	        } else {
	            address_one.removeClass('has-error-border');
	        }

	        if(!validateZip(post_code.val())){
	            post_code.addClass('has-error-border');
	            $('#checkout-postcode').parent().append("<p style='color:red;'>*This is not a valid code.</p>");
	            error = true;
	        } else {
	            post_code.removeClass('has-error-border');
	        }
	        if(!validateCity(city.val())){
	            city.addClass('has-error-border');
	            $('#checkout-city').parent().append("<p style='color:red;'>*This is not a valid city name.</p>");
	            error = true;
	        } else {
	            city.removeClass('has-error-border');
	        }
			if(!login_state)
			{
				if (!is_valid_email(mail)) {
			        error = true;
				}
		        if(email_enabel === 'false')
		        {
		            error = true;
		        }
			}
	        if(!error){
		          self_this.submit();
	        } else {
	            form_this.preventDefault();
	        }		
	}

    $('#select-country').on('change', function () {
        var _this = this;
        var qty_raw = $('.input-qty-val');
        var qty = $(qty_raw).length;
        var product = qty_raw[0].id.replace ( /[^\d.]/g, '' );
        var check_lang = $("input[name='lng']").val();

        if(check_lang == "en") {
            var subtotal_price = $('#subtotal-price').html().replace("z"+"\u0142", '');
        } else {
            var subtotal_price = $('#subtotal-price').html().replace("\u20ac", '');
        }

            var selected_option = $('#select-country').find(":selected").text();

            $.ajax( {
                    type : "get",
                    url: "functions.php",
                    data: {
                        get_country : 'get',
                        product : product,
                        country: selected_option,
                        qty : qty
                    }
                } )
                .done(function(res) {
                    var response = jQuery.parseJSON(res);

                    var pl = response.pl;
                    console.log(pl);
                    var en = response.en; 
                    console.log(en);
                    if(check_lang == 'pl'){
                        //english
                        $('#shipping-price').html("\u20ac"+ en);
                        var total =  +subtotal_price + +en;
                        $('#total-price').html("\u20ac" + total);
                    } else {
						$('#shipping-price').html(pl + " z"+"\u0142" );
                        var total =  +subtotal_price + +pl;
                        $('#total-price').html(total + "z"+"\u0142" );
                    }
                })
                .always(function() {
                });

    });

    // $('#review-form').submit(function(){
    //    alert('I do something before the actual submission');
    //    return true;
    // });


        /* Invoice Required checkbox */

		function copy_shipping_address_from_select_option(address_select) {
			$selectedOption = $(address_select).find('option:selected');
	
			$('.shipping_firstname').val($selectedOption.attr('data-firstname'));
			$('.shipping_lastname').val($selectedOption.attr('data-lastname'));
			$('.shipping_postcode').val($selectedOption.attr('data-postalcode'));
			$('.shipping_city').val($selectedOption.attr('data-city'));
			$('.shipping_country').val($selectedOption.attr('data-country'));
			$('.shipping_addressline1').val($selectedOption.attr('data-addressline1'));
			$('.shipping_addressline2').val($selectedOption.attr('data-addressline2'));

			console.log($('.shipping_firstname'));
		}

		function copy_billing_address_from_select_option(address_select) {
			$selectedOption = $(address_select).find('option:selected');

			$('.billing_firstname').val($selectedOption.attr('data-firstname'));
			$('.billing_lastname').val($selectedOption.attr('data-lastname'));
			$('.billing_postcode').val($selectedOption.attr('data-postalcode'));
			$('.billing_city').val($selectedOption.attr('data-city'));
			$('.billing_country').val($selectedOption.attr('data-country'));
			$('.billing_addressline1').val($selectedOption.attr('data-addressline1'));
			$('.billing_addressline2').val($selectedOption.attr('data-addressline2'));
		}

		function activate_custom_shipping_address() {
			$('.save-custom-address-checkbox').prop('checked', true);
			$('.save-custom-address-checkbox').prop('disabled', false);
			$('.save-custom-address-wrapper').show();

			$('.shipping_field').prop('disabled', false);

			$('.shipping_field').parents('.form-field').removeClass('form-field-disabled');
			$('.shipping-addresses-select').prop('disabled', true);
			$('.shipping-addresses-select').parents('.form-field').addClass('form-field-disabled');
		}

		function deactivate_custom_shipping_address() {
			$('.save-custom-address-checkbox').prop('checked', false);
			$('.save-custom-address-checkbox').prop('disabled', true);

			$('.save-custom-address-wrapper').hide();

			$('.shipping_field').prop('disabled', true);

			$('.shipping_field').parents('.form-field').addClass('form-field-disabled');
			$('.shipping-addresses-select').prop('disabled', false);
			$('.shipping-addresses-select').parents('.form-field').removeClass('form-field-disabled');
		}

		function activate_custom_billing_address() {
			$('.save-custom-billing-address-checkbox').prop('checked', true);
			$('.save-custom-billing-address-wrapper').show();

			$('.billing_field').prop('disabled', false);
			
			$('.billing_field').parents('.form-field').removeClass('form-field-disabled');
			$('.billing-addresses-select').prop('disabled', true);
			$('.billing-addresses-select').parents('.form-field').addClass('form-field-disabled');
		}

		function deactivate_custom_billing_address() {
			$('.save-custom-billing-address-checkbox').prop('checked', false);
			$('.save-custom-billing-address-wrapper').hide();

			$('.billing_field').prop('disabled', true);

			$('.billing_field').parents('.form-field').addClass('form-field-disabled');
			$('.billing-addresses-select').prop('disabled', false);
			$('.billing-addresses-select').parents('.form-field').removeClass('form-field-disabled');
		}

		function initalize() {
			if(typeof checkout_page_flag !== 'undefined')// check if checkout_page_flagis is decleared and is not undefined 
			{
				if(checkout_page_flag === 'true')
				{
					if(back === 'back')
					{
						if(login_state)
						{
					    	var usecustom_flag = $('.usecustom');
					    	if (usecustom_flag[1].checked) 
					    	{
								activate_custom_shipping_address();
							}
							else
							{
								deactivate_custom_shipping_address();
							}					
						}
					}
					else
					{
						var empty='';
						$('#checkout-fname').val(empty);
						$('#checkout-lname').val(empty);
						$('#checkout-addressone').val(empty);
						$('#checkout-addresstwo').val(empty);	
						$('#checkout-postcode').val(empty);
						$('#checkout-city').val(empty);
						$('#select-country').val(empty);
						$('#checkout-email').val(empty);		
					}		
				}
			}
		}


		if ($('.addresses-select').length) {
			$('.usecustom').on('change', function() {
				if ($(this).val() == 1) {
					activate_custom_shipping_address();
				} else {
					deactivate_custom_shipping_address();
				}
			});

			// deactivate_custom_shipping_address();
		} else {
			activate_custom_shipping_address();
		}

		if ($('.billing-addresses-select').length) {
			$('.usecustombilling').on('change', function() {
				if ($(this).val() == 1) {
					activate_custom_billing_address();
				} else {
					deactivate_custom_billing_address();
				}
			});

			deactivate_custom_billing_address();
		} else {
			activate_custom_billing_address();
		}

        $( ".invoice-required-checkbox").on('change', function() {
        	// debugger;
            if(this.checked) {
                $(".display-on-off").css("display","block");
            }
            else {
                $(".display-on-off").css("display","none");
            }
        });

		$('.create-account-checkbox').on('change', function() {
			if (this.checked) {
				$('.account-info-wrapper').css('display', 'block');
			} else {
				$('.account-info-wrapper').css('display', 'none');
			}
		});

        /* // Invoice Required checkbox */
        $(".menu-button-wrapper").click(function(){
            $("#nav").toggle(
                function() {
                $("nav").animate()
                }
            ),
                $("#navicon").toggleClass('fa-times').toggleClass('fa-bars')
        });
});


/*
 *
 * Helper functions
 *
 * */
function validateCity(city){
    var re = /^[a-zA-Z'-]+(?:[\s-][a-zA-Z]+)*$/;
    return re.test(city);
}
function validateZip(zip){
    // var re = /^\d{5}$|^\d{5}-\d{4}$/; for US zip codes only
	var re = /^[a-zA-Z0-9 -]{4,}$/;
    return re.test(zip);
}
function validateAddress(address){
    var re = /(\d+\s*(?:[A-Z](?![A-Z]))?)/;
    return re.test(address);
}
function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function validatePhone(phone_num){
    var re = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i
    return re.test(phone_num);
}


